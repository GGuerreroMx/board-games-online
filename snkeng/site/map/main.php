<?php

//
switch ( se_nav::current() ) {
	//
	case 'games':
		seLoad_appParams('games', true);
		require $conditions['app']['games']['dir'].'/frontend/map_pages.php';
		break;

	//
	case '':
	case null:
		require $_SERVER['DOCUMENT_ROOT'].'/snkeng/site/func/pages_simple/f_index.php';
		break;

	// Otros
	default:
		seLoad_appParams('site', true);
		require $conditions['app']['site']['dir'].'/frontend/map_pages.php';
		break;
}
//
