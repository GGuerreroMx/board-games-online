<?php
//
return [
    'server' => [
	    'test' => false,
	    'local' => false,
	    'base_path' => '',
	    'url' => ''
    ],
	'site' => [
		'url'=>'http://www.boardgames.cora.snako.dev',
		'surl'=>'https://www.boardgames.cora.snako.dev',
		'img'=>'/snkeng/site/res/img/logos/main.jpg',
		'name'=>'Board Games',
		'desc'=>'Board games',
		'domain'=>'boardgames.cora.snako.dev',
		'baseurl'=>'www.boardgames.cora.snako.dev',
		//
		'langs' => ['es_MX'],
		'lang' => 'es_MX'
	],
	'page' => [
		'complete' => false,
		'template' => 'main',
		'base' => '' // Empty -> defaults to system
	],
	'extmod' => [
		'gA' =>'',
		'gCaptcha' => ['key' => '', 'secret' => '']
	],
	'snk_eng' => [
		'ieCode'=>'mCarCHckd99CQMyDPGnFZrteFCBSz4'
	],
	'admin' => [
		'mail'=>'adenoidsnake@hotmail.com',
		'contact'=>'gguerreromx@hotmail.com'
	]
];