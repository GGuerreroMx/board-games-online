<?php
// Configuración Telnorm
return [
	'locations' => [
		'boardgames.cora.snako.dev' => [
			'db' => 'local_test',
			'lang' => 'es_MX',
			'local' => true,
			'test' => true,
		],
	],
	'db'=> [
		'local_test'=>[
			'db'=>'db_boardgames',
			'adm' => ['root', 'snkTst'],
			'usr' => ['root', 'snkTst'],
		]
	],
];
