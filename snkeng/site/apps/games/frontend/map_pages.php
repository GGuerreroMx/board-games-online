<?php
\se_nav::next(); // remove games
//
$conditions['app']['name'] = 'games';

// debugVariable($conditions['app']['site']);

//
switch ( \se_nav::next() ) {
	//
	case 'game':
		$gameURLData = \se_nav::next();
		if ( empty($gameURLData) ) {
			\se_nav::invalidPage('No hay información del juego');
		}

		//
		list($gameId, $gameKey) = explode(':', $gameURLData);
		$gameId = intval($gameId);
		if ( empty($gameId) || empty($gameKey) ) { \se_nav::killWithError('Datos no válidos.'); }

		//
		$sql_qry = <<<SQL
SELECT
    bgm.bgm_id AS id,
	bgm.bgm_dt_add AS dtAdd, bgm.bgm_dt_mod AS dtMod,
	bgm.bgm_game AS game, bgm.bgm_players AS players,
	bgm.bgm_status_general AS statusGeneral,
    bgm_ikey AS iKey,
	bgm.bgm_data AS gameData
FROM st_bg_matches AS bgm
WHERE bgm.bgm_id={$gameId};
SQL;
		$gameData = $mysql->singleRowAssoc($sql_qry,
			[
				'int' => ['id']
			],
			[
				'errorKey' => '',
				'errorDesc' => '',
				'errorEmpty' => 'No hay juego con estas condiciones.',
			]
		);

		//
		if ( $gameData['iKey'] !== $gameKey) {
			\se_nav::killWithError('Datos del juego no válidos.');
		}

		//
		if ( !file_exists( __DIR__ . "/../games/{$gameData['game']}/index.php" ) ) {
			\se_nav::killWithError('Juego no encontrado.', '¿Fue borrado?', 500);
		}

		//
		$params['vars']['gameId'] = $gameData['id'];
		$params['vars']['gameKey'] = $gameData['iKey'];

		//
		require __DIR__ . "/../games/{$gameData['game']}/index.php";
		break;

	//
	case 'load':
		require __DIR__ . '/pages/game_load.php';
		break;

	//
	case 'new':
		require __DIR__ . '/pages/game_new.php';
		break;

	//
	case '':
	case null:
		require __DIR__ . '/pages/index.php';
		break;

	//
	default:
		\se_nav::invalidPage();
		break;
}
//
