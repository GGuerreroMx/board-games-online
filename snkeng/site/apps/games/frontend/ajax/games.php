<?php
//
switch ( \se_nav::next() ) {
	//
	case 'create':
		//
		$cData = \se_saveData::postParsing(
			$response,
			[
				'gameData' => ['name' => 'Usuarios', 'type' => 'json', 'validate' => true],
			]
		);

		//
		if ( !file_exists(__DIR__ . "/../../games/{$cData['gameData']['game']}/index.php") ) {
			\se_nav::killWithError('El juego no existe');
		}

		$response['d']['id'] = $mysql->getNextId('st_bg_matches');
		$response['d']['iKey'] = \se_randString(16);

		// Player analizis
		$totalPlayers = 0;
		$playerData = [];
		$usr_sql = "";
		$first = true;
		foreach ( $cData['gameData']['usr'] as $cUser ) {
			$cUser['color'] = intval($cUser['color']);
			if ( empty($cUser['name']) || $cUser['color'] === 0 ) {
				continue;
			}

			//
			$iKey = \se_randString(16);

			if ( !$first ) { $usr_sql.= ",\n"; }

			//
			$usr_sql.= <<<SQL
('{$response['d']['id']}', '{$iKey}', '{$totalPlayers}', '{$cUser['name']}', '{$cUser['color']}')
SQL;
			//
			$totalPlayers++;
			$first = false;
		}


		if ( $totalPlayers < 2 ) {
			\se_nav::killWithError('Jugadores insuficientes.');
		}

		$ins_qry = <<<SQL
INSERT INTO st_bg_matches (
	bgm_id, bgm_ikey, bgm_game, bgm_players, bgm_status_general
) VALUES (
	'{$response['d']['id']}', '{$response['d']['iKey']}', '{$cData['gameData']['game']}', '{$totalPlayers}', '1'
);
INSERT INTO st_bg_matches_players (
	bgm_id,	bgmp_ikey, bgmp_index, bgmp_name, bgmp_color
) VALUES 
{$usr_sql};\n
SQL;

		// debugVariable($ins_qry);

		//
		$mysql->submitMultiQuery($ins_qry, [
			'errorKey' => '',
			'errorDesc' => ''
		]);
		break;
	//
	case 'update':
		//
		\se_saveData::sql_complex_rowUpd(
			$response,
			[
				'id' => ['name' => 'ID Objeto', 'type' => 'int', 'validate' => true],
				'gameData' => ['name' => 'Tipo', 'type' => 'json', 'validate' => true],
			],
			[
				'elems' => [
					'bgm_data' => 'gameData',
					'bgm_status_general' => 'statusGeneral',
				],
				'tName' => 'st_bg_matches',
				'tId' => ['nm' => 'id', 'db' => 'bgm_id'],
			],
			[
				'setData' => [
					'statusGeneral' => 2
				]
			]
		);
		break;
	//
	case 'download':
		$gameId = intval($_POST['id']);
		$gameKey = strval($_POST['iKey']);
		//
		$sql_qry = <<<SQL
SELECT
	bgm_id AS id, bgm_ikey AS iKey, bgm_status_general AS statusGeneral, bgm_data AS gameData
FROM st_bg_matches
WHERE bgm_id={$gameId}\n
SQL;

		//
		$response['d'] = $mysql->singleRowAssoc($sql_qry,
			[
				'int' => ['id', 'statusGeneral'],
				'json' => ['gameData'],
			],
			[
				'errorKey' => 'gameLoad01',
				'errorDesc' => 'No fue posible cargar el juego.'
			]
		);

		//
		if ( $response['d']['iKey'] !== $gameKey ) {
			\se_killWithError('Datos no válidos.');
		}

		//
		if ( $response['d']['statusGeneral'] === 1 ) {
			//
			$sql_qry = <<<SQL
SELECT
	bgmp_id AS id, bgmp_ikey AS iKey, bgmp_name AS name, bgmp_color AS color
FROM st_bg_matches_players
WHERE bgm_id={$gameId}
LIMIT 10;\n
SQL;

			//
			$response['d']['players'] = $mysql->returnArray($sql_qry,
				[
					'int' => ['id', 'color'],
				],
				[
					'errorKey' => 'gameLoad02Players',
					'errorDesc' => 'No fue posible obtener la información de los jugadores.'
				]
			);
		}
		break;

	// Leer todas
	case 'readAll':
		$an_qry = (require __DIR__ . '/../nav/games.php');
		// Preparar estructura
		$post = new manual_ajaxjson();
		$post->createJSON($an_qry['games'], 'post');
		break;

	//
	default:
		\se_nav::invalidPage();
		break;
}
//
