<?php
//
$nav = [
	'games' => [
		'sel' => "bgm.bgm_id AS id,
				bgm.bgm_dt_add AS dtAdd, bgm.bgm_dt_mod AS dtMod,
				bgm.bgm_game AS game, bgm.bgm_players AS players,
				bgm.bgm_status_general AS statusGeneral, bgm.bgm_iKey AS iKey",
		'from' => 'st_bg_matches AS bgm',
		'lim' => 20,
		'where' => [
			'id' => ['name'=>'ID', 'db'=>'bgm.bgm_id', 'vtype'=>'int', 'stype'=>'eq'],
			'game' => ['name'=>'Game', 'db'=>'bgm.bgm_game', 'vtype'=>'str', 'stype'=>'eq'],
			'players' => ['name'=>'Players', 'db'=>'bgm.bgm_players', 'vtype'=>'int', 'stype'=>'eq'],
			'statusGeneral' => ['name'=>'Status General', 'db'=>'bgm.bgm_status_general', 'vtype'=>'int', 'stype'=>'eq'],
		],
		'order' => [
			'dtMod' => ['name'=>'Dt Mod', 'db'=>'bgm.bgm_dt_mod', 'set'=>'DESC'],
		]
	],
];
//
return $nav;
