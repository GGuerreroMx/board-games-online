<?php
// Files
\se_nav::pageFileGroupAdd(['game_new.js']);


$page['head']['title'] = 'Setup';
//
$page['body'] = <<<HTML
<div class="fullTitle"><div class="wpContent">Juego Nuevo</div></div>
<div id="page_contact" class="wpContent grid">
	<div class="gr_sz04 gr_ps04">\n
		<form class="se_form" se-plugin="games_game_new">
			<label class="separator required">
				<div class="cont"><span class="title">Juego</span><span class="desc"></span></div>
				<select name="game">
					<option value="rosk">ROSK</option>
					<option value="megacorp">Capitalism</option>
				</select>
			</label>
			<div class="separator required">
				<label>
					<div class="cont"><span class="title">Jugadores</span><span class="desc"></span></div>
					<table class="se_table wide alternate border gOMB">
						<thead>
							<tr>
								<th>#</th>
								<th>Nombre</th>
								<th>Color</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>1</td>
								<td><input type="text" name="usr[0][name]" value="Alpha"></td>
								<td>
									<select name="usr[0][color]">
										<option value="0">Inactivo</option>
										<option value="1" selected="">Azul</option>
										<option value="2">Rojo</option>
										<option value="3">Amarillo</option>
										<option value="4">Verde</option>
										<option value="5">Blanco</option>
										<option value="6">Negro</option>
									</select>
								</td>
							</tr>
							<tr data-user="1">
								<td>2</td>
								<td><input type="text" name="usr[1][name]" value="Bravo"> </td>
								<td>
									<select name="usr[1][color]">
										<option value="0">Inactivo</option>
										<option value="1">Azul</option>
										<option value="2" selected="">Rojo</option>
										<option value="3">Amarillo</option>
										<option value="4">Verde</option>
										<option value="5">Blanco</option>
										<option value="6">Negro</option>
									</select>
								</td>
							</tr>
							<tr data-user="2">
								<td>3</td>
								<td><input type="text" name="usr[2][name]" value="Charlie"> </td>
								<td>
									<select name="usr[2][color]">
										<option value="0">Inactivo</option>
										<option value="1">Azul</option>
										<option value="2">Rojo</option>
										<option value="3" selected="">Amarillo</option>
										<option value="4">Verde</option>
										<option value="5">Blanco</option>
										<option value="6">Negro</option>
									</select>
								</td>
							</tr>
							<tr data-user="3">
								<td>4</td>
								<td><input type="text" name="usr[3][name]" value="Delta"> </td>
								<td>
									<select name="usr[3][color]">
										<option value="0">Inactivo</option>
										<option value="1">Azul</option>
										<option value="2">Rojo</option>
										<option value="3">Amarillo</option>
										<option value="4" selected="">Verde</option>
										<option value="5">Blanco</option>
										<option value="6">Negro</option>
									</select>
								</td>
							</tr>
							<tr data-user="4">
								<td>5</td>
								<td><input type="text" name="usr[4][name]"> </td>
								<td>
									<select name="usr[4][color]">
										<option value="0">Inactivo</option>
										<option value="1">Azul</option>
										<option value="2">Rojo</option>
										<option value="3">Amarillo</option>
										<option value="4">Verde</option>
										<option value="5">Blanco</option>
										<option value="6">Negro</option>
									</select>
								</td>
							</tr>
							<tr data-user="5">
								<td>6</td>
								<td><input type="text" name="usr[5][name]"> </td>
								<td>
									<select name="usr[5][color]">
										<option value="0">Inactivo</option>
										<option value="1">Azul</option>
										<option value="2">Rojo</option>
										<option value="3">Amarillo</option>
										<option value="4">Verde</option>
										<option value="5">Blanco</option>
										<option value="6">Negro</option>
									</select>
								</td>
							</tr>
						</tbody>
					</table>
				</label>
			</div>
			<button type="submit"><svg class="icon inline mr"><use xlink:href="#fa-mail-forward" /></svg>Enviar</button>
			<output se-elem="response"></output>
		</form>
	</div>
</div>
HTML;
//