<?php
// Query
$an_qry = (require __DIR__ . '/../nav/games.php');
//
$an_empty = "No hay elementos que mostrar.";

// Estructura
$an_struct = <<<HTML
<tr se-ajaxelem="obj" data-objid="!id;" data-objtitle="!title;" data-btype="!bTypeNum;">
	<td>!id;</td>
	<td>!game;</td>
	<td>!players;</td>
	<td>!statusGeneral;</td>
	<td>!dtMod;</td>
	<td><a class="btn" se_target="se_middle" href="/games/game/!id;:!iKey;/desktop">Abrir</a></td>
</tr>
HTML;
//
$exData = [
	'js_id' => 'pagElements',
	'js_url' => '/ajax/api/games/game/readAll',
	'printElements' => false,
	'printType' => 'table',
	'tableWide' => true,
	'contentClass' => 'siteAdm_games',
	'tableHead'=>[
		['name' => 'ID', 'filter' => 1, 'fName'=>'id'],
		['name' => 'Juego', 'filter' => 1, 'fName'=>'title'],
		['name' => 'Jugadores'],
		['name' => 'Estado'],
		['name' => 'Jugado'],
		['name' => 'Acciones'],
	],
	'settings' => ['nav' => true],
];

// debugVariable($post->sql_qry, '', true);
$post = new manual_ajaxjson();
$post->createIniElem($an_qry['games'], 'get', $an_struct, $an_empty, $exData);

// Page
$page['body'] = <<<HTML
<div class="fullTitle"><div class="wpContent">Cargar juegos</div></div>

<div class="wpContent">
{$post->r_html}
</div>
HTML;
//
