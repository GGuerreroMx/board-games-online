"use strict";
se.plugin.games_game_new = function (plugEl, options) {
	//
	function init() {
		plugEl.se_on("submit", submit);
	}

	function submit(e) {
		e.preventDefault();

		// Send to server
		se.ajax.json(
			'/ajax/api/games/game/create/',
			{
				gameData:JSON.stringify(se.form.serializeObject(plugEl))
			},
			{
				onSuccess:(msg) => {
					console.log("game created");
					//
					se.ajax.pageLink(
						"/games/game/" + msg.d.id + ":" + msg.d.iKey + "/desktop",
						'se_middle'
					);
				},
				onFail:() => {
					console.error("failure, with emotional damage");
				}
			}
		);
	}

	// Iniciar
	init();
};