﻿"use strict";

var
	app = {
		state: {
			userName: '',
			userIndex: -1,
			boardGame: '',
			boardData: null
		},
		funcs: {},
		variables: {}
	},
	boardGames = {};