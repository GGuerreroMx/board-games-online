﻿"use strict";
var boardGames = {
	func:{},
};

boardGames.func.saveBoard = (boardName, gameId, gameKey, boardData) => {
	let boardDataTxt = JSON.stringify(boardData);

	// localStorage.setItem( boardName, boardDataTxt);

	// Send to server
	se.ajax.json(
		'/ajax/api/games/game/update/',
		{
			id:gameId,
			iKey:gameKey,
			game:boardName,
			gameData:boardDataTxt
		},
		{
			onSuccess:(msg) => {
				console.log("GAME SAVED.");
			},
			onFail:() => {
				console.error("failure, with emotional damage");
			}
		}
	);

};
boardGames.func.loadBoard = (boardName, gameId, gameKey, onSuccess, onFail) => {
	// Send to server
	se.ajax.json(
		'/ajax/api/games/game/download/',
		{
			id:gameId,
			iKey:gameKey,
			game:boardName
		},
		{
			onSuccess:(msg) => {
				onSuccess(msg.d);
			},
			onFail:onFail
		}
	);
};