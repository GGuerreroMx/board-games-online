﻿"use strict";

//
var mqtt_connector = {

	mqtt_object:null,
	object_type:null,
	game_id:null,
	msgInCallback:null,
	onConnectCallback:null,
	//
	connect:(objType, gameId, messageInCallback, onConnect) => {
		//
		mqtt_connector.object_type = objType;
		mqtt_connector.game_id = gameId;
		mqtt_connector.msgInCallback = messageInCallback;
		mqtt_connector.onConnectCallback = onConnect;

		//
		mqtt_connector.mqtt_object = new Paho.MQTT.Client(window.location.hostname, 443, "/mqtt/", 'game_' + gameId + '_' + objType + '_' + parseInt(Math.random() * 100, 10));

		// set callback handlers
		mqtt_connector.mqtt_object.onConnectionLost = mqtt_connector.onConnectionLost;
		mqtt_connector.mqtt_object.onMessageArrived = mqtt_connector.onMessageArrived;

		// connect the client
		mqtt_connector.mqtt_object.connect({
			onSuccess:mqtt_connector.onConnect,
			useSSL: true
		});
	},
	//
	onConnect:() => {
		// Once a connection has been made, make a subscription and send a message.
		console.debug("MQTT CONNECTED");

		//
		switch ( mqtt_connector.object_type ) {
			case 'desktop':
				console.debug("Subscribe: ", "game/" + mqtt_connector.game_id + "/userOp/#");
				//
				mqtt_connector.mqtt_object.subscribe(
					"game/" + mqtt_connector.game_id + "/userOp/#",
					{
						qos:2
					}
				);
				break;
			case 'mobile':
				console.log("Subscribe: ", "game/" + mqtt_connector.game_id + "/board/#");
				//
				mqtt_connector.mqtt_object.subscribe(
					"game/" + mqtt_connector.game_id + "/board/#",
					{
						qos:2
					}
				);
				break;
		}

		if ( typeof mqtt_connector.onConnectCallback === 'function' ) {
			mqtt_connector.onConnectCallback();
		}

	},
	onConnectionLost:(responseObject) => {
		if ( responseObject.errorCode !== 0 ) {
			console.error("onConnectionLost:"+responseObject.errorMessage);
		}
	},
	onMessageArrived:(message) => {
		// console.log("MQTT - onMessageArrived: " + message.destinationName + " | " + message.payloadString);

		//
		if ( typeof mqtt_connector.msgInCallback === 'function' ) {
			let msgHeader = message.destinationName.split("/"),
				msgStructure = JSON.parse(message.payloadString);

			//
			switch ( msgHeader[0] ) {
				//
				case 'game':
					let cGameId = intVal(msgHeader[1]);
					if ( cGameId !== mqtt_connector.game_id ) {
						console.error("mqtt inválido, no corresponde al juego");
					}

					mqtt_connector.msgInCallback(msgHeader, msgStructure);
					break;
			}
		}
	},
	sendMessage:(msgTarget, msgContent, retain = false) => {
		let msgTargetProc = "game/" + mqtt_connector.game_id + msgTarget,
			msgContentTxt = JSON.stringify(msgContent);

		if ( !mqtt_connector.mqtt_object ) {
			console.error("El objeto mqtt no está presente, no pudo ser enviado el mensaje.", msgTarget);
			return;
		}

		console.debug("MQTT SEND:", msgTargetProc)

		// let message = new Paho.MQTT.Message(msgContentTxt);
		// message.destinationName = "game/" + mqtt_connector.game_id + msgTarget;

		//
		mqtt_connector.mqtt_object.send(msgTargetProc, msgContentTxt, 2, retain);
	}
}