<?php
//
\se_nav::cacheCheckFile(__FILE__);

// Icons
\se_nav::pageFileGroupAdd('se-icons');


// General files
\se_nav::pageFileGroupAdd(['main.js', 'core_main.js', 'main_mobile.css']);

// Extra fonts
\se_nav::pageFileExternalAdd('//fonts.googleapis.com/css?family=Montserrat|Open+Sans:400,400italic,700');


$curYear = date("Y");

$page['body'] = <<<HTML
<div id="se_main">
<!-- INI:MAIN -->
<main id="se_middle">
{$page['body']}
</main>
<!-- END:MAIN -->\n
</div>
HTML;
//
