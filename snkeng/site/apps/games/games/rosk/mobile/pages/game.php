<?php
// Files
\se_nav::pageFileGroupAdd(['game_rosk_mobile.js', 'game_rosk_mobile.css', 'game_rosk_general.js', 'game_general.js', 'game_general_mobile.css', 'game_general_mobile.js']);

// Content
$page['body'] = <<<HTML
<div class="mobileGameApp" se-plugin="games_rosk_mobile_game" data-gameid="{$params['vars']['gameId']}" data-userid="{$params['vars']['userId']}" data-userkey="{$params['vars']['userKey']}" data-userindex="{$params['vars']['userIndex']}">
	<div class="mobileAppTitle">ROSK</div>
	<div class="screensContainer">
		<div data-screen-name="start" aria-hidden="true">
			<div class="appTitle">Esperando inicio de juego</div>
		</div>
		<div data-screen-name="playerSelect" aria-hidden="true">
			<div class="appTitle">Selección jugador</div>
			<div class="userNotSelected">
				<div se-elem="playerList">
					<template>
						<div data-index="!index;" aria-selected="false">!name;</div>
					</template>
					<div class="playerList" se-elem="content"></div>
				</div>
			</div>
			<div class="userSelected">
				<div>Listo para jugar: <div class="playerName"></div></div>
				<button class="btn wide red" se-act="cancelUserSelect"><svg class="icon inline mr"><use xlink:href="#fa-remove" /></svg>Cancelar selección</button>
			</div>
		</div>
		<div class="phone_app" data-screen-name="mainGame" aria-hidden="true">
			<div class="app_screens">
				<section id="section_users" aria-hidden="true">
					<div class="appTitle">General</div>
					<div class="appSection">
						<div class="appSubTitle">Status</div>
						<table class="se_table alternate border wide" se-elem="playerList">
							<template>
								<tr data-color="!playerColor;" data-turn="!isTurn;" data-isuser="!isUser;" data-isalive="!isAlive;">
									<td class="name">!playerName;</td>
									<td se-elem="totalTerritories">!totalTerritories;</td>
									<td se-elem="totalUnits">!totalUnits;</td>
									<td se-elem="totalPlanes">!totalPlanes;</td>
									<td se-elem="totalCapitals">!totalCapitals;</td>
									<td se-elem="totalBases">!totalBases;</td>
									<td><div class="fakeCheck" se-elem="hasCapital" data-value="!hasCapital;"></div></td>
								</tr>
							</template>
							<thead>
							<tr>
								<th>Jugador</th>
								<th><svg class="icon inline"><use xlink:href="#fa-flag" /></svg></th>
								<th><svg class="icon inline"><use xlink:href="#fa-male" /></svg></th>
								<th><svg class="icon inline"><use xlink:href="#fa-fighter-jet" /></svg></th>
								<th><svg class="icon inline"><use xlink:href="#fa-star" /></svg></th>
								<th><svg class="icon inline"><use xlink:href="#fa-fort-awesome" /></svg></th>
								<th>Cap</th>
							</tr>
							</thead>
							<tbody se-elem="content"></tbody>
						</table>
					</div>
					<div class="appSection">
						<div class="appSubTitle">Configuración</div>
						<div>
							<button class="btn wide green" se-act="notificationTurnOn"><svg class="icon inline mr"><use xlink:href="#fa-alert" /></svg>Activar notificaciones</button>
						</div>
					</div>
				</section>
				<section id="section_cards" aria-hidden="true">
					<div class="appTitle">Tarjetas</div>
					<div se-elem="cards">
						<div class="appComment">
							<p>El límite de tarjetas es de 5.</p>
							<p>De no descargar tarjetas, se removerá el exceso al azar.</p>
						</div>
						<div class="notification alert" se-elem="cardExcessAlert" aria-hidden="false" ">
							<div class="icon"><svg><use xlink:href="#fa-exclamation" /></svg></div>
							<div class="text">
								<span class="title">Exceso de tarjetas</span>
								<span class="content">Descarte para que no sea descartada automáticamente.</span>
							</div>
						</div>
						<template>
							<div data-id="!id;" data-type="!type;" data-usable="" aria-selected="false"><span>!content;</span><button class="btn small"><svg class="icon inline"><use xlink:href="#fa-trash" /></svg></button></div>
						</template>
						<div class="userCards" se-elem="content"></div>
					</div>
				</section>
				<section id="section_sideObj" aria-hidden="true">
					<div class="appTitle">Objetivos Secundarios</div>
					<div se-elem="objectivesSecondary">
						<template>
							<div data-difficulty="!difficulty;" data-owner="!owner;">
								<div class="requirements">!requirements;</div>
								<div class="bonus">!bonus;</div>
							</div>
						</template>
						<div class="objectiveSecondaryList" se-elem="content"></div>
					</div>
				</section>
				<section id="section_mainObj" aria-hidden="true">
					<div class="appTitle">Objetivos Primarios</div>
					<div se-elem="objectivesPrimary">
						<template>
							<div data-own="!own;"><ul>!content;</ul></div>
						</template>
						<div class="objectivePrimaryList" se-elem="content"></div>
					</div>
				</section>
				<section id="section_progress" aria-hidden="true">
					<div class="appTitle">Progreso</div>
					<table class="userObjective" >
						<tbody></tbody>
					</table>
				</section>
			</div>
			<div class="app_tabs">
				<a aria-selected="false" href="#section_users">
					<svg class="icon"><use xlink:href="#fa-group"/></svg>
				</a>
				<a aria-selected="false" href="#section_cards">
					<svg class="icon"><use xlink:href="#fa-file"/></svg>
				</a>
				<a aria-selected="false" href="#section_sideObj">
					<svg class="icon"><use xlink:href="#fa-tasks"/></svg>
				</a>
				<a aria-selected="false" href="#section_mainObj">
					<svg class="icon"><use xlink:href="#fa-briefcase"/></svg>
				</a>
				<a aria-selected="false" href="#section_progress">
					<svg class="icon"><use xlink:href="#fa-key"/></svg>
				</a>
			</div>
		</div>
	</div>
</div>
HTML;
