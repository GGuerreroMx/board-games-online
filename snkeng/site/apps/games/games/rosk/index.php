<?php
//
switch ( se_nav::next() ) {
	//
	case '':
	case null:
		\se_nav::killWithError('No debería entrar directamente al directorio', '');
		break;
	//
	case 'desktop':
		\se_nav::$templateFile = $_SERVER['DOCUMENT_ROOT'] . '/snkeng/site/apps/games/games/rosk/desktop/template/main.php';
		// General files
		require __DIR__ . '/desktop/pages/game.php';
		break;
	//
	case 'mobile':
		\se_nav::$templateFile = $_SERVER['DOCUMENT_ROOT'] . '/snkeng/site/apps/games/games/rosk/mobile/template/main.php';

		$userURLData = \se_nav::next();
		if ( empty($userURLData) ) {
			\se_nav::invalidPage('No hay información del juego');
		}

		list($userDBId, $userDBKey) = explode(':', $userURLData);
		$userDBId = intval($userDBId);
		if ( empty($userDBId) || empty($userDBKey) ) { \se_nav::killWithError('Datos no válidos.', 'Incompletos'); }

		// Validar usuario
		$sql_qry = <<<SQL
SELECT
    bgmp.bgmp_id AS id, bgmp.bgm_id AS gameId,
	bgmp.bgmp_ikey AS iKey,
	bgmp.bgmp_index AS playerIndex, bgmp.bgmp_color AS playerColor, bgmp.bgmp_name AS playerName
FROM st_bg_matches_players AS bgmp
WHERE bgmp.bgmp_id={$userDBId};
SQL;
		$userData = $mysql->singleRowAssoc($sql_qry,
			[
				'int' => ['id', 'gameId']
			],
			[
				'errorKey' => '',
				'errorDesc' => '',
				'errorEmpty' => 'No existe usuario con estos datos.',
			]
		);

		// Validación de clave
		if ( $userDBKey !== $userData['iKey'] ) {
			\se_nav::killWithError('Datos no válidos.', 'Clave');
		}

		// Validación de juego
		if ( $params['vars']['gameId'] !== $userData['gameId'] ) {
			\se_nav::killWithError('Datos no válidos.', 'Jugador no corresponde al juego.');
		}

		// Variable set
		$params['vars']['userKey'] = $userData['iKey'];
		$params['vars']['userId'] = $userData['id'];
		$params['vars']['userIndex'] = $userData['playerIndex'];

		//
		require __DIR__ . '/mobile/pages/game.php';
		break;

	//
	default:
		\se_nav::invalidPage();
		break;
}
//
