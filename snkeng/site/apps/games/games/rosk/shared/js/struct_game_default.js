"use strict";
//
boardGames.risk = {
	initialStatus: {
		status: {
			fase: 'placement',
			faseRounds: 0,
			userTurn: 0,
			userStep: '',
			turnInfo: {}
		},
		stats: {
			timeStart: 0,
			totalRounds: 0,
		},
		players: [],
		sectors: {
			africa: {
				userIndex: 0,
				relType: 0
			},
			asia: {
				userIndex: 0,
				relType: 0
			},
			oceania: {
				userIndex: 0,
				relType: 0
			},
			europe: {
				userIndex: 0,
				relType: 0
			},
			northamerica: {
				userIndex: 0,
				relType: 0
			},
			southamerica: {
				userIndex: 0,
				relType: 0
			},
		},
		locations: {
			af_east_africa: {
				owner: -1,
				units: -1,
				isCapital: false,
				isBaseFor: 0
			},
			af_egypt: {
				owner: -1,
				units: -1,
				isCapital: false,
				isBaseFor: 0
			},
			af_congo: {
				owner: -1,
				units: -1,
				isCapital: false,
				isBaseFor: 0
			},
			af_madagascar: {
				owner: -1,
				units: -1,
				isCapital: false,
				isBaseFor: 0
			},
			af_south_africa: {
				owner: -1,
				units: -1,
				isCapital: false,
				isBaseFor: 0
			},
			af_north_africa: {
				owner: -1,
				units: -1,
				isCapital: false,
				isBaseFor: 0
			},
			as_afghanistan: {
				owner: -1,
				units: -1,
				isCapital: false,
				isBaseFor: 0
			},
			as_india: {
				owner: -1,
				units: -1,
				isCapital: false,
				isBaseFor: 0
			},
			as_irkutsk: {
				owner: -1,
				units: -1,
				isCapital: false,
				isBaseFor: 0
			},
			as_kamchatka: {
				owner: -1,
				units: -1,
				isCapital: false,
				isBaseFor: 0
			},
			as_middle_east: {
				owner: -1,
				units: -1,
				isCapital: false,
				isBaseFor: 0
			},
			as_mongolia: {
				owner: -1,
				units: -1,
				isCapital: false,
				isBaseFor: 0
			},
			as_siam: {
				owner: -1,
				units: -1,
				isCapital: false,
				isBaseFor: 0
			},
			as_china: {
				owner: -1,
				units: -1,
				isCapital: false,
				isBaseFor: 0
			},
			as_japan: {
				owner: -1,
				units: -1,
				isCapital: false,
				isBaseFor: 0
			},
			as_siberia: {
				owner: -1,
				units: -1,
				isCapital: false,
				isBaseFor: 0
			},
			as_ural: {
				owner: -1,
				units: -1,
				isCapital: false,
				isBaseFor: 0
			},
			as_yakutsk: {
				owner: -1,
				units: -1,
				isCapital: false,
				isBaseFor: 0
			},
			au_eastern_australia: {
				owner: -1,
				units: -1,
				isCapital: false,
				isBaseFor: 0
			},
			au_new_guniea: {
				owner: -1,
				units: -1,
				isCapital: false,
				isBaseFor: 0
			},
			au_western_australia: {
				owner: -1,
				units: -1,
				isCapital: false,
				isBaseFor: 0
			},
			au_indonesia: {
				owner: -1,
				units: -1,
				isCapital: false,
				isBaseFor: 0
			},
			eu_great_britain: {
				owner: -1,
				units: -1,
				isCapital: false,
				isBaseFor: 0
			},
			eu_iceland: {
				owner: -1,
				units: -1,
				isCapital: false,
				isBaseFor: 0
			},
			eu_polonia: {
				owner: -1,
				units: -1,
				isCapital: false,
				isBaseFor: 0
			},
			eu_germany: {
				owner: -1,
				units: -1,
				isCapital: false,
				isBaseFor: 0
			},
			eu_scandinavia: {
				owner: -1,
				units: -1,
				isCapital: false,
				isBaseFor: 0
			},
			eu_southern_europe: {
				owner: -1,
				units: -1,
				isCapital: false,
				isBaseFor: 0
			},
			eu_ukraine: {
				owner: -1,
				units: -1,
				isCapital: false,
				isBaseFor: 0
			},
			eu_western_europe: {
				owner: -1,
				units: -1,
				isCapital: false,
				isBaseFor: 0
			},
			na_alaska: {
				owner: -1,
				units: -1,
				isCapital: false,
				isBaseFor: 0
			},
			na_alberta: {
				owner: -1,
				units: -1,
				isCapital: false,
				isBaseFor: 0
			},
			na_central_america: {
				owner: -1,
				units: -1,
				isCapital: false,
				isBaseFor: 0
			},
			na_cuba: {
				owner: -1,
				units: -1,
				isCapital: false,
				isBaseFor: 0
			},
			na_eastern_united_states: {
				owner: -1,
				units: -1,
				isCapital: false,
				isBaseFor: 0
			},
			na_greenland: {
				owner: -1,
				units: -1,
				isCapital: false,
				isBaseFor: 0
			},
			na_northwest_territory: {
				owner: -1,
				units: -1,
				isCapital: false,
				isBaseFor: 0
			},
			na_ontario: {
				owner: -1,
				units: -1,
				isCapital: false,
				isBaseFor: 0
			},
			na_western_united_states: {
				owner: -1,
				units: -1,
				isCapital: false,
				isBaseFor: 0
			},
			na_quebec: {
				owner: -1,
				units: -1,
				isCapital: false,
				isBaseFor: 0
			},
			sa_argentina: {
				owner: -1,
				units: -1,
				isCapital: false,
				isBaseFor: 0
			},
			sa_brazil: {
				owner: -1,
				units: -1,
				isCapital: false,
				isBaseFor: 0
			},
			sa_peru: {
				owner: -1,
				units: -1,
				isCapital: false,
				isBaseFor: 0
			},
			sa_venezuela: {
				owner: -1,
				units: -1,
				isCapital: false,
				isBaseFor: 0
			},
		},
		cards:{
			stack:[],
			discard:[]
		},
		sideObjectives:[]
	},
	properties: {
		map: {
			sectors: {
				africa: {
					name: 'Africa',
					locations: 6,
					majority: 4,
					bonus_majority: 3,
					bonus_complete: 4
				},
				asia: {
					name: 'Asia',
					locations: 12,
					majority: 7,
					bonus_majority: 7,
					bonus_complete: 10
				},
				oceania: {
					name: 'Oceania',
					locations: 4,
					majority: 3,
					bonus_majority: 2,
					bonus_complete: 3
				},
				europe: {
					name: 'Europa',
					locations: 8,
					majority: 5,
					bonus_majority: 5,
					bonus_complete: 7
				},
				northamerica: {
					name: 'Norte américa',
					locations: 10,
					majority: 6,
					bonus_majority: 5,
					bonus_complete: 7
				},
				southamerica: {
					name: 'Sur américa',
					locations: 4,
					majority: 3,
					bonus_majority: 2,
					bonus_complete: 3
				},
			},
			locations: {
				af_east_africa: {
					name: 'Africa del este',
					sector: 'africa',
					capital: false,
					related: ['af_egypt', 'af_congo', 'af_madagascar', 'af_north_africa', 'af_south_africa']
				},
				af_egypt: {
					name: 'Egipto',
					sector:'africa',
					capital: true,
					related: ['af_north_africa', 'eu_southern_europe', 'as_middle_east', 'af_north_africa', 'af_east_africa']
				},
				af_congo: {
					name: 'Congo',
					sector:'africa',
					capital: false,
					related: ['af_east_africa', 'af_north_africa', 'af_south_africa']
				},
				af_madagascar: {
					name: 'Madagascar',
					sector:'africa',
					capital: false,
					related: ['af_east_africa', 'af_south_africa']
				},
				af_south_africa: {
					name: 'Sudafrica',
					sector:'africa',
					capital: true,
					related: ['af_east_africa', 'af_congo', 'af_madagascar']
				},
				af_north_africa: {
					name: 'Africa del norte',
					sector:'africa',
					capital: false,
					related: ['sa_brazil', 'eu_western_europe', 'af_egypt', 'af_congo', 'af_east_africa']
				},
				as_afghanistan: {
					name: 'Afganistan',
					sector:'asia',
					capital: false,
					related: ['eu_ukraine', 'as_ural', 'as_china', 'as_india', 'as_middle_east']
				},
				as_india: {
					name: 'India',
					sector:'asia',
					capital: false,
					related: ['as_middle_east', 'as_afghanistan', 'as_china', 'as_siam']
				},
				as_irkutsk: {
					name: 'Irkutsk',
					sector:'asia',
					capital: false,
					related: ['as_siberia', 'as_yakutsk', 'as_kamchatka', 'as_mongolia']
				},
				as_kamchatka: {
					name: 'Kamchatka',
					sector:'asia',
					capital: false,
					related: ['as_yakutsk', 'as_irkutsk', 'as_mongolia', 'as_japan', 'na_alaska']
				},
				as_middle_east: {
					name: 'Medio Oriente',
					sector:'asia',
					capital: true,
					related: ['af_egypt', 'eu_southern_europe', 'eu_ukraine', 'as_afghanistan', 'as_india']
				},
				as_mongolia: {
					name: 'Mongolia',
					sector:'asia',
					capital: false,
					related: ['as_siberia', 'as_irkutsk', 'as_kamchatka', 'as_japan', 'as_china']
				},
				as_siam: {
					name: 'Siam',
					sector:'asia',
					capital: false,
					related: ['as_india', 'as_china', 'au_indonesia']
				},
				as_china: {
					name: 'China',
					sector:'asia',
					capital: true,
					related: ['as_afghanistan', 'as_ural', 'as_siberia', 'as_mongolia', 'as_siam', 'as_india']
				},
				as_japan: {
					name: 'Japón',
					sector:'asia',
					capital: true,
					related: ['as_mongolia', 'as_kamchatka']
				},
				as_siberia: {
					name: 'Siberia',
					sector:'asia',
					capital: false,
					related: ['as_ural', 'as_yakutsk', 'as_irkutsk', 'as_mongolia', 'as_china']
				},
				as_ural: {
					name: 'Ural',
					sector:'asia',
					capital: false,
					related: ['eu_ukraine', 'as_siberia', 'as_china', 'as_afghanistan']
				},
				as_yakutsk: {
					name: 'Yakutsk',
					sector:'asia',
					capital: false,
					related: ['as_siberia', 'as_kamchatka', 'as_irkutsk']
				},
				au_eastern_australia: {
					name: 'Australia del este',
					sector:'oceania',
					capital: true,
					related: ['au_western_australia', 'au_indonesia', 'au_new_guniea']
				},
				au_new_guniea: {
					name: 'Nueva Guinea',
					sector:'oceania',
					capital: false,
					related: ['au_eastern_australia', 'au_indonesia', 'au_western_australia']
				},
				au_western_australia: {
					name: 'Austalia del oeste',
					sector:'oceania',
					capital: false,
					related: ['au_eastern_australia', 'au_indonesia', 'au_new_guniea']
				},
				au_indonesia: {
					name: 'Indonesia',
					sector:'oceania',
					capital: false,
					related: ['au_eastern_australia', 'au_western_australia', 'au_new_guniea', 'as_siam']
				},
				eu_great_britain: {
					name: 'Gran Bretaña',
					sector:'europe',
					capital: true,
					related: ['eu_iceland', 'eu_germany', 'eu_western_europe']
				},
				eu_iceland: {
					name: 'Islandia',
					sector:'europe',
					capital: false,
					related: ['na_greenland', 'eu_scandinavia', 'eu_great_britain']
				},
				eu_polonia: {
					name: 'Polonia',
					sector:'europe',
					capital: false,
					related: ['eu_ukraine', 'eu_southern_europe', 'eu_germany']
				},
				eu_germany: {
					name: 'Alemania',
					sector:'europe',
					capital: true,
					related: ['eu_scandinavia', 'eu_polonia', 'eu_southern_europe', 'eu_western_europe', 'eu_great_britain']
				},
				eu_scandinavia: {
					name: 'Escandinavia',
					sector:'europe',
					capital: false,
					related: ['eu_iceland', 'eu_ukraine', 'eu_germany']
				},
				eu_southern_europe: {
					name: 'Sur de europa',
					sector:'europe',
					capital: false,
					related: ['eu_germany', 'eu_polonia', 'eu_ukraine', 'as_middle_east', 'af_egypt', 'eu_western_europe']
				},
				eu_ukraine: {
					name: 'Ukrania',
					sector:'europe',
					capital: true,
					related: ['eu_scandinavia', 'as_ural', 'as_afghanistan', 'as_middle_east', 'eu_southern_europe', 'eu_polonia']
				},
				eu_western_europe: {
					name: 'Oeste de europa',
					sector:'europe',
					capital: false,
					related: ['eu_great_britain', 'eu_germany', 'eu_southern_europe', 'af_north_africa']
				},
				na_alaska: {
					name: 'Alaska',
					sector:'northamerica',
					capital: false,
					related: ['as_kamchatka', 'na_northwest_territory', 'na_alberta']
				},
				na_alberta: {
					name: 'Alberta',
					sector:'northamerica',
					capital: false,
					related: ['na_alaska', 'na_northwest_territory', 'na_ontario', 'na_western_united_states']
				},
				na_central_america: {
					name: 'America central',
					sector:'northamerica',
					capital: false,
					related: ['na_western_united_states', 'na_eastern_united_states', 'na_cuba', 'sa_venezuela']
				},
				na_cuba: {
					name: 'Cuba',
					sector:'northamerica',
					capital: false,
					related: ['na_central_america', 'na_eastern_united_states', 'sa_venezuela']
				},
				na_eastern_united_states: {
					name: 'Este de los Estados Unidos',
					sector:'northamerica',
					capital: true,
					related: ['na_western_united_states', 'na_ontario', 'na_quebec', 'na_cuba', 'na_central_america']
				},
				na_greenland: {
					name: 'Groelandia',
					sector:'northamerica',
					capital: false,
					related: ['na_northwest_territory', 'eu_iceland', 'na_quebec', 'na_ontario']
				},
				na_northwest_territory: {
					name: 'Territorio del Norte',
					sector:'northamerica',
					capital: false,
					related: ['na_alaska', 'na_greenland', 'na_ontario', 'na_alberta']
				},
				na_ontario: {
					name: 'Ontario',
					sector:'northamerica',
					capital: true,
					related: ['na_greenland', 'na_quebec', 'na_eastern_united_states', 'na_western_united_states', 'na_alberta', 'na_northwest_territory']
				},
				na_western_united_states: {
					name: 'Oeste de los Estados Unidos',
					sector:'northamerica',
					capital: false,
					related: ['na_alberta', 'na_ontario', 'na_western_united_states', 'na_central_america']
				},
				na_quebec: {
					name: 'Quebec',
					sector:'northamerica',
					capital: false,
					related: ['na_ontario', 'na_greenland', 'na_eastern_united_states']
				},
				sa_argentina: {
					name: 'Argentina',
					sector:'southamerica',
					capital: false,
					related: ['sa_peru', 'sa_brazil']
				},
				sa_brazil: {
					name: 'Brasil',
					sector:'southamerica',
					capital: true,
					related: ['sa_venezuela', 'af_north_africa', 'sa_argentina', 'sa_peru']
				},
				sa_peru: {
					name: 'Peru',
					sector:'southamerica',
					capital: false,
					related: ['sa_venezuela', 'sa_brazil', 'sa_argentina']
				},
				sa_venezuela: {
					name: 'Venezuela',
					sector:'southamerica',
					capital: false,
					related: ['na_central_america', 'sa_brazil', 'sa_peru', 'na_cuba']
				},
			}
		},
		cards:[
			{ type:'sector', value:'africa' },
			{ type:'sector', value:'asia' },
			{ type:'sector', value:'oceania' },
			{ type:'sector', value:'europe' },
			{ type:'sector', value:'northamerica' },
			{ type:'sector', value:'southamerica' },
			{ type:'location', value:'af_east_africa' },
			{ type:'location', value:'af_egypt' },
			{ type:'location', value:'af_congo' },
			{ type:'location', value:'af_madagascar' },
			{ type:'location', value:'af_south_africa' },
			{ type:'location', value:'af_north_africa' },
			{ type:'location', value:'as_afghanistan' },
			{ type:'location', value:'as_india' },
			{ type:'location', value:'as_irkutsk' },
			{ type:'location', value:'as_kamchatka' },
			{ type:'location', value:'as_middle_east' },
			{ type:'location', value:'as_mongolia' },
			{ type:'location', value:'as_siam' },
			{ type:'location', value:'as_china' },
			{ type:'location', value:'as_japan' },
			{ type:'location', value:'as_siberia' },
			{ type:'location', value:'as_ural' },
			{ type:'location', value:'as_yakutsk' },
			{ type:'location', value:'au_eastern_australia' },
			{ type:'location', value:'au_new_guniea' },
			{ type:'location', value:'au_western_australia' },
			{ type:'location', value:'au_indonesia' },
			{ type:'location', value:'eu_great_britain' },
			{ type:'location', value:'eu_iceland' },
			{ type:'location', value:'eu_polonia' },
			{ type:'location', value:'eu_germany' },
			{ type:'location', value:'eu_scandinavia' },
			{ type:'location', value:'eu_southern_europe' },
			{ type:'location', value:'eu_ukraine' },
			{ type:'location', value:'eu_western_europe' },
			{ type:'location', value:'na_alaska' },
			{ type:'location', value:'na_alberta' },
			{ type:'location', value:'na_central_america' },
			{ type:'location', value:'na_cuba' },
			{ type:'location', value:'na_eastern_united_states' },
			{ type:'location', value:'na_greenland' },
			{ type:'location', value:'na_northwest_territory' },
			{ type:'location', value:'na_ontario' },
			{ type:'location', value:'na_western_united_states' },
			{ type:'location', value:'na_quebec' },
			{ type:'location', value:'sa_argentina' },
			{ type:'location', value:'sa_brazil' },
			{ type:'location', value:'sa_peru' },
			{ type:'location', value:'sa_venezuela' },
			{ type:'additive', value:2 },
			{ type:'additive', value:2 },
			{ type:'additive', value:2 },
			{ type:'additive', value:2 },
			{ type:'additive', value:2 },
			{ type:'additive', value:2 },
			{ type:'additive', value:3 },
			{ type:'additive', value:3 },
			{ type:'additive', value:3 },
			{ type:'additive', value:3 },
			{ type:'additive', value:3 },
			{ type:'additive', value:3 },
			{ type:'additive', value:3 },
			{ type:'additive', value:4 },
			{ type:'additive', value:4 },
			{ type:'additive', value:4 },
			{ type:'additive', value:4 },
			{ type:'additive', value:4 },
			{ type:'additive', value:5 },
			{ type:'additive', value:5 },
			{ type:'additive', value:5 },
			{ type:'additive', value:5 },
		],
		sideObjectives:[
			{
				difficulty:'low',
				text:'Capturar Europa',
				type:'sector',
				value:'europe',
			},
			{
				difficulty:'low',
				text:'Capturar Norte américa',
				type:'sector',
				value:'northamerica',
			},
			{
				difficulty:'low',
				text:'Capturar Asia',
				type:'sector',
				value:'asia',
			},
			{
				difficulty:'low',
				text:'Tener 5 capitales',
				type:'capitals',
				value:5
			},

			{
				difficulty:'low',
				text:'Tener 2 bases',
				type:'bases',
				value:2
			},
			{
				difficulty:'low',
				text:'Capturar 3 territorios en un turno',
				type:'locationsCapture',
				value:3
			},
			{
				difficulty:'high',
				text:'Capturar 5 territorios en un turno',
				type:'locationsCapture',
				value:5
			},
			{
				difficulty:'high',
				text:'Tener 3 bases',
				type:'bases',
				value:3
			},
			{
				difficulty:'high',
				text:'Tener 18 territorios',
				type:'locations',
				value:18
			},
			{
				difficulty:'high',
				text:'Tener 2 continentes',
				type:'sectorsTotal',
				value:2
			},
			{
				difficulty:'high',
				text:'Tener 7 capitales',
				type:'capitals',
				value:7
			}
		],
		bonuses:[
			{ type:'real', difficulty:'high', action:'dice_defense', text:'Dado extra de defensa' },
			{ type:'real', difficulty:'high', action:'dice_attack', text:'Dado extra de ataque' },
			{ type:'game', difficulty:'high', action:'units_extra', text:'2 Unidades extra colocación' },
			{ type:'game', difficulty:'low', action:'movement_extra_ini', text:'Movimiento extra inicio' },
			{ type:'game', difficulty:'low', action:'movement_extra_end', text:'Movimiento extra final' },
			{ type:'game', difficulty:'low', action:'security_card', text:'Tarjeta extra' },
		],
		objectives:[
			{
				sectors:[],
				locations:[],
				totals:[
					{ type:'capitals', count:8, units:1}
				]
			},
			{
				sectors:[],
				locations:[],
				totals:[
					{ type:'capitals', count:5, units:10}
				]
			},
			{
				sectors:[],
				locations:[
					{ name:'na_western_united_states', units:50 },
					{ name:'as_japan', units:50 }
				],
				totals:[]
			},
			{
				sectors:[],
				locations:[
					{ name:'as_afghanistan', units:50 }
				],
				totals:[]
			},
			{
				sectors:[
					{ name:'southamerica', type:2 },
					{ name:'europe', type:2 }
				],
				locations:[
					{ name:'eu_germany', units:20 }
				],
				totals:[]
			},
			{
				sectors:[
					{ name:'southamerica', type:2 },
					{ name:'europe', type:2 }
				],
				locations:[],
				totals:[]
			},
			{
				sectors:[
					{ name:'northamerica', type:2 },
					{ name:'oceania', type:2 }
				],
				locations:[],
				totals:[]
			},
			{
				sectors:[
					{ name:'northamerica', type:2 },
					{ name:'oceania', type:2 }
				],
				locations:[],
				totals:[]
			},
			{
				sectors:[
					{ name:'europe', type:2 },
					{ name:'any', type:2 },
					{ name:'oceania', type:2 }
				],
				locations:[],
				totals:[]
			},
			{
				sectors:[
					{ name:'asia', type:2 },
					{ name:'europe', type:1 },
					{ name:'africa', type:1 }
				],
				locations:[],
				totals:[]
			},
			{
				sectors:[
					{ name:'asia', type:2 },
					{ name:'southamerica', type:2 }
				],
				locations:[],
				totals:[]
			},
			{
				sectors:[
					{ name:'asia', type:2 },
					{ name:'africa', type:2 }
				],
				locations:[],
				totals:[]
			},
			{
				sectors:[
					{ name:'africa', type:2 },
					{ name:'northamerica', type:2 }
				],
				locations:[],
				totals:[]
			},
			{
				sectors:[
					{ name:'europe', type:1 },
					{ name:'asia', type:1 },
					{ name:'northamerica', type:1 }
				],
				locations:[],
				totals:[]
			},
			{
				sectors:[],
				locations:[],
				totals:[
					{ type:'bases', count:3, units:1},
					{ type:'locations', count:16, units:1}
				]
			},
			{
				sectors:[],
				locations:[],
				totals:[
					{ type:'locations', count:24, units:1}
				]
			}
		]
	}
};