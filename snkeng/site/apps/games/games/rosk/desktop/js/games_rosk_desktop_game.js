"use strict";

//
se.plugin.games_rosk_desktop_game = function (plugElem) {
	//
	let gameStatus = null,
		boardName = 'risk',
		properties = {
			gameId:intVal(plugElem.se_data('gameid')),
			gameKey:plugElem.se_data('gamekey'),
		},
		qrGenerator
		;

	//
	const
		//
		boardOptions = plugElem.querySelector('#boardOptions'),
		//
		endOfGame = plugElem.querySelector('#endOfGame'),
		endOfUser = plugElem.querySelector('#endOfUser'),
		//
		userPlayerTable = plugElem.querySelector('table[se-elem="playerList"]'),
		userPlayerTableTemplate = userPlayerTable.querySelector('template'),
		userPlayerTableContent = userPlayerTable.querySelector('tbody'),
		//
		userQRDialog = plugElem.querySelector('div[se-elem="userQRDialog"]'),
		qrObject = userQRDialog.querySelector('div[se-elem="qrObject"]'),
		//
		liveConsole = plugElem.querySelector('div.liveConsole'),
		//
		boardScreenContainer = plugElem.querySelector('div.boardScreenContainer'),
		boardMap = boardScreenContainer.querySelector('div[se-elem="boardMap"]'),
		//
		//
		resupplySumary = plugElem.querySelector('#resupplySumary'),
		//
		cardOps = plugElem.querySelector('#cardOps'),
		cardOpsForm = cardOps.querySelector('form'),
		//
		bombingRunDialog = plugElem.querySelector('#bombingRunDialog'),
		bombingRunForm = bombingRunDialog.querySelector('form'),
		//
		endOfTurnSummary = plugElem.querySelector('#endOfTurnSummary'),
		//
		resupplyLocation = plugElem.querySelector('#resupplyLocation'),
		resupplyLocationForm = resupplyLocation.querySelector('form'),
		resupplyLocationRange = resupplyLocation.querySelector('input[type="range"]'),
		resupplyLocationNumber = resupplyLocation.querySelector('input[type="number"]'),
		//
		unitsMovement = plugElem.querySelector('#unitsMovement'),
		unitsMovementForm = unitsMovement.querySelector('form'),
		unitsMovementRange = unitsMovement.querySelector('input[type="range"]'),
		unitsMovementNumber = unitsMovement.querySelector('input[type="number"]'),
		//
		battleGrounds = plugElem.querySelector('#battleGrounds'),
		battleGroundsWarnBattleAir = battleGrounds.querySelector('div[se-elem="battle_air_warning"]'),
		battleGroundsFormBattleAir = battleGrounds.querySelector('form[se-elem="battle_air"]'),
		battleGroundsFormBattleLand = battleGrounds.querySelector('form[se-elem="battle_land"]')
		;

	//
	function init() {
		console.log("ROSK loaded");

		// Bindings

		// Dialog - Resupply
		resupplyLocationRange.se_on('input', user_resupply_form_slider_move);
		resupplyLocationNumber.se_on('change', user_resupply_form_input_change);
		resupplyLocationForm.se_on('submit', user_resupply_form_accept);
		// Dialog - Movement
		unitsMovementRange.se_on('input', user_movement_form_slider_move);
		unitsMovementNumber.se_on('change', user_movement_form_input_change);
		unitsMovementForm.se_on('submit', user_movement_form_accept);
		// Dialog - Battle
		battleGroundsFormBattleAir.se_on('change', 'input', battle_aereal_change);
		battleGroundsFormBattleAir.se_on('submit', battle_aereal_exec);
		battleGroundsFormBattleLand.se_on('change', 'input', battle_land_change);
		battleGroundsFormBattleLand.se_on('submit', battle_land_exec);
		// Cards
		cardOpsForm.se_on('submit', user_card_exec);
		bombingRunForm.se_on('submit', user_card_bombing_exec);
		// Map
		boardMap.se_on('mouseenter', '.country_group', mapSectionEnter);
		boardMap.se_on('mouseleave ', '.country_group', mapSectionLeave);
		boardMap.se_on('click', '.country_group', mapSectionClick);
		// General
		plugElem.se_on('click', 'button[se-act]', userAction);
		// User QR
		userQRDialog.se_plugin('pseudoDialog');
		qrGenerator = new QRCode(qrObject, {});

		// Startup


		// Get current board status
		boardGames.func.loadBoard(boardName, properties.gameId, properties.gameKey,
			(msg) => {
				console.log("RECIEVED DATA", msg);
				// First time load
				if ( msg.statusGeneral === 1 ) {
					// Setup initial conditions
					fase_setup_refresh(msg.players);
				}
				else {
					// Load from server
					gameStatus = msg.gameData;
				}

				//
				console.log("BOARD STATUS", gameStatus);

				// Advance to last saved status
				board_status_apply();
				riskUpdateMapConditions();
				user_turn_apply(false);

				// MQTT
				mqtt_connector.connect('desktop', properties.gameId, mqttMessageIn, mqttReportUpdate);
			},
			() => {
				alert("el juego no cargó");
			}
		);

	}

	//
	function mqttMessageIn(msgHeader, msgData) {
		console.log("msgin", msgHeader);
		//
		switch ( msgHeader[2] ) {
			//
			case 'userOp':
				switch ( msgHeader[3] ) {
					//
					case 'discard':
						if ( !msgData.card ) {
							console.error("datos no válidos", msgData);
							return;
						}
						//
						user_card_discard(msgData.userId, msgData.card);
						break;
					//
					default:
						console.error("incomming message not valid user op", msgHeader);
						break;
				}
				break;
			//
			default:
				console.error("incomming message not valid", msgHeader);
				break;
		}
	}

	//
	function mapSectionEnter(e, cEl) {
		e.preventDefault();

		//
		switch ( gameStatus.status.fase ) {
			//
			case 'placementUser':
			case 'firstRoundResupply':
			case 'mainGame':
				console.log("remember showing what country can be attacked");
				break;
			//
			default:
				console.log("country clicked, no operation", cEl);
				break;
		}

	}

	//
	function mapSectionLeave(e, cEl) {
		e.preventDefault();

		//
		switch ( gameStatus.status.fase ) {
			//
			case 'placementUser':
			case 'firstRoundResupply':
			case 'mainGame':
				console.log("remove all notifications of possible attack");
				break;
			//
			default:
				console.log("country clicked, no operation", cEl);
				break;
		}

	}

	//
	function mapSectionClick(e, cEl) {
		e.preventDefault();

		let locationId = cEl.id;

		//
		switch ( gameStatus.status.fase ) {
			//
			case 'placementUser':
				fase_placementUser_country_select(locationId);
				break;
			//
			case 'firstRoundResupply':
				user_resupply_location_select(locationId);
				break;
			//
			case 'firstRoundCombat':
				//
				switch ( gameStatus.status.userStep ) {
					case 'combat':
						user_combat_location_select(locationId);
						break;
					case 'movement_ini':
					case 'movement_end':
					case 'movement_end_extra':
						user_movement_location_select(locationId);
						break;
				}
				break;
			//
			case 'mainGame':
				//
				switch ( gameStatus.status.userStep ) {
					case 'resupply':
						user_resupply_location_select(locationId);
						break;
					case 'combat':
						if ( gameStatus.status.turnInfo.cardMode ) {
							user_card_eval(locationId);
						} else {
							user_combat_location_select(locationId);
						}
						break;
					case 'movement_ini':
					case 'movement_end':
					case 'movement_end_extra':
						user_movement_location_select(locationId);
						break;
				}
				break;
		}

	}

	//
	function playerSet(players) {
		//
		let cPlayers = [],
			cSectors = {};

		// Prepare general properties
		for ( const [cSector, cProperties] of Object.entries(boardGames.risk.properties.map.sectors) ) {
			cSectors[cSector] = {
				hasMajority:false,
				hasOwnership:false,
				territories: 0
			};
		}

		// Loop for elements
		for ( let cEl of players ) {
			//
			cPlayers.push({
				props:{
					id: cEl.id,
					iKey: cEl.iKey,
					name: cEl.name,
					color: cEl.color,
				},
				data: {
					isAlive:true,
					hasCapital:true,
					totalTerritories:0,
					totalCapitals:0,
					totalBases:0,
					totalUnits:0,
					totalSectors:0,
					totalPlanes:0,
					sectors:se.object.copy(cSectors),
					cards:[],
					bonuses:[],
					objective:-1
				}
			});
		}

		//
		if ( cPlayers.length < 3 ) {
			alert("cantidad de jugadores insuficientes");
			return;
		}

		// Update status
		gameStatus.players = se.object.copy(cPlayers);
	}

	//
	function resetGame() {
		gameStatus = se.object.copy(boardGames.risk.initialStatus);
		gameStatus_save();
		board_status_apply();
		// Send update
		mqttReportUpdate()
	}

	//
	function board_newFase_set(faseName) {
		// Set parameters
		gameStatus.status.fase = faseName;
		gameStatus.status.faseRounds = 0;
		gameStatus.status.userTurn = 0;

		// Update board
		board_status_apply();

		//
		console.log("GAME - NEW FASE: ", faseName);
	}

	//
	function board_status_apply() {
		//
		if ( gameStatus.status.fase !== 'start' ) {
			user_print_list();
		}

		//
		switch ( gameStatus.status.fase ) {
			//
			case 'placement':
				board_switch_screen('riskBoard');
				fase_placement_reset();
				break;
			//
			case 'placementUser':
				board_switch_screen('riskBoard');
				break;
			//
			case 'firstRoundResupply':
				board_switch_screen('riskBoard');
				break;
			//
			case 'firstRoundCombat':
				board_switch_screen('riskBoard');
				break;
			//
			case 'mainGame':
				board_switch_screen('riskBoard');
				break;
			//
			default:
				console.error("learn to code, game status fase not defined...", gameStatus);
				break;
		}
	}

	//
	function board_notification_set(text) {
		liveConsole.se_text(text);
		// Fade out
		setTimeout(() => {
			liveConsole.se_text('');
		}, 3000)
	}

	//
	function board_switch_screen(screenName) {
		let targetScreen = boardScreenContainer.querySelector('div[data-screen-name="' + screenName + '"]');
		//
		if ( !targetScreen ) {
			console.error("wrong board?", screenName);
			return;
		}

		// Set current fase, for menus
		targetScreen.se_data('fase', gameStatus.status.fase);

		// Check if selected and skip if it is
		if ( targetScreen.se_attr('aria-hidden') === 'false' ) {
			return;
		}
		//
		console.log("SWITCHING TO", screenName, targetScreen);
		//
		boardScreenContainer.querySelectorAll('* > div[data-screen-name]').se_attr('aria-hidden', 'true');
		targetScreen.se_attr('aria-hidden', 'false');
	}

	//
	function user_print_list() {
		userPlayerTableContent.se_empty();
		let tableStructure = userPlayerTableTemplate.se_html();
		for ( let userIndex in gameStatus.players ) {
			if ( !gameStatus.players.hasOwnProperty(userIndex) ) { continue; }

			//
			userPlayerTableContent.se_append(se.struct.stringPopulate(tableStructure, {
				userId:gameStatus.players[userIndex].props.id,
				userKey:gameStatus.players[userIndex].props.iKey,
				playerColor:gameStatus.players[userIndex].props.color,
				playerName:gameStatus.players[userIndex].props.name,
				//
				isAlive:(gameStatus.players[userIndex].data.isAlive) ? 1 : 0,
				totalTerritories:gameStatus.players[userIndex].data.totalTerritories,
				totalUnits:gameStatus.players[userIndex].data.totalUnits,
				totalPlanes:gameStatus.players[userIndex].data.totalPlanes,
				totalCapitals:gameStatus.players[userIndex].data.totalCapitals,
				totalBases:gameStatus.players[userIndex].data.totalBases,
				hasCapital:( gameStatus.players[userIndex].data.hasCapital ) ? 1 : 0,

			}));
		}

		//
	}

	//<editor-fold desc="Fase Placement">

	//
	function fase_setup_refresh(setPlayers = null) {
		// Load default board
		gameStatus = se.object.copy(boardGames.risk.initialStatus);

		// Merge board with players
		playerSet(setPlayers);

		//
		let totalObjectives = boardGames.risk.properties.objectives.length,
			objectiveArray = [];

		//
		for ( let i = 0; i < totalObjectives; i++ ) {
			objectiveArray.push(i);
		}

		//
		objectiveArray = se.array.shuffle(objectiveArray);

		// Assign objectives
		for ( let userIndex in gameStatus.players ) {
			if ( !gameStatus.players.hasOwnProperty(userIndex) ) { continue; }

			//
			gameStatus.players[userIndex].data.objective = objectiveArray.shift();
		}

		console.log("Objectives set", gameStatus.players);

		// Mix deck
		let totalCards = boardGames.risk.properties.cards.length,
			cardsArray = [];

		//
		for ( let i = 0; i < totalCards; i++ ) {
			cardsArray.push(i);
		}

		//
		gameStatus.cards.stack = se.array.shuffle(cardsArray);

		//
		console.log("cards shuffle result: ", gameStatus.cards.stack);

		// Mix sideObjectives with bonuses
		let totalSideObjectives = boardGames.risk.properties.sideObjectives.length,
			sideObjArrayHard = [],
			sideObjArrayEasy = [],
			totalBonuses = boardGames.risk.properties.bonuses.length,
			bonusesArrayHard = [],
			bonusesArrayEasy = [];

		//
		for ( let i = 0; i < totalSideObjectives; i++ ) {
			if ( boardGames.risk.properties.sideObjectives[i].difficulty === 'low' ) {
				sideObjArrayEasy.push(i);
			} else {
				sideObjArrayHard.push(i);
			}
		}
		//
		for ( let i = 0; i < totalBonuses; i++ ) {
			if ( boardGames.risk.properties.bonuses[i].difficulty === 'low' ) {
				bonusesArrayEasy.push(i);
			} else {
				bonusesArrayHard.push(i);
			}
		}

		let totalEasy = Math.min(sideObjArrayEasy.length, bonusesArrayEasy.length),
			totalHard = Math.min(sideObjArrayHard.length, bonusesArrayHard.length);

		// Pre shuffle
		sideObjArrayEasy = se.array.shuffle(sideObjArrayEasy);
		sideObjArrayHard = se.array.shuffle(sideObjArrayHard);
		bonusesArrayEasy = se.array.shuffle(bonusesArrayEasy);
		bonusesArrayHard = se.array.shuffle(bonusesArrayHard);

		//
		for ( let i = 0; i < totalEasy; i++ ) {
			gameStatus.sideObjectives.push({
				owner:-1,
				sideObjective:sideObjArrayEasy.shift(),
				bonus:bonusesArrayEasy.shift(),
			});
		}
		//
		for ( let i = 0; i < totalHard; i++ ) {
			gameStatus.sideObjectives.push({
				owner:-1,
				sideObjective:sideObjArrayHard.shift(),
				bonus:bonusesArrayHard.shift(),
			});
		}

		console.log("INITIAL CONDITION SET");
	}

	//</editor-fold>

	//<editor-fold desc="End of turn conditions">

	//
	function user_eval_endOfTurn() {
		console.debug("evaluating end of turn");

		//
		se.dialog.openModal(endOfTurnSummary);

		//
		let resultList = endOfTurnSummary.querySelector('table[se-elem="resultList"]'),
			resultTemplateHTML = resultList.querySelector('template').se_html(),
			resultContent = resultList.querySelector('tbody[se-elem="content"]');

		// Reset results previous to action
		resultContent.se_empty();

		if ( !gameStatus.status.turnInfo.hasOwnProperty('results') || !gameStatus.status.turnInfo.results.captureLocations ) {
			console.debug("nothing to see...");
			resultContent.se_append(se.struct.stringPopulate(resultTemplateHTML, {
				icon:'#fa-pied-piper-alt',
				text:'No hay cambios'
			}));
			return;
		}

		// Give cards?
		if ( gameStatus.status.turnInfo.results.captureLocations ) {
			// Check if it needs shuffling
			if ( gameStatus.cards.stack.length < 3 ) {
				let tempArray = gameStatus.cards.stack.concat(gameStatus.cards.discard);
				gameStatus.cards.stack = se.array.shuffle(tempArray);
				gameStatus.cards.discard = [];
			}

			//
			gameStatus.players[gameStatus.status.userTurn].data.cards.push(gameStatus.cards.stack.shift());
			gameStatus.players[gameStatus.status.userTurn].data.cards.push(gameStatus.cards.stack.shift());
			gameStatus.players[gameStatus.status.userTurn].data.cards.push(gameStatus.cards.stack.shift());

			//
			console.debug("tarjetas");
			resultContent.se_append(se.struct.stringPopulate(resultTemplateHTML, {
				icon:'#fa-file',
				text:'Tarjetas'
			}));
		}

		let cPlayerStatus = gameStatus.players[gameStatus.status.userTurn];

		// Give extra card?
		if ( cPlayerStatus.data.bonuses.includes('security_card') ) {
			// Check if it needs shuffling
			if ( gameStatus.cards.stack.length < 1 ) {
				let tempArray = gameStatus.cards.stack.concat(gameStatus.cards.discard);
				gameStatus.cards.stack = se.array.shuffle(tempArray);
				gameStatus.cards.discard = [];
			}

			//
			gameStatus.players[gameStatus.status.userTurn].data.cards.push(gameStatus.cards.stack.shift());

			//
			console.debug("tarjeta extra");
			resultContent.se_append(se.struct.stringPopulate(resultTemplateHTML, {
				icon:'#fa-file',
				text:'Tarjeta de seguridad'
			}));
		}

		// Check for objective bonus
		for ( const [cIndex, cSideObjList] of Object.entries(gameStatus.sideObjectives) ) {
			if ( cSideObjList.owner !== -1 ) { continue; }
			// Check objective value
			let cSideObj = boardGames.risk.properties.sideObjectives[cSideObjList.sideObjective],
				cBonus = boardGames.risk.properties.bonuses[cSideObjList.bonus],
				reclaim = false;

			//
			switch ( cSideObj.type ) {
				//
				case 'sector':
					if ( cPlayerStatus.data.sectors[cSideObj.value].hasOwnership ) {
						reclaim = true;
					}
					break;
				//
				case 'bases':
					if ( cPlayerStatus.data.totalBases >= cSideObj.value ) {
						reclaim = true;
					}
					break;
				//
				case 'capitals':
					if ( cPlayerStatus.data.totalCapitals >= cSideObj.value ) {
						reclaim = true;
					}
					break;
				//
				case 'locations':
					if ( cPlayerStatus.data.totalTerritories >= cSideObj.value ) {
						reclaim = true;
					}
					break;
				//
				case 'locationsCapture':
					if ( gameStatus.status.turnInfo.results.captureLocations >= cSideObj.value ) {
						reclaim = true;
					}
					break;
				//
				case 'sectorsTotal':
					if ( cPlayerStatus.data.totalSectors >= cSideObj.value ) {
						reclaim = true;
					}
					break;
				//
				default:
					console.error("type no definido? wtf", cSideObj);
					break;
			}

			//
			if ( reclaim ) {
				gameStatus.players[gameStatus.status.userTurn].data.bonuses.push(cBonus.action);
				gameStatus.sideObjectives[cIndex].owner = gameStatus.players[gameStatus.status.userTurn].props.color;

				//
				resultContent.se_append(se.struct.stringPopulate(resultTemplateHTML, {
					icon:'#fa-ticket',
					text:'Objetivo parcial: ' + cSideObj.text + ' ⇒ ' + cBonus.text
				}));
			}
		}

		// Update report
		riskUpdateMapConditions();

		// Check winning conditions
		let cMainObj = boardGames.risk.properties.objectives[cPlayerStatus.data.objective];

		console.log("CHECKING END CONDITION", cMainObj, gameStatus);

		// Check capital
		if ( !cPlayerStatus.data.hasCapital ) { return; }

		// Sector check
		if ( cMainObj.sectors.length ) {
			//
			for ( let cStep of cMainObj.sectors ) {
				let cSector = cPlayerStatus.data.sectors[cStep.name];
				//
				if ( cStep.type === 1 && !cSector.hasMajority ) {
					return;
				} else if ( cStep.type === 2 && !cSector.hasOwnership ) {
					return;
				}
			}
		}

		// Locations check
		if ( cMainObj.locations.length ) {
			for ( let cStep of cMainObj.locations ) {
				let cLocation = gameStatus.locations[cStep.name];
				if ( ( cLocation.owner !== gameStatus.status.userTurn ) || ( cLocation.units < cStep.units ) ) {
					return;
				}
			}
		}

		// Total check
		if ( cMainObj.totals.length ) {
			for ( let cStep of cMainObj.totals ) {
				let countValid = 0;
				//
				switch ( cStep.type ) {
					//
					case 'capitals':
						// Check current step
						if ( cPlayerStatus.data.totalCapitals < cStep.count ) {
							return;
						}

						//
						for ( const [cLocation, cProperties] of Object.entries(gameStatus.locations) ) {
							if ( cProperties.owner === gameStatus.status.userTurn && cProperties.isCapital && cProperties.units >= cStep.units ) {
								countValid++;
							}
						}

						// Not enough units?
						if ( countValid < cStep.count ) {
							return;
						}
						break;
					//
					case 'locations':
						// Check current step
						if ( cPlayerStatus.data.totalTerritories < cStep.count ) {
							return;
						}

						//
						for ( const [cLocation, cProperties] of Object.entries(gameStatus.locations) ) {
							if ( cProperties.owner === gameStatus.status.userTurn && cProperties.units >= cStep.units ) {
								countValid++;
							}
						}

						if ( countValid < cStep.count ) {
							return;
						}
						break;
					//
					case 'bases':
						// Check current step
						if ( cPlayerStatus && cPlayerStatus.data.totalBases < cStep.count ) {
							return;
						}

						//
						for ( const [cLocation, cProperties] of Object.entries(gameStatus.locations) ) {
							if ( cProperties.owner === gameStatus.status.userTurn && cProperties.isBaseFor !== -1 && cProperties.units >= cStep.units ) {
								countValid++;
							}
						}

						if ( countValid < cStep.count ) {
							return;
						}
						break;
				}
			}
		}

		// Set as winner
		console.log("user conditions passed");

		// Show current user information
		se.element.childrenUpdate(endOfGame, {
			'.userInformation':[
				['data', 'side', gameStatus.players[gameStatus.status.userTurn].props.color],
				['text', gameStatus.players[gameStatus.status.userTurn].props.name]
			]
		});

		// Cancel end of turn, show end of game
		se.dialog.close(endOfTurnSummary);
		se.dialog.openModal(endOfGame);

	}

	//</editor-fold>

	//<editor-fold desc="Fase Placement">

	//
	function fase_placement_reset() {
		let capitalList = [],
			userCount = gameStatus.players.length;

		//
		for ( const [cLocation, cProperties] of Object.entries(boardGames.risk.properties.map.locations) ) {
			if ( cProperties.capital ) {
				capitalList.push(cLocation);
			}
		}

		// Reset current map
		for ( let locationName in gameStatus.locations ) {
			if ( !gameStatus.locations.hasOwnProperty(locationName) ) { continue; }
			gameStatus.locations[locationName].owner = -1;
			gameStatus.locations[locationName].units = -1;
			gameStatus.locations[locationName].isCapital = false;
		}

		//
		se.array.shuffle(capitalList);

		//
		let cCount = 0,
			cUserIndex = 0;
		for ( let cLocation of capitalList ) {
			let unitTotal = ( cCount < userCount * 2 ) ? 2 : 1;

			// Set capital
			gameStatus.locations[cLocation].isCapital = true;

			// Reset user count if required
			if ( cUserIndex >= userCount ) {
				cUserIndex = 0;
			}

			// Base check (only here)
			if ( cCount < userCount ) {
				gameStatus.locations[cLocation].isBaseFor = cUserIndex;
				boardMap.querySelector('#' + cLocation + '_bunker').se_data('side', gameStatus.players[cUserIndex].props.color);
			} else {
				gameStatus.locations[cLocation].isBaseFor = -1;
				boardMap.querySelector('#' + cLocation + '_bunker').se_data('side', 0);
			}


			//
			riskLocationSet(cUserIndex, cLocation, unitTotal, false);

			//
			cUserIndex++;
			cCount++;
		}

		//
		riskUpdateMapConditions();
	}

	//
	function fase_placement_accept() {
		// Change fase
		board_newFase_set('placementUser');

		// First user turn
		user_turn_apply(true);
	}

	//</editor-fold>

	//<editor-fold desc="Fase placementUser">

	//
	function fase_placementUser_country_select(cLocation) {
		//
		if ( gameStatus.locations[cLocation].owner !== -1 ) {
			board_notification_set("Territorio ya ocupado.")
			return;
		}

		// Set domain
		riskLocationSet(gameStatus.status.userTurn, cLocation, 1, true);

		// Check if finished
		for ( const [cLocation, cProperties] of Object.entries(gameStatus.locations) ) {
			if ( cProperties.owner === -1 ) {
				user_turn_next();
				return;
			}
		}

		//
		board_notification_set("Juego listo para iniciar.")
	}

	//
	function fase_placementUser_reset() {
		// Reset current map
		for ( let locationName in gameStatus.locations ) {
			// Exists and ignore capitals
			if ( !gameStatus.locations.hasOwnProperty(locationName) || gameStatus.locations[locationName].isCapital ) { continue; }
			//
			gameStatus.locations[locationName].owner = -1;
			gameStatus.locations[locationName].units = -1;
		}

		//
		user_turn_reset();
		riskUpdateMapConditions();
		gameStatus_save();
	}

	//
	function fase_placementUser_random() {
		let availableLocations = [];
		// Reset current map
		for ( let locationName in gameStatus.locations ) {
			// Exists and ignore capitals
			if ( !gameStatus.locations.hasOwnProperty(locationName) || gameStatus.locations[locationName].isCapital ) { continue; }

			//
			availableLocations.push(locationName);
		}

		//
		availableLocations = se.array.shuffle(availableLocations);

		//
		let cPlayersLength = gameStatus.players.length,
			cPlayer = 0,
			cLoc = null;

		//
		while ( availableLocations.length ) {
			if ( cPlayer >= cPlayersLength ) {
				cPlayer = 0;
			}

			//
			cLoc = availableLocations.shift();

			//
			gameStatus.locations[cLoc].owner = cPlayer;
			gameStatus.locations[cLoc].units = 1;

			cPlayer++;
		}

		//
		riskUpdateMapConditions();
		gameStatus_save();
	}

	//
	function fase_placementUser_accept() {
		console.log("check");

		//
		for ( const [cLocation, cProperties] of Object.entries(gameStatus.locations) ) {
			if ( cProperties.owner === -1 ) {
				alert("no se han terminado de poner las piezas");
				return;
			}
		}

		//
		board_newFase_set('firstRoundResupply');

		// First user turn in new fase
		user_turn_apply(true);
	}


	//</editor-fold>

	//<editor-fold desc="Resupply operations">

	//
	function user_resupply_location_select(cLocation) {
		// gameStatus.players[gameStatus.status.userTurn]
		//
		if ( gameStatus.locations[cLocation].owner !== gameStatus.status.userTurn ) {
			board_notification_set("Territorio no pertenece al usuario.");
			return;
		}

		// Obtain maximum units
		let cSector = boardGames.risk.properties.map.locations[cLocation].sector,
			maxUnits = 0;

		console.log("attemping resupply", cSector, gameStatus.status.turnInfo);

		// Normal units
		maxUnits+= gameStatus.status.turnInfo.supplyContent.units;

		// Continent units
		if ( gameStatus.status.turnInfo.supplyContent.sectors[cSector] !== undefined ) {
			maxUnits += gameStatus.status.turnInfo.supplyContent.sectors[cSector];
		}

		//
		if ( maxUnits === 0 ) {
			board_notification_set("Usuario no cuenta con unidades para este territorio.")
			return;
		}

		//
		resupplyLocation.querySelector('div.locationName').se_text(boardGames.risk.properties.map.locations[cLocation].name);

		// Set location
		resupplyLocationForm.se_formElVal('location', cLocation);

		// Set
		resupplyLocationRange.se_attr('max', maxUnits);
		resupplyLocationNumber.se_attr('max', maxUnits);

		// Determine maximum units
		resupplyLocationRange.value = maxUnits;
		resupplyLocationNumber.value = maxUnits;

		//
		se.dialog.openModal(resupplyLocation);
	}

	//
	function user_resupply_start() {
		if ( gameStatus.status.userStep !== 'recieve' ) {
			console.warn("avoided duplicate reception");
			return;
		}

		// Definiciones iniciales
		let cPlayer = gameStatus.players[gameStatus.status.userTurn],
			curOrder = {
				jets:0,
				units:0,
				sectors:{}
			};

		//
		se.dialog.openModal(resupplySumary);

		// Show current user information
		se.element.childrenUpdate(resupplySumary, {
			'.userInformation':[
				['data', 'side', gameStatus.players[gameStatus.status.userTurn].props.color],
				['text', gameStatus.players[gameStatus.status.userTurn].props.name]
			]
		});

		//

		let supplyList = resupplySumary.querySelector('table[se-elem="suplyList"]'),
			supplyTemplateHTML = supplyList.querySelector('template').se_html(),
			supplyContent = supplyList.querySelector('tbody[se-elem="content"]');

		//
		supplyContent.se_empty();

		// Jets
		curOrder.jets = intVal(cPlayer.data.totalBases);
		gameStatus.players[gameStatus.status.userTurn].data.totalPlanes+= curOrder.jets;
		//
		supplyContent.se_append(se.struct.stringPopulate(supplyTemplateHTML, {
			icon:'#fa-fighter-jet',
			type:'Jets',
			ammount:curOrder.jets,
		}));

		// General units
		curOrder.units+= Math.floor(cPlayer.data.totalTerritories / 2);
		curOrder.units+= intVal(cPlayer.data.totalCapitals);

		console.log("add units", cPlayer.data.totalTerritories, cPlayer.data.totalCapitals);

		//
		supplyContent.se_append(se.struct.stringPopulate(supplyTemplateHTML, {
			icon:'#fa-user',
			type:'Generales',
			ammount:curOrder.units,
		}));

		// Extra bonus units?
		if ( gameStatus.players[gameStatus.status.userTurn].data.bonuses.includes('units_extra') ) {
			curOrder.units+= 2;

			//
			supplyContent.se_append(se.struct.stringPopulate(supplyTemplateHTML, {
				icon:'#fa-user',
				type:'Unidades bonus',
				ammount:2,
			}));
		}

		//
		console.log("CHECKING USER SECTORS", cPlayer, cPlayer.sectors);

		//
		if ( cPlayer.data.sectors ) {
			for ( const [cSector, cProperties] of Object.entries(cPlayer.data.sectors) ) {
				//
				if (cProperties.hasMajority) {
					curOrder.sectors[cSector] = boardGames.risk.properties.map.sectors[cSector].bonus_majority;
					//
					supplyContent.se_append(se.struct.stringPopulate(supplyTemplateHTML, {
						icon: '#fa-user',
						type: 'Mayoría ' + boardGames.risk.properties.map.sectors[cSector].name,
						ammount: curOrder.sectors[cSector],
					}));
				} else if (cProperties.hasOwnership) {
					curOrder.sectors[cSector] = boardGames.risk.properties.map.sectors[cSector].bonus_complete;
					//
					supplyContent.se_append(se.struct.stringPopulate(supplyTemplateHTML, {
						icon: '#fa-user',
						type: 'Completo ' + boardGames.risk.properties.map.sectors[cSector].name,
						ammount: curOrder.sectors[cSector],
					}));
				}
			}
		}

		// Auto descartar
		if ( cPlayer.data.cards.length > 5 ) {
			let removeXCards = cPlayer.data.cards.length - 5;

			// shuffle
			cPlayer.data.cards = se.array.shuffle(cPlayer.data.cards);

			// remove cards
			for ( let i = 0; i < removeXCards; i++ ) {
				gameStatus.cards.discard.push(gameStatus.players[gameStatus.status.userTurn].data.cards.shift());
			}

			//
			supplyContent.se_append(se.struct.stringPopulate(supplyTemplateHTML, {
				icon:'#fa-file',
				type:'Tarjetas auto descartadas',
				ammount:removeXCards,
			}));
		}

		//
		console.log("resupply order", curOrder);

		// Save current supply list
		gameStatus.status.turnInfo.supplyContent = curOrder;
	}

	//
	function user_resupply_form_slider_move() {
		resupplyLocationNumber.value = resupplyLocationRange.value;
	}

	//
	function user_resupply_form_input_change() {
		resupplyLocationRange.value = resupplyLocationNumber.value;
	}

	//
	function user_resypply_complete_check() {
		let complete = true,
			comment = '';

		//
		if ( gameStatus.status.turnInfo.supplyContent.units ) {
			complete = false;
			comment+= "Unidades generales disponibles: " + gameStatus.status.turnInfo.supplyContent.units + "\n";
		}

		// Sector check
		for ( const [cSector, cCount] of Object.entries(gameStatus.status.turnInfo.supplyContent.sectors) ) {
			if ( cCount ) {
				complete = false;
				comment+= "Unidades en " + boardGames.risk.properties.map.sectors[cSector].name + ": " + cCount + "\n";
			}
		}

		// Notificar en caso de estar incompleto
		if ( !complete ) {
			alert(comment);
		}

		//
		return complete;
	}

	//
	function user_resupply_form_accept(e) {
		e.preventDefault();

		let cLocation = resupplyLocationForm.se_formElVal('location'),
			unitAmmount = intVal(resupplyLocationForm.se_formElVal('ammount')),
			unitPending = unitAmmount;

		//
		se.dialog.close(resupplyLocation);

		// Current location sector
		let cSector = boardGames.risk.properties.map.locations[cLocation].sector;

		// Check if it has sector bonus
		if (
			gameStatus.status.turnInfo.supplyContent.sectors.hasOwnProperty(cSector) &&
			gameStatus.status.turnInfo.supplyContent.sectors[cSector] !== 0
		) {
			if ( gameStatus.status.turnInfo.supplyContent.sectors[cSector] <= unitPending ) {
				unitPending-= gameStatus.status.turnInfo.supplyContent.sectors[cSector];
				gameStatus.status.turnInfo.supplyContent.sectors[cSector] = 0;
			} else {
				gameStatus.status.turnInfo.supplyContent.sectors[cSector]-= unitPending;
				unitPending = 0;
			}
		}

		// Need extra bonus?
		if ( unitPending ) {
			gameStatus.status.turnInfo.supplyContent.units-= unitPending;
		}

		//
		gameStatus.locations[cLocation].units+= unitAmmount;
		riskUpdateMapConditions();
		gameStatus_save();

		//
		console.log("final unit accounting", gameStatus.status.turnInfo.supplyContent.units, gameStatus.status.turnInfo.supplyContent.sectors);

		// Check if has finished placing units
		if ( gameStatus.status.turnInfo.supplyContent.units === 0 ) {
			let advance = false;
			// Check continental bonuses
			if ( !gameStatus.status.turnInfo.supplyContent.sectors ) {
				// Advance
				advance = true;

				//
				console.log("no bonus, advance");
			}
			else {
				let remainingUnits = 0;
				for ( const [cLocation, cCount] of Object.entries(gameStatus.status.turnInfo.supplyContent.sectors) ) {
					remainingUnits+= cCount;
				}

				//
				console.log("units in other sectors", remainingUnits);

				//
				if ( remainingUnits === 0 ) {
					console.log("units in other sectors are not, go");
					advance = true;
				}
			}

			//
			if ( advance ) {
				console.log("units in other sectors are not, go", gameStatus.status.userStep);
				user_step_next();
			}
		}

	}

	//</editor-fold>

	//<editor-fold desc="Movement">

	//
	function user_movement_location_select(cLocation) {
		if ( ! gameStatus.status.turnInfo.movement ) {
			//
			gameStatus.status.turnInfo.movement = {
				origin: null,
				target: null
			};
		}

		//
		console.debug("Movement mode", cLocation, gameStatus.status.turnInfo.movement);

		//
		if ( !gameStatus.status.turnInfo.movement.origin ) {
			//
			if ( gameStatus.locations[cLocation].owner !== gameStatus.status.userTurn ) {
				board_notification_set('Usuario no cuenta con unidades para este territorio');
				return;
			}

			//
			if ( gameStatus.locations[cLocation].units < 2 ) {
				board_notification_set('Unidades insuficientes.');
				return;
			}

			// Check for available fight locations
			let availableMovement = false;
			for ( let relLocName of boardGames.risk.properties.map.locations[cLocation].related ) {
				// skip different user locations
				if ( gameStatus.locations[relLocName].owner !== gameStatus.status.userTurn ) {
					continue;
				}

				availableMovement = true;

				//
				boardMap.querySelector('#' + relLocName + " .country").se_classAdd('candidate');
			}

			// Add if capital
			if ( gameStatus.locations[cLocation].isCapital ) {
				for ( const [relLocName, cLocProperties] of Object.entries(gameStatus.locations) ) {
					// skip different user locations
					if ( !cLocProperties.isCapital || cLocProperties.owner !== gameStatus.status.userTurn ) {
						continue;
					}

					availableMovement = true;

					//
					boardMap.querySelector('#' + relLocName + " .country").se_classAdd('candidate');
				}
			}

			// Allow or not movement
			if ( !availableMovement ) {
				board_notification_set('Movimiento no disponible');
				return;
			}

			// Set as current territory
			gameStatus.status.turnInfo.movement.origin = cLocation;
		} else {
			//
			if (
				!boardGames.risk.properties.map.locations[gameStatus.status.turnInfo.movement.origin].related.includes(cLocation) &&
				!gameStatus.locations[gameStatus.status.turnInfo.movement.origin].isCapital &&
				!gameStatus.locations[cLocation].isCapital
			) {
				board_notification_set('No existe ruta para el movimiento.');
				return;
			}

			//
			if ( gameStatus.locations[cLocation].owner !== gameStatus.status.userTurn ) {
				board_notification_set('Territorio no es del usuario.');
				return;
			}

			// Set as current territory
			gameStatus.status.turnInfo.movement.target = cLocation;

			// Set initial information
			let availableUnits = gameStatus.locations[gameStatus.status.turnInfo.movement.origin].units - 1;

			// Set
			unitsMovementRange.se_attr('max', availableUnits);
			unitsMovementNumber.se_attr('max', availableUnits);

			// Determine maximum units
			unitsMovementRange.value = availableUnits;
			unitsMovementNumber.value = availableUnits;

			// Display information
			let movementInfo = unitsMovement.querySelector('table.movementInfo');
			se.element.childrenUpdate(movementInfo, {
				'.territory td:first-child' : [
					['text', boardGames.risk.properties.map.locations[gameStatus.status.turnInfo.movement.origin].name]
				],
				'.territory td:last-child' : [
					['text', boardGames.risk.properties.map.locations[gameStatus.status.turnInfo.movement.target].name]
				],
				'.units td:first-child span' : [
					['text', gameStatus.locations[gameStatus.status.turnInfo.movement.origin].units ]
				],
				'.units td:last-child span' : [
					['text', gameStatus.locations[gameStatus.status.turnInfo.movement.target].units ]
				]
			});

			//
			se.dialog.openModal(unitsMovement);
			risk_map_clean();
		}
	}

	//
	function user_movement_form_slider_move() {
		unitsMovementNumber.value = unitsMovementRange.value;
	}

	//
	function user_movement_form_input_change() {
		unitsMovementRange.value = unitsMovementNumber.value;
	}

	//
	function user_movement_form_accept(e) {
		e.preventDefault();

		let unitAmmount = intVal(unitsMovementForm.se_formElVal('ammount'));

		// Move units
		gameStatus.locations[gameStatus.status.turnInfo.movement.origin].units-= unitAmmount;
		gameStatus.locations[gameStatus.status.turnInfo.movement.target].units+= unitAmmount;

		// Close information
		se.dialog.close(unitsMovement);
		gameStatus.status.turnInfo.movement.origin = null;
		gameStatus.status.turnInfo.movement.target = null;

		// Recalculate changes and save
		riskUpdateMapConditions();
		gameStatus_save();
		user_step_next();
	}

	//</editor-fold>

	//
	function risk_map_clean() {
		//
		boardMap.querySelectorAll(".country.selected").se_classDel('selected');
		boardMap.querySelectorAll(".country.candidate").se_classDel('candidate');
	}

	//<editor-fold desc="Battle">

	//
	function user_combat_location_select(cLocation) {
		if ( !gameStatus.status.turnInfo.battle ) {
			//
			gameStatus.status.turnInfo.battle = {
				origin: null,
				target: null
			};
		}

		//
		console.debug("COMBAT mode select", cLocation, gameStatus.status.turnInfo.battle);

		//
		if ( !gameStatus.status.turnInfo.battle.origin ) {
			//
			if ( gameStatus.locations[cLocation].owner !== gameStatus.status.userTurn ) {
				board_notification_set('El territorio no es del usuario.');
				return;
			}

			//
			if ( gameStatus.locations[cLocation].units < 2 ) {
				board_notification_set('No se cuentan con suficientes unidades.');
				return;
			}

			// Check for available fight locations
			let availableConflict = false;
			for ( let relLocName of boardGames.risk.properties.map.locations[cLocation].related ) {
				// skip same user locations
				if ( gameStatus.locations[relLocName].owner === gameStatus.status.userTurn ) {
					continue;
				}

				availableConflict = true;

				//
				boardMap.querySelector('#' + relLocName + " .country").se_classAdd('candidate');
			}

			if ( !availableConflict ) {
				board_notification_set('Territorio sin conexión.');
				return;
			}

			// Set as current territory
			gameStatus.status.turnInfo.battle.origin = cLocation;

			//
			boardMap.querySelector('#' + cLocation + " .country").se_classAdd('selected');
		} else {
			// Cancel if same
			if ( cLocation === gameStatus.status.turnInfo.battle.origin ) {
				gameStatus.status.turnInfo.battle.origin = null;
				risk_map_clean();
				return;
			}

			//
			if ( !boardGames.risk.properties.map.locations[gameStatus.status.turnInfo.battle.origin].related.includes(cLocation) ) {
				board_notification_set('No hay forma de atacar este territorio.');
				return;
			}

			//
			if ( gameStatus.locations[cLocation].owner === gameStatus.status.userTurn ) {
				board_notification_set('Territorio es del usuario.');
				return;
			}

			// Set as current territory
			gameStatus.status.turnInfo.battle.target = cLocation;

			//
			se.dialog.openModal(battleGrounds);

			// Set initial information

			let targetUserIndex = gameStatus.locations[gameStatus.status.turnInfo.battle.target].owner,
				battleInfo = battleGrounds.querySelector('table.battleInfo');

			se.element.childrenUpdate(battleInfo, {
				'.userName td:first-child' : [
					['text', gameStatus.players[gameStatus.status.userTurn].props.name],
					['data', 'side', gameStatus.players[gameStatus.status.userTurn].props.color]
				],
				'.userName td:last-child' : [
					['text', gameStatus.players[targetUserIndex].props.name],
					['data', 'side', gameStatus.players[targetUserIndex].props.color]
				],
				'.territory td:first-child' : [
					['text', boardGames.risk.properties.map.locations[gameStatus.status.turnInfo.battle.origin].name]
				],
				'.territory td:last-child' : [
					['text', boardGames.risk.properties.map.locations[gameStatus.status.turnInfo.battle.target].name]
				],
				'.units td:first-child span' : [
					['text', gameStatus.locations[gameStatus.status.turnInfo.battle.origin].units ]
				],
				'.units td:last-child span' : [
					['text', gameStatus.locations[gameStatus.status.turnInfo.battle.target].units ]
				],
				'.planes td:first-child span' : [
					['text', gameStatus.players[gameStatus.status.userTurn].data.totalPlanes ]
				],
			});

			//
			battle_switch_screen('pick');
			risk_map_clean();
		}
	}

	//
	function battle_close() {
		se.dialog.close(battleGrounds);
		gameStatus.status.turnInfo.battle.origin = null;
		gameStatus.status.turnInfo.battle.target = null;

		// Recalculate changes and save (they may not exist)
		riskUpdateMapConditions();
		gameStatus_save();
	}

	//
	function battle_aereal_pick() {
		battle_switch_screen('aereal');
		battle_aereal_setup();
	}

	//
	function battle_aereal_setup() {
		// First set ammount of planes (if 3 or more)
		let ammountOfPlanes = Math.min(gameStatus.players[gameStatus.status.userTurn].data.totalPlanes, 3),
			bombersOpt = battleGroundsFormBattleAir.querySelector('[se-elem="bombers"]');

		// Determinar si es posible pelear
		if ( gameStatus.locations[gameStatus.status.turnInfo.battle.target].units > 1 ) {
			battleGroundsWarnBattleAir.se_attr('aria-hidden', 'true');
			battleGroundsFormBattleAir.se_attr('aria-hidden', 'false');
		} else {
			battleGroundsWarnBattleAir.se_attr('aria-hidden', 'false');
			battleGroundsFormBattleAir.se_attr('aria-hidden', 'true');
		}

		//
		battleGroundsFormBattleAir.reset();

		//
		bombersOpt.querySelectorAll('label').se_attr('aria-hidden', "true");

		// Add plane options
		for ( let i = 0; i < ammountOfPlanes; i++ ) {
			let rVal = i + 1;
			bombersOpt.querySelectorAll('label[data-value="' + rVal + '"]').se_attr('aria-hidden', "false");
		}

		//
	}

	//
	function battle_aereal_change(e, cEl) {
		e.preventDefault();

		let bombersCount = intVal(battleGroundsFormBattleAir.se_formElVal('bombersCount')),
			defenseCount = intVal(battleGroundsFormBattleAir.se_formElVal('defenseCount'));

		// Deny option to get effect if cero
		if ( defenseCount >= bombersCount ) {
			battleGroundsFormBattleAir.se_formElVal('bombEffect', 0);
		}
	}

	//
	function battle_aereal_exec(e) {
		e.preventDefault();

		//
		let bombersCount = intVal(battleGroundsFormBattleAir.se_formElVal('bombersCount')),
			defenseCount = intVal(battleGroundsFormBattleAir.se_formElVal('defenseCount')),
			bombEffect = intVal(battleGroundsFormBattleAir.se_formElVal('bombEffect'));

		// Change effect
		// Deny option to get effect if cero
		if ( defenseCount >= bombersCount ) {
			bombEffect = 0;
		}

		// Remove bombers
		if ( defenseCount ) {
			gameStatus.players[gameStatus.status.userTurn].data.totalPlanes-= defenseCount;
			if ( gameStatus.players[gameStatus.status.userTurn].data.totalPlanes < 0 ) {
				gameStatus.players[gameStatus.status.userTurn].data.totalPlanes = 0;
			}
		}

		// Remove units
		if ( bombEffect ) {
			gameStatus.locations[gameStatus.status.turnInfo.battle.target].units -= bombEffect;
			if ( gameStatus.locations[gameStatus.status.turnInfo.battle.target].units < 1 ) {
				gameStatus.locations[gameStatus.status.turnInfo.battle.target].units = 1;
			}
		}

		// Update
		let battleInfo = battleGrounds.querySelector('table.battleInfo');

		//
		se.element.childrenUpdate(battleInfo, {
			'.units td:first-child span' : [
				['text', gameStatus.locations[gameStatus.status.turnInfo.battle.origin].units ]
			],
			'.units td:last-child span' : [
				['text', gameStatus.locations[gameStatus.status.turnInfo.battle.target].units ]
			],
			'.planes td:first-child span' : [
				['text', gameStatus.players[gameStatus.status.userTurn].data.totalPlanes ]
			],
		});

		// Reset and do cleanup
		battle_aereal_setup();
	}

	//
	function battle_land_setup() {
		// First set ammount of planes (if 3 or more)
		let unitsAttackAvailable = gameStatus.locations[gameStatus.status.turnInfo.battle.origin].units - 1,
			unitsDefenseAvailable = gameStatus.locations[gameStatus.status.turnInfo.battle.target].units,
			totalUnits = Math.max( 3, Math.min(unitsAttackAvailable, unitsDefenseAvailable) ),
			formUnitsAttack = battleGroundsFormBattleLand.querySelector('[se-elem="unitsAttack"]'),
			formEffect = battleGroundsFormBattleLand.querySelector('[se-elem="effect"]')
		;

		//
		battleGroundsFormBattleLand.reset();

		// Add attack options
		formUnitsAttack.querySelectorAll('label').se_attr('aria-hidden', "true");
		for ( let i = 0; i < unitsAttackAvailable; i++ ) {
			let rVal = i + 1;
			formUnitsAttack.querySelectorAll('label[data-value="' + rVal + '"]').se_attr('aria-hidden', "false");
		}

		// Add effect options
		formEffect.querySelectorAll('label').se_attr('aria-hidden', "true");
	}

	//
	function battle_land_pick() {
		battle_switch_screen('land');
		battle_land_setup();
	}

	//
	function battle_land_change(e, cEl) {
		e.preventDefault();

		//
		if ( cEl.name !== 'attackUnits' ) {
			return;
		}

		let unitsAttackAvailable = intVal(battleGroundsFormBattleLand.se_formElVal('attackUnits')),
			unitsDefenseAvailable = gameStatus.locations[gameStatus.status.turnInfo.battle.target].units,
			//
			totalUnits = Math.min( 3, Math.min(unitsAttackAvailable, unitsDefenseAvailable) ),
			formEffect = battleGroundsFormBattleLand.querySelector('[se-elem="effect"]')
		;

		//
		formEffect.querySelectorAll('label').se_attr('aria-hidden', "true");
		formEffect.querySelectorAll('label[data-value="' + totalUnits + '"]').se_attr('aria-hidden', "false");
	}

	//
	function battle_land_exec(e) {
		e.preventDefault();
		//
		let unitsAttackAvailable = intVal(battleGroundsFormBattleLand.se_formElVal('attackUnits')),
			unitsDefenseAvailable = gameStatus.locations[gameStatus.status.turnInfo.battle.target].units,
			//
			totalUnits = Math.min( 3, Math.min(unitsAttackAvailable, unitsDefenseAvailable) ),
			battleEffect = battleGroundsFormBattleLand.se_formElVal('battleEffect').split("-");

		//
		battleEffect[0] = intVal(battleEffect[0]);
		battleEffect[1] = intVal(battleEffect[1]);

		//
		if ( totalUnits !== (battleEffect[0] + battleEffect[1]) ) {
			console.error("WTF CANT SUM?", totalUnits, battleEffect);
			return;
		}

		// Remove attackers
		if ( battleEffect[0] ) {
			gameStatus.locations[gameStatus.status.turnInfo.battle.origin].units-= battleEffect[0];
		}

		// Remove defenders
		if ( battleEffect[1] ) {
			gameStatus.locations[gameStatus.status.turnInfo.battle.target].units -= battleEffect[1];

			// Transfer unit of surviving troops end battle
			if ( gameStatus.locations[gameStatus.status.turnInfo.battle.target].units <= 0 ) {
				// Transfer
				let survivors = unitsAttackAvailable - battleEffect[0],
					targetUserIndex = gameStatus.locations[gameStatus.status.turnInfo.battle.target].owner;

				//
				gameStatus.locations[gameStatus.status.turnInfo.battle.origin].units-= survivors;
				gameStatus.locations[gameStatus.status.turnInfo.battle.target].units = survivors;
				gameStatus.locations[gameStatus.status.turnInfo.battle.target].owner = gameStatus.status.userTurn;

				//
				if ( !gameStatus.status.turnInfo.hasOwnProperty('results') ) {
					gameStatus.status.turnInfo.results = {
						captureLocations:0,
						captureCapitals:0,
						captureBases:0
					};
				}
				//
				gameStatus.status.turnInfo.results.captureLocations++;
				if ( gameStatus.locations[gameStatus.status.turnInfo.battle.target].isCapital ) {
					gameStatus.status.turnInfo.results.captureCapitals++;
					if ( gameStatus.locations[gameStatus.status.turnInfo.battle.target].isBaseFor ) {
						gameStatus.status.turnInfo.results.captureBases++;
					}
				}

				// Check if player is eliminated (The territory count has not yet been updated, so one territory is dead)
				let targetUser = gameStatus.players[targetUserIndex];
				if ( targetUser.data.totalTerritories === 1 ) {
					// Player is now eliminated
					targetUser.data.isAlive = false;

					// Show current user information
					se.element.childrenUpdate(endOfUser, {
						'.userInformation':[
							['data', 'side', targetUser.props.color],
							['text', targetUser.props.name]
						]
					});

					//
					se.dialog.openModal(endOfUser);
				}

				// Close
				battle_close();
				return;
			}
		}

		// Update
		let battleInfo = battleGrounds.querySelector('table.battleInfo');

		//
		se.element.childrenUpdate(battleInfo, {
			'.units td:first-child span' : [
				['text', gameStatus.locations[gameStatus.status.turnInfo.battle.origin].units ]
			],
			'.units td:last-child span' : [
				['text', gameStatus.locations[gameStatus.status.turnInfo.battle.target].units ]
			],
		});



		// Reset and do cleanup
		battle_land_setup();
	}

	//
	function battle_switch_screen(screenName) {
		let targetScreen = battleGrounds.querySelector('div[data-screen-name="' + screenName + '"]');
		//
		if ( !targetScreen ) {
			console.error("wrong board?", screenName);
			return;
		}

		// Check if selected and skip if it is
		if ( targetScreen.se_attr('aria-hidden') === 'false' ) {
			return;
		}

		//
		console.log("SWITCHING TO", screenName, targetScreen);
		//
		battleGrounds.querySelectorAll('* > div[data-screen-name]').se_attr('aria-hidden', 'true')
		targetScreen.se_attr('aria-hidden', 'false');
	}

	//</editor-fold>

	//<editor-fold desc="Card play">

	//
	function user_card_discard(userId, cCardId) {
		console.log("discarding", userId, cCardId);
		cCardId = intVal(cCardId);
		let index = gameStatus.players[userId].data.cards.indexOf(cCardId);
		// Validate
		if ( index === -1 ) {
			console.error("no se encontró la tarjeta en el usuario", cCardId, gameStatus.players[userId].data.cards);
			return;
		}
		// Move card (remove, add)
		gameStatus.players[userId].data.cards.splice(index, 1);
		gameStatus.cards.discard.push(cCardId);

		//
		riskUpdateMapConditions();
		gameStatus_save();
	}

	//
	function user_card_play() {
		if ( !gameStatus.status.turnInfo.hasOwnProperty('cardMode') ) {
			//
			gameStatus.status.turnInfo.cardMode = false;
		}

		let playCardsBtn = plugElem.querySelector('button[se-act="playCards"]');

		//
		if ( gameStatus.status.turnInfo.cardMode ) {
			playCardsBtn.se_data('active', 0);
			gameStatus.status.turnInfo.cardMode = false;
		} else {
			playCardsBtn.se_data('active', 1);
			gameStatus.status.turnInfo.cardMode = true;
		}
	}

	//
	function user_card_eval(cLocation) {
		let applicableSectors = [],
			applicableLocations = [],
			applicableBonus = [],
			isValid = false,
			mode = '';

		// Check if territory can have any interaction
		if ( gameStatus.locations[cLocation].owner === gameStatus.status.userTurn ) {
			mode = 'resupply';
		} else {
			mode = 'airCombat';

			//
			for ( let neighBor of boardGames.risk.properties.map.locations[cLocation].related ) {
				let cNeighBor = gameStatus.locations[neighBor];
				if ( cNeighBor.owner === gameStatus.status.userTurn && cNeighBor.units > 1 && gameStatus.locations[cLocation].units > 1) {
					isValid = true;
					break;
				}
			}

			if ( !isValid ) {
				board_notification_set("No existe territorio listo para combate para este territorio o el territorio tiene una sola unidad, no aplicable.");
				return;
			}
		}

		// Check valid cards
		for ( let cIndex of gameStatus.players[gameStatus.status.userTurn].data.cards ) {
			let cardData = boardGames.risk.properties.cards[cIndex];

			//
			switch ( cardData.type ) {
				case 'sector':
					if ( boardGames.risk.properties.map.locations[cLocation].sector === cardData.value ) {
						applicableSectors.push(cIndex);
					}
					break;
				case 'location':
					if ( cLocation === cardData.value ) {
						applicableLocations.push(cIndex);
					}
					break;
				case 'additive':
					applicableBonus.push({cardId:cIndex, value:cardData.value});
					break;
				default:
					console.error("but how?");
					break;
			}
		}

		//
		if ( applicableLocations.length === 0 && applicableSectors.length === 0 ) {
			board_notification_set("No existen tarjetas aplicables para esta operación.");
			return;
		}

		let cardIdToUse = ( applicableLocations.length !== 0 ) ? applicableLocations[0] : applicableSectors[0];

		//
		se.dialog.openModal(cardOps);
		cardOpsForm.se_formElVal('cardId', cardIdToUse);
		cardOpsForm.se_formElVal('locId', cLocation);
		cardOpsForm.se_formElVal('mode', mode);
		//
		cardOps.querySelector('div.locationName').se_text(boardGames.risk.properties.map.locations[cLocation].name);
		cardOps.querySelector('.cardsIcon use').se_attr('xlink:href', ( mode === 'resupply' ) ? '#fa-user' : '#fa-fighter-jet');

		// Bonus cards
		let cardsList = cardOps.querySelector('div[se-elem="cards"]'),
			cardsTemplateHTML = cardsList.querySelector('template').se_html(),
			cardsContent = cardsList.querySelector('div[se-elem="content"]');

		console.log("bonus", applicableBonus);
		//
		cardsContent.se_empty();
		for ( let cCards of applicableBonus ) {
			console.log("card", cCards);
			cardsContent.se_append(se.struct.stringPopulate(cardsTemplateHTML, cCards));
		}
	}

	//
	function user_card_exec(e) {
		e.preventDefault();
		let formData = se.form.serializeObject(cardOpsForm);

		// Cards to remove
		let bonus = 1,
			cardsToRemove = [];

		// Get cards
		cardsToRemove.push(formData.cardId);
		for ( const [cardIndex, cardValue] of Object.entries(formData.card) ) {
			bonus += intVal(cardValue);
			cardsToRemove.push(cardIndex);
		}

		// Remove Cards
		for ( let cCardId of cardsToRemove ) {
			cCardId = intVal(cCardId);
			let index = gameStatus.players[gameStatus.status.userTurn].data.cards.indexOf(cCardId);
			// Validate
			if ( index === -1 ) {
				console.error("no se encontró la tarjeta en el usuario", cCardId, gameStatus.players[gameStatus.status.userTurn].data.cards);
				return;
			}
			// Move card (remove, add)
			gameStatus.players[gameStatus.status.userTurn].data.cards.splice(index, 1);
			gameStatus.cards.discard.push(cCardId);
		}

		//
		switch ( formData.mode ) {
			// Is very direct, add units, forget life
			case 'resupply':
				gameStatus.locations[formData.locId].units+= bonus;
				riskUpdateMapConditions();
				//
				cardOpsForm.reset();
				se.dialog.close(cardOps);

				riskUpdateMapConditions();
				gameStatus_save();
				break;
			//
			case 'airCombat':
				se.dialog.close(cardOps);

				// Set
				se.dialog.openModal(bombingRunDialog);

				bombingRunDialog.se_data('active', 1);

				let enemyLocation = gameStatus.locations[formData.locId],
					targetUserIndex = enemyLocation.owner,
					battleInfo = bombingRunDialog.querySelector('.battleInfo');
				se.element.childrenUpdate(battleInfo, {
					'.userName td:first-child' : [
						['text', gameStatus.players[gameStatus.status.userTurn].props.name],
						['data', 'side', gameStatus.players[gameStatus.status.userTurn].props.color]
					],
					'.userName td:last-child' : [
						['text', gameStatus.players[targetuserIndex].props.name],
						['data', 'side', gameStatus.players[targetuserIndex].props.color]
					],
					'.territory td:last-child' : [
						['text', boardGames.risk.properties.map.locations[formData.locId].name]
					],
					'.units td:first-child span' : [
						['text', bonus ]
					],
					'.units td:last-child span' : [
						['text', gameStatus.locations[formData.locId].units ]
					],
				});
				bombingRunForm.se_formElVal('location', formData.locId);
				bombingRunForm.se_formElVal('bombersCount', bonus);
				break;
			//
			default:
				console.error(" what, pero cómo?");
				break;
		}


	}

	//
	function user_card_bombing_exec(e) {
		e.preventDefault();

		let stopOperation = false,
			//
			location = bombingRunForm.se_formElVal('location'),
			bombersCount = intVal(bombingRunForm.se_formElVal('bombersCount')),
			defenseCount = intVal(bombingRunForm.se_formElVal('defenseCount')),
			bombEffect = intVal(bombingRunForm.se_formElVal('bombEffect'))
		;

		// Change effect
		// Deny option to get effect if cero
		if ( defenseCount >= bombersCount ) {
			bombEffect = 0;
		}

		// Remove bombers
		if ( defenseCount ) {
			bombersCount-= defenseCount;
			bombingRunForm.se_formElVal('bombersCount', bombersCount);
			if ( bombersCount <= 0 ) {
				stopOperation = true;
			}
		}

		// Remove units
		if ( bombEffect ) {
			gameStatus.locations[location].units -= bombEffect;
			if ( gameStatus.locations[location].units < 1 ) {
				gameStatus.locations[location].units = 1;
				stopOperation = true;
			}
		}

		console.log("update?", location, defenseCount, bombersCount, bombEffect, gameStatus.locations[location].units);

		// Update
		let battleInfo = bombingRunDialog.querySelector('table.battleInfo');
		se.element.childrenUpdate(battleInfo, {
			'.units td:first-child span' : [
				['text', bombersCount ]
			],
			'.units td:last-child span' : [
				['text', gameStatus.locations[location].units ]
			],
		});

		bombingRunForm.reset();

		//
		riskUpdateMapConditions();
		gameStatus_save();

		//
		if ( stopOperation ) {
			bombingRunDialog.se_data('active', 1);
		}
	}

	//</editor-fold>

	//<editor-fold desc="User turn ops">

	//
	function user_turn_reset() {
		gameStatus.status.userTurn = 0;
		user_turn_apply(true);
	}

	//
	function user_turn_apply(firsTime = true) {
		//
		if ( !userPlayerTableContent.children.length ) {
			console.error("no tengo idea porque puse esto XD");
			return;
		}

		console.debug("GAME USER TURN APPLY: ");

		//
		userPlayerTableContent.querySelectorAll('tr').se_data('turn', 0);
		userPlayerTableContent.children[gameStatus.status.userTurn].se_data('turn', 1);

		// Specific operations at start
		if ( firsTime ) {
			switch ( gameStatus.status.fase ) {
				//
				case 'placement':
				case 'placementUser':
					break;
				//
				case 'firstRoundResupply':
					user_set_step('recieve');
					user_resupply_start();
					user_set_step('resupply');
					break;
				//
				case 'firstRoundCombat':
					user_set_step('combat');
					break;
				//
				case 'mainGame':
					user_set_step('recieve');
					user_resupply_start();
					user_set_step('resupply');
					break;
				//
				default:
					console.error("learn to code, game status fase not defined...", gameStatus);
					break;
			}
			//
			gameStatus_save();
		} else {
			// Set last user step
			user_set_step(gameStatus.status.userStep);
		}
	}

	//
	function user_turn_next() {
		do {
			gameStatus.status.userTurn++;
			if ( gameStatus.status.userTurn >= gameStatus.players.length ) {
				gameStatus.status.userTurn = 0;
				gameStatus.status.faseRounds++;
			}
		} while( !gameStatus.players[gameStatus.status.userTurn].data.isAlive );

		// Reset turn info (avoid collitions)
		gameStatus.status.turnInfo = {
			results:{
				captureLocations: false,
				supplyContent:{
					jets:0,
					units:0,
					sectors:{}
				}
			}
		};

		// Specific operations
		switch ( gameStatus.status.fase ) {
			//
			case 'placement':
				break;
			//
			case 'placementUser':
				// Manually acepted since can be reseted.
				break;
			//
			case 'firstRoundResupply':
				if ( gameStatus.status.faseRounds > 0 ) {
					// All players placed, advance to next section
					board_newFase_set('firstRoundCombat');
				}
				break;
			//
			case 'firstRoundCombat':
				if ( gameStatus.status.faseRounds > 0 ) {
					// All players placed, advance to next section
					board_newFase_set('mainGame');
				}
				break;
			//
			case 'mainGame':
				break;
			//
			default:
				console.error("Current main fase not defined:", gameStatus.status);
				break;
		}

		//
		user_turn_apply(true);
	}

	//
	function user_step_next() {
		// Specific operations
		switch ( gameStatus.status.fase ) {
			//
			case 'start':
			case 'setup':
			case 'placement':
			case 'placementUser':
				break;
			//
			case 'firstRoundResupply':
				// Check if not missing resupply
				if ( !user_resypply_complete_check() ) {
					return;
				}
				user_turn_next();
				break;
			//
			case 'firstRoundCombat':
				//
				switch ( gameStatus.status.userStep ) {
					case 'combat':
						user_set_step('movement_end');
						break;
					case 'movement_end':
						user_set_step('endTurnEval');
						user_eval_endOfTurn();
						break;
					case 'endTurnEval':
						user_turn_next();
						break;
				}
				break;
			//
			case 'mainGame':
				switch ( gameStatus.status.userStep ) {
					case 'resupply':
						// Check if not missing resupply
						if ( !user_resypply_complete_check() ) {
							board_notification_set("Faltan colocar unidades.")
							return;
						}

						//
						if ( gameStatus.players[gameStatus.status.userTurn].data.bonuses.includes('movement_extra_ini') ) {
							user_set_step('movement_ini');
						} else {
							user_set_step('combat');
						}
						break;
					case 'movement_ini':
						user_set_step('combat');
						break;
					case 'combat':
						if ( !gameStatus.status.turnInfo.results.captureLocations && !confirm('No ha capturado ningun territorio. \n ¿Continuar?') ) {
							return;
						}
						user_set_step('movement_end');
						break;
					case 'movement_end':
						if ( gameStatus.players[gameStatus.status.userTurn].data.bonuses.includes('movement_extra_end') ) {
							user_set_step('movement_end_extra');
						} else {
							user_set_step('endTurnEval');
							user_eval_endOfTurn();
						}
						break;
					case 'movement_end_extra':
						user_set_step('endTurnEval');
						user_eval_endOfTurn();
						break;
					case 'endTurnEval':
						user_turn_next();
						break;
				}
				break;
			//
			default:
				console.error("learn to code, game status fase not defined...", gameStatus);
				break;
		}
	}

	//
	function user_set_step(stepName) {
		boardScreenContainer.querySelector('div[data-screen-name="riskBoard"]').se_data('userStep', stepName);
		gameStatus.status.userStep = stepName;
	}

	//
	function userAction(e, cBtn) {
		e.preventDefault();

		//
		switch ( cBtn.se_attr('se-act') ) {
			//
			case 'optionsOpen':
				se.dialog.openModal(boardOptions);
				break;
			//
			case 'optionsClose':
				se.dialog.close(boardOptions);
				break;
			//
			case 'resupplySumaryClose':
				se.dialog.close(resupplySumary);
				break;
			//
			case 'endOfGameClose':
				se.dialog.close(endOfGame);
				break;
			//
			case 'endOfUserClose':
				se.dialog.close(endOfUser);
				break;
			//
			case 'cardOpsClose':
				se.dialog.close(cardOps);
				break;
			//
			case 'bombingRunClose':
				if ( confirm('Cerrar?. se perderan todos los aviones') ) {
					bombingRunForm.reset();
					se.dialog.close(bombingRunDialog);
				}
				break;
			//
			case 'bombingRunEnd':
				bombingRunForm.reset();
				se.dialog.close(bombingRunDialog);
				break;
			//
			case 'endOfTurnSummaryClose':
				se.dialog.close(endOfTurnSummary);
				user_turn_next(); // In case of summary, it actually need to continue to next user step
				break;
			//
			case 'resupplyLocationClose':
				se.dialog.close(resupplyLocation);
				break;
			//
			case 'unitsMovementClose':
				se.dialog.close(unitsMovement);
				gameStatus.status.turnInfo.movement.origin = null;
				gameStatus.status.turnInfo.movement.target = null;
				break;
			//
			case 'battleGroundsClose':
				battle_close();
				break;
			//
			case 'playCards':
				user_card_play();
				break;
			//
			case 'user':
				user_resupply_location_close();
				break;
			//
			case 'resetAll':
				if ( confirm('¿Borrar todo el juego?') ) {
					resetGame();
					se.dialog.close(boardOptions);
				}
				break;
			//
			case 'fase_placement_reset':
				fase_placement_reset();
				break;
			//
			case 'fase_placement_accept':
				fase_placement_accept();
				break;
			//
			case 'fase_placementUser_reset':
				fase_placementUser_reset();
				break;
			//
			case 'fase_placementUser_random':
				fase_placementUser_random();
				break;
			//
			case 'fase_placementUser_accept':
				fase_placementUser_accept();
				break;
			//
			case 'battle_aereal_pick':
				battle_aereal_pick();
				break;
			//
			case 'battle_land_pick':
				battle_land_pick();
				break;
			//
			case 'battle_to_start':
				battle_switch_screen('pick');
				break;
			//
			case 'nextStep':
				user_step_next();
				break;

			//
			case 'playerQR':
				userShowQR(cBtn);
				break;

			//
			default:
				console.error("EVENTO DE BOTÓN NO DEFINIDO.", cBtn);
				break;
		}
		//
	}

	//</editor-fold>

	//<editor-fold desc="Board ops">

	// Define a location to a user
	function riskLocationSet(userIndex, location, troupCount, updateAll = false) {
		// Update map

		// Save information
		gameStatus.locations[location].owner = userIndex;
		gameStatus.locations[location].units = troupCount;

		//
		if ( updateAll ) {
			riskUpdateMapConditions();
		}
	}

	// Check user territories and do an update to all stylings
	function riskUpdateMapConditions() {
		// Reset users
		for ( let userIndex = 0; userIndex < gameStatus.players.length; userIndex++ ) {
			// Properties
			gameStatus.players[userIndex].data.hasCapital = false;
			gameStatus.players[userIndex].data.totalTerritories = 0;
			gameStatus.players[userIndex].data.totalCapitals = 0;
			gameStatus.players[userIndex].data.totalBases = 0;
			gameStatus.players[userIndex].data.totalUnits = 0;
			gameStatus.players[userIndex].data.totalSectors = 0;

			// Reset sectors
			for ( let sectorIndex in gameStatus.players[userIndex].data.sectors ) {
				if ( !gameStatus.players[userIndex].data.sectors.hasOwnProperty(sectorIndex) ) { continue; }
				//
				gameStatus.players[userIndex].data.sectors[sectorIndex].territories = 0;
				gameStatus.players[userIndex].data.sectors[sectorIndex].hasMajority = false;
				gameStatus.players[userIndex].data.sectors[sectorIndex].hasOwnership = false;
			}
		}

		// Count units/territories
		for ( const [cLocation, cLocProperties] of Object.entries(gameStatus.locations) ) {
			let cLocationObj = boardMap.querySelector('#' + cLocation);
			// Skip unasigned territories
			if ( cLocProperties.owner === -1 ) {
				// Update map
				cLocationObj.querySelector('#' + cLocation + '_soldier_circle').se_data('side', 0)
				cLocationObj.querySelector('#' + cLocation + '_soldier_count' ).se_text("00");
				//
				continue;
			}

			// Basic check
			gameStatus.players[cLocProperties.owner].data.totalTerritories++;
			gameStatus.players[cLocProperties.owner].data.totalUnits+= cLocProperties.units;

			//
			cLocationObj.querySelector('#' + cLocation + '_soldier_circle').se_data('side', gameStatus.players[cLocProperties.owner].props.color)
			cLocationObj.querySelector('#' + cLocation + '_soldier_count' ).se_text(cLocProperties.units.toString().padStart(2, "0"));

			// Advanced check (capitals, bases)
			if ( boardGames.risk.properties.map.locations[cLocation].capital ) {
				gameStatus.players[cLocProperties.owner].data.totalCapitals++;

				// Only capital can be bases
				if ( cLocProperties.isBaseFor !== -1 ) {
					gameStatus.players[cLocProperties.owner].data.totalBases++;

					// only bases can be of user
					if ( cLocProperties.isBaseFor === cLocProperties.owner ) {
						gameStatus.players[cLocProperties.owner].data.hasCapital = true;
						boardMap.querySelector('#' + cLocation + '_bunker').se_data('side', gameStatus.players[cLocProperties.owner].props.color);
					}
				}
			}

			// Sector check
			gameStatus.players[cLocProperties.owner].data.sectors[boardGames.risk.properties.map.locations[cLocation].sector].territories++;
		}

		// Sector operations
		for ( let cSector in gameStatus.sectors ) {
			// Check for mayorities / control
			userCheck:{
				for ( let userIndex in gameStatus.players ) {
					//
					if ( !gameStatus.players.hasOwnProperty(userIndex) ) { continue; }

					// Check sectors
					if ( boardGames.risk.properties.map.sectors[cSector].locations === gameStatus.players[userIndex].data.sectors[cSector].territories ) {
						//
						gameStatus.players[userIndex].data.sectors[cSector].hasOwnership = true;
						gameStatus.players[userIndex].data.sectors[cSector].hasMajority = false;
						//
						gameStatus.sectors[cSector].userIndex = intVal(userIndex);
						gameStatus.sectors[cSector].relType = 2;
						//
						gameStatus.players[userIndex].data.totalSectors++;
						//
						break userCheck;
					}
					else if ( boardGames.risk.properties.map.sectors[cSector].majority <= gameStatus.players[userIndex].data.sectors[cSector].territories ) {
						gameStatus.players[userIndex].data.sectors[cSector].hasOwnership = false;
						gameStatus.players[userIndex].data.sectors[cSector].hasMajority = true;
						//
						gameStatus.sectors[cSector].userIndex = intVal(userIndex);
						gameStatus.sectors[cSector].relType = 1;
						//
						break userCheck;
					}
				}

				// No majority or ownership
				gameStatus.sectors[cSector].userIndex = -1;
				gameStatus.sectors[cSector].relType = 0;
			}

			// Continent style update
			let cSectorInMap = boardMap.querySelector('#' + cSector);

			//
			if ( gameStatus.sectors[cSector].userIndex === -1 ) {
				cSectorInMap.se_data('side', 0).se_data('rel', 0);
			}
			else {
				cSectorInMap.se_data('side', gameStatus.players[gameStatus.sectors[cSector].userIndex].props.color );
				cSectorInMap.se_data('rel', gameStatus.sectors[cSector].relType);
			}
		}

		// Update users table
		if ( userPlayerTableContent.children.length ) {
			for ( let userIndex in gameStatus.players ) {
				if ( !gameStatus.players.hasOwnProperty(userIndex) ) {
					continue;
				}

				//
				userPlayerTableContent.children[userIndex].se_data('isAlive', (gameStatus.players[userIndex].data.isAlive) ? 1 : 0);

				//
				se.element.childrenUpdate(
					userPlayerTableContent.children[userIndex],
					{
						'td[se-elem="totalTerritories"]': [
							['text', gameStatus.players[userIndex].data.totalTerritories]
						],
						'td[se-elem="totalUnits"]': [
							['text', gameStatus.players[userIndex].data.totalUnits]
						],
						'td[se-elem="totalPlanes"]': [
							['text', gameStatus.players[userIndex].data.totalPlanes]
						],
						'td[se-elem="totalCapitals"]': [
							['text', gameStatus.players[userIndex].data.totalCapitals]
						],
						'td[se-elem="totalBases"]': [
							['text', gameStatus.players[userIndex].data.totalBases]
						],
						'div[se-elem="hasCapital"]': [
							['data', 'value', ( gameStatus.players[userIndex].data.hasCapital ) ? 1 : 0]
						],
					}
				);
			}
		}
	}

	//</editor-fold>

	//<editor-fold desc="General functions">

	// Save update
	function gameStatus_save() {
		// Set update
		console.debug("GAME SAVING...", gameStatus);

		//
		boardGames.func.saveBoard(boardName, properties.gameId, properties.gameKey, gameStatus);

		//
		mqttReportUpdate()
	}

	// Set update in MQTT
	function mqttReportUpdate() {
		mqtt_connector.sendMessage('/board/update', gameStatus, true);
	}

	//
	function userShowQR(btn) {
		let usrUrl = window.location.origin + "/games/game/" + properties.gameId + ":" + properties.gameKey + "/mobile/" + btn.se_data('userid') + ":" + btn.se_data('userkey');
		qrGenerator.makeCode(usrUrl);
		userQRDialog.pseudoDialog.show();

		console.log("USER URL IS: ", usrUrl);
	}
	//</editor-fold>

	//
	init();
	return {};
};
//
