<?php
// Files
\se_nav::pageFileGroupAdd(['game_rosk_desktop.js', 'game_rosk_desktop.css', 'game_rosk_general.js', 'game_general_desktop.css', 'game_general_desktop.js', 'game_general.js']);

// Read map directly
$svgMap =  file_get_contents(__DIR__ . '/../svg/map_proc.svg');

// Content
$page['body'] = <<<HTML
<div id="roskBoard" class="general_boardgame" se-plugin="games_rosk_desktop_game" data-gameid="{$params['vars']['gameId']}" data-gamekey="{$params['vars']['gameKey']}">

	<!-- DIALOG: OPTIONS -->
	<dialog id="boardOptions" class="asWindow">
		<div class="win_main">
			<div class="win_header">
				<div class="title">Nombre</div>
				<button se-act="optionsClose" class="close"><svg class="icon"><use xlink:href="#fa-remove" /></svg></button>
			</div>
			<div class="win_content menuList">
				<button se-act="resetAll"><svg class="icon inline mr"><use xlink:href="#fa-save" /></svg> Reiniciar todo</button>
			</div>
		</div>
	</dialog>

	<!-- DIALOG: Resupply SUMARY -->
	<dialog id="resupplySumary" class="asWindow">
		<div class="win_main">
			<div class="win_header">
				<div class="title">Suministros</div>
				<button se-act="resupplySumaryClose" class="close"><svg class="icon"><use xlink:href="#fa-remove" /></svg></button>
			</div>
			<div class="win_content">
				<div class="userInformation" data-side=""></div>
				<table class="supplyList" se-elem="suplyList">
					<template>
						<tr>
							<td><svg class="icon inline mr"><use xlink:href="!icon;"/></svg></td>
							<td>!type;</td>
							<td>!ammount;</td>
						</tr>
					</template>
					<tbody se-elem="content"></tbody>
				</table>
				<button class="btn wide blue" se-act="resupplySumaryClose"><svg class="icon inline mr"><use xlink:href="#fa-check" /></svg>Aceptar</button>
			</div>
		</div>
	</dialog>

	<!-- DIALOG: End of turn summary -->
	<dialog id="endOfTurnSummary" class="asWindow">
		<div class="win_main">
			<div class="win_header">
				<div class="title">Final de turno</div>
				<button se-act="endOfTurnSummaryClose" class="close"><svg class="icon"><use xlink:href="#fa-remove" /></svg></button>
			</div>
			<div class="win_content">
				<div class="userInformation" data-side=""></div>
				<table class="resultList" se-elem="resultList">
					<template>
						<tr>
							<td><svg class="icon inline mr"><use xlink:href="!icon;"/></svg></td>
							<td>!text;</td>
						</tr>
					</template>
					<tbody se-elem="content"></tbody>
				</table>
				<button class="btn wide blue" se-act="endOfTurnSummaryClose"><svg class="icon inline mr"><use xlink:href="#fa-check" /></svg>Aceptar</button>
			</div>
		</div>
	</dialog>

	<!-- DIALOG: Cards OP -->
	<dialog id="cardOps" class="asWindow">
		<div class="win_main">
			<div class="win_header">
				<div class="title">Operaciones de tarjeta</div>
				<button se-act="cardOpsClose" class="close"><svg class="icon"><use xlink:href="#fa-remove" /></svg></button>
			</div>
			<div class="win_content">
				<div class="dialogTitle">Operaciones de tarjeta</div>
				<div class="locationName"></div>
				<div class="cardsIcon"><svg class="icon inline"><use xlink:href="#fa-remove" /></svg></div>

				<form class="cards">
					<input type="hidden" name="cardId" />
					<input type="hidden" name="locId" />
					<input type="hidden" name="mode" />
					<div>Tarjetas additivas:</div>
					<div se-elem="cards">
						<template>
							<label>
								<input type="checkbox" name="card[!cardId;]" value="!value;" />
								<div>!value;</div>
							</label>
						</template>
						<div class="cardList" se-elem="content"></div>
					</div>
					<button class="btn wide blue" type="submit"><svg class="icon inline mr"><use xlink:href="#fa-check" /></svg>Aceptar</button>
				</form>

				<button class="btn wide red" se-act="cardOpsClose"><svg class="icon inline mr"><use xlink:href="#fa-remove" /></svg>Cerrar</button>
			</div>
		</div>
	</dialog>

	<!-- DIALOG: Bombing run -->
	<dialog id="bombingRunDialog" class="asWindow" data-active="1">
		<div class="win_main">
			<div class="win_header">
				<div class="title">Operaciones de tarjeta</div>
				<button se-act="bombingRunClose" class="close"><svg class="icon"><use xlink:href="#fa-remove" /></svg></button>
			</div>
			<div class="win_content">
				<div class="dialogTitle">Bombardeo con tarjeta</div>

				<table class="battleInfo">
					<tr class="title">
						<td>Ataca</td>
						<td>Defiende</td>
					</tr>
					<tr class="userName">
						<td data-side=""></td>
						<td data-side=""></td>
					</tr>
					<tr class="territory">
						<td></td>
						<td></td>
					</tr>
					<tr class="units">
						<td><svg class="icon inline mr"><use xlink:href="#fa-fighter-jet" /></svg><span></span></td>
						<td><svg class="icon inline mr"><use xlink:href="#fa-user" /></svg><span></span></td>
					</tr>
				</table>

				<form class="combat activeElement">
					<input type="hidden" name="location" />
					<input type="hidden" name="bombersCount" />
					<div class="observation">Defensa</div>
					<div class="operation">
						<label>
							<input type="radio" required name="defenseCount" value="2" />
							<div>
								<svg class="icon inline mr"><use xlink:href="#fa-fighter-jet" /></svg> x 2
							</div>
						</label>
						<label>
							<input type="radio" required name="defenseCount" value="1" />
							<div>
								<svg class="icon inline mr"><use xlink:href="#fa-fighter-jet" /></svg> x 1
							</div>
						</label>
						<label>
							<input type="radio" required name="defenseCount" value="0" />
							<div>
								<svg class="icon inline mr"><use xlink:href="#fa-fighter-jet" /></svg> x 0
							</div>
						</label>
					</div>
					<div class="observation">Efecto</div>
					<div class="operation">
						<label>
							<input type="radio" required name="bombEffect" value="3" />
							<div>
								<svg class="icon inline mr"><use xlink:href="#fa-user-times" /></svg> x 3
							</div>
						</label>
						<label>
							<input type="radio" required name="bombEffect" value="2" />
							<div>
								<svg class="icon inline mr"><use xlink:href="#fa-user-times" /></svg> x 2
							</div>
						</label>
						<label>
							<input type="radio" required name="bombEffect" value="1" />
							<div>
								<svg class="icon inline mr"><use xlink:href="#fa-user-times" /></svg> x 1
							</div>
						</label>
						<label>
							<input type="radio" required name="bombEffect" value="0" />
							<div>
								<svg class="icon inline mr"><use xlink:href="#fa-user-times" /></svg> x 0
							</div>
						</label>
					</div>
					<button class="btn wide blue" type="submit"><svg class="icon inline mr"><use xlink:href="#fa-check" /></svg>Aceptar</button>
				</form>

				<button class="btn wide red inactiveElement" se-act="bombingRunEnd"><svg class="icon inline mr"><use xlink:href="#fa-remove" /></svg>Cerrar</button>
			</div>
		</div>
	</dialog>

	<!-- DIALOG: End of battle -->
	<dialog id="endOfUser" class="asWindow userDialogInformation">
		<div class="win_main">
			<div class="win_header">
				<div class="title">Jugador Eliminado</div>
				<button se-act="endOfUserClose" class="close"><svg class="icon"><use xlink:href="#fa-remove" /></svg></button>
			</div>
			<div class="win_content">
				<div class="userTitle defeat">Jugador ha sido eliminado</div>
				<div class="userInformation" data-side=""></div>
				<div class="userIcon"><svg class="icon inline"><use xlink:href="#fa-user-times" /></svg></div>
			</div>
		</div>
	</dialog>

	<!-- DIALOG: End of game -->
	<dialog id="endOfGame" class="asWindow userDialogInformation">
		<div class="win_main">
			<div class="win_header">
				<div class="title">Final del juego</div>
				<button se-act="endOfGameClose" class="close"><svg class="icon"><use xlink:href="#fa-remove" /></svg></button>
			</div>
			<div class="win_content">
				<div class="userTitle win">Victoria!</div>
				<div class="userInformation" data-side=""></div>
				<div class="userIcon"><svg class="icon inline"><use xlink:href="#fa-birthday-cake" /></svg></div>
			</div>
		</div>
	</dialog>

	<!-- DIALOG: Resupply -->
	<dialog id="resupplyLocation" class="asWindow">
		<div class="win_main">
			<div class="win_header">
				<div class="title">Insertar tropas en:</div>
				<button se-act="resupplyLocationClose" class="close"><svg class="icon"><use xlink:href="#fa-remove" /></svg></button>
			</div>
			<div class="win_content">
				<div class="locationName"></div>

				<form class="se_form">
					<input type="hidden" name="location" />
					Cantidad
					<div class="inputSlider">
						<input type="range" name="ammount" min="1" max="" />
						<input type="number" min="1" max="" step="1" />
					</div>
					<button class="btn wide blue" type="submit"><svg class="icon inline mr"><use xlink:href="#fa-check" /></svg>Insertar</button>
				</form>

				<button class="btn wide red" se-act="resupplyLocationClose"><svg class="icon inline mr"><use xlink:href="#fa-remove" /></svg>Cerrar</button>
			</div>
		</div>
	</dialog>

	<!-- DIALOG: Movement -->
	<dialog id="unitsMovement" class="asWindow">
		<div class="win_main">
			<div class="win_header">
				<div class="title">Desplazar tropas:</div>
				<button se-act="unitsMovementClose" class="close"><svg class="icon"><use xlink:href="#fa-remove" /></svg></button>
			</div>
			<div class="win_content">
				<table class="movementInfo">
					<tr class="title">
						<td>Origen</td>
						<td class="thin"></td>
						<td>Destino</td>
					</tr>
					<tr class="territory">
						<td></td>
						<td class="thin"><svg class="icon inline"><use xlink:href="#fa-arrow-right" /></svg></td>
						<td></td>
					</tr>
					<tr class="units">
						<td><svg class="icon inline mr"><use xlink:href="#fa-user" /></svg><span></span></td>
						<td class="thin"></td>
						<td><svg class="icon inline mr"><use xlink:href="#fa-user" /></svg><span></span></td>
					</tr>
				</table>

				<div>
					<div class="locationName"></div>
					<form class="se_form">
						<input type="hidden" name="location" />
						Cantidad
						<div class="inputSlider">
							<input type="range" name="ammount" min="1" max="" />
							<input type="number" min="1" max="" step="1" />
						</div>
						<button class="btn wide blue" type="submit"><svg class="icon inline mr"><use xlink:href="#fa-check" /></svg>Mover</button>
					</form>


					<button class="btn wide blue" se-act="unitsMovementClose"><svg class="icon inline mr"><use xlink:href="#fa-remove" /></svg>Cerrar</button>
				</div>
			</div>
		</div>
	</dialog>

	<!-- DIALOG: BATTLE -->
	<dialog id="battleGrounds" class="asWindow">
		<div class="win_main">
			<div class="win_header">
				<div class="title">Batalla</div>
				<button se-act="battleGroundsClose" class="close"><svg class="icon"><use xlink:href="#fa-remove" /></svg></button>
			</div>
			<div class="win_content">
				<table class="battleInfo">
					<tr class="title">
						<td>Ataca</td>
						<td>Defiende</td>
					</tr>
					<tr class="userName">
						<td data-side=""></td>
						<td data-side=""></td>
					</tr>
					<tr class="territory">
						<td></td>
						<td></td>
					</tr>
					<tr class="units">
						<td><svg class="icon inline mr"><use xlink:href="#fa-user" /></svg><span></span></td>
						<td><svg class="icon inline mr"><use xlink:href="#fa-user" /></svg><span></span></td>
					</tr>
					<tr class="planes">
						<td><svg class="icon inline mr"><use xlink:href="#fa-fighter-jet" /></svg><span></span></td>
						<td></td>
					</tr>
				</table>
				<div class="battleScreens">
					<div class="grid" data-screen-name="pick" aria-hidden="false">
						<div class="gr_sz06 gr_ps03 menuList">
							<button se-act="battle_aereal_pick"><svg class="icon inline mr"><use xlink:href="#fa-fighter-jet" /></svg> Bombardeo</button>
							<button se-act="battle_land_pick"><svg class="icon inline mr"><use xlink:href="#fa-user" /></svg> Asalto</button>
							<div><button class="btn wide red" se-act="battleGroundsClose"><svg class="icon inline mr"><use xlink:href="#fa-remove" /></svg> Cancelar</button></div>
						</div>
					</div>
					<div data-screen-name="aereal" aria-hidden="true">
						<div>
							<div class="boardSectionTitle">Bombardeo</div>
							<div class="notification alert" se-elem="battle_air_warning" aria-hidden="true">El límite de tropas en el territorio enemigo ha sido alcanzado.</div>
							<form class="combat" se-elem="battle_air" aria-hidden="true">
								<div class="observation">Cantidad de bombarderos</div>
								<div class="operation" se-elem="bombers">
									<label data-value="3">
										<input type="radio" required name="bombersCount" value="3" />
										<div>
											<svg class="icon inline mr"><use xlink:href="#fa-fighter-jet" /></svg> x 3
										</div>
									</label>
									<label data-value="2">
										<input type="radio" required name="bombersCount" value="2" />
										<div>
											<svg class="icon inline mr"><use xlink:href="#fa-fighter-jet" /></svg> x 2
										</div>
									</label>
									<label data-value="1">
										<input type="radio" required name="bombersCount" value="1" />
										<div>
											<svg class="icon inline mr"><use xlink:href="#fa-fighter-jet" /></svg> x 1
										</div>
									</label>
								</div>
								<div class="observation">Defensa</div>
								<div class="operation">
									<label>
										<input type="radio" required name="defenseCount" value="2" />
										<div>
											<svg class="icon inline mr"><use xlink:href="#fa-fighter-jet" /></svg> x 2
										</div>
									</label>
									<label>
										<input type="radio" required name="defenseCount" value="1" />
										<div>
											<svg class="icon inline mr"><use xlink:href="#fa-fighter-jet" /></svg> x 1
										</div>
									</label>
									<label>
										<input type="radio" required name="defenseCount" value="0" />
										<div>
											<svg class="icon inline mr"><use xlink:href="#fa-fighter-jet" /></svg> x 0
										</div>
									</label>
								</div>
								<div class="observation">Efecto</div>
								<div class="operation" se-elem="effect">
									<label>
										<input type="radio" required name="bombEffect" value="3" />
										<div>
											<svg class="icon inline mr"><use xlink:href="#fa-user-times" /></svg> x 3
										</div>
									</label>
									<label>
										<input type="radio" required name="bombEffect" value="2" />
										<div>
											<svg class="icon inline mr"><use xlink:href="#fa-user-times" /></svg> x 2
										</div>
									</label>
									<label>
										<input type="radio" required name="bombEffect" value="1" />
										<div>
											<svg class="icon inline mr"><use xlink:href="#fa-user-times" /></svg> x 1
										</div>
									</label>
									<label>
										<input type="radio" required name="bombEffect" value="0" />
										<div>
											<svg class="icon inline mr"><use xlink:href="#fa-user-times" /></svg> x 0
										</div>
									</label>
								</div>
								<button class="btn wide blue" type="submit"><svg class="icon inline mr"><use xlink:href="#fa-check" /></svg>Aceptar</button>
							</form>
						</div>
						<div><button class="btn wide green" se-act="battle_land_pick"><svg class="icon inline mr"><use xlink:href="#fa-user" /></svg> Asalto</button></div>
						<div><button class="btn wide red" se-act="battleGroundsClose"><svg class="icon inline mr"><use xlink:href="#fa-remove" /></svg> Cancelar</button></div>
					</div>
					<div data-screen-name="land" aria-hidden="true">
						<div>
							<div class="boardSectionTitle">Asalto</div>
							<form class="combat" se-elem="battle_land">
								<div class="observation">Cantidad de tropas</div>
								<div class="operation" se-elem="unitsAttack">
									<label data-value="3">
										<input type="radio" required name="attackUnits" value="3" />
										<div>
											<svg class="icon inline mr"><use xlink:href="#fa-user" /></svg> x 3
										</div>
									</label>
									<label data-value="2">
										<input type="radio" required name="attackUnits" value="2" />
										<div>
											<svg class="icon inline mr"><use xlink:href="#fa-user" /></svg> x 2
										</div>
									</label>
									<label data-value="1">
										<input type="radio" required name="attackUnits" value="1" />
										<div>
											<svg class="icon inline mr"><use xlink:href="#fa-user" /></svg> x 1
										</div>
									</label>
								</div>
								<div class="observation">Efecto</div>
								<div class="operation" se-elem="effect">
									<label data-value="3">
										<input type="radio" required name="battleEffect" value="0-3" />
										<div>
											0 -
											<svg class="icon inline mr objDefense"><use xlink:href="#fa-user-times" /></svg>
											<svg class="icon inline mr objDefense"><use xlink:href="#fa-user-times" /></svg>
											<svg class="icon inline mr objDefense"><use xlink:href="#fa-user-times" /></svg>
										</div>
									</label>
									<label data-value="3">
										<input type="radio" required name="battleEffect" value="1-2" />
										<div>
											<svg class="icon inline mr objAttack"><use xlink:href="#fa-user-times" /></svg>
											-
											<svg class="icon inline mr objDefense"><use xlink:href="#fa-user-times" /></svg>
											<svg class="icon inline mr objDefense"><use xlink:href="#fa-user-times" /></svg>
										</div>
									</label>
									<label data-value="3">
										<input type="radio" required name="battleEffect" value="2-1" />
										<div>
											<svg class="icon inline mr objAttack"><use xlink:href="#fa-user-times" /></svg>
											<svg class="icon inline mr objAttack"><use xlink:href="#fa-user-times" /></svg>
											-
											<svg class="icon inline mr objDefense"><use xlink:href="#fa-user-times" /></svg>
										</div>
									</label>
									<label data-value="3">
										<input type="radio" required name="battleEffect" value="3-0" />
										<div>
											<svg class="icon inline mr objAttack"><use xlink:href="#fa-user-times" /></svg>
											<svg class="icon inline mr objAttack"><use xlink:href="#fa-user-times" /></svg>
											<svg class="icon inline mr objAttack"><use xlink:href="#fa-user-times" /></svg>
											-
											0
										</div>
									</label>
									<label data-value="2">
										<input type="radio" required name="battleEffect" value="0-2" />
										<div>
											0
											-
											<svg class="icon inline mr objDefense"><use xlink:href="#fa-user-times" /></svg>
											<svg class="icon inline mr objDefense"><use xlink:href="#fa-user-times" /></svg>
										</div>
									</label>
									<label data-value="2">
										<input type="radio" required name="battleEffect" value="1-1" />
										<div>
											<svg class="icon inline mr objAttack"><use xlink:href="#fa-user-times" /></svg>
											-
											<svg class="icon inline mr objDefense"><use xlink:href="#fa-user-times" /></svg>
										</div>
									</label>
									<label data-value="2">
										<input type="radio" required name="battleEffect" value="2-0" />
										<div>
											<svg class="icon inline mr objAttack"><use xlink:href="#fa-user-times" /></svg>
											<svg class="icon inline mr objAttack"><use xlink:href="#fa-user-times" /></svg>
											-
											0
										</div>
									</label>
									<label data-value="1">
										<input type="radio" required name="battleEffect" value="0-1" />
										<div>
											0
											-
											<svg class="icon inline mr objDefense"><use xlink:href="#fa-user-times" /></svg>
										</div>
									</label>
									<label data-value="1">
										<input type="radio" required name="battleEffect" value="1-0" />
										<div>
											<svg class="icon inline mr objAttack"><use xlink:href="#fa-user-times" /></svg>
											-
											0
										</div>
									</label>
								</div>
								<button class="btn wide blue" type="submit"><svg class="icon inline mr"><use xlink:href="#fa-check" /></svg>Aceptar</button>
							</form>
						</div>

						<div><button class="btn wide green" se-act="battle_aereal_pick"><svg class="icon inline mr"><use xlink:href="#fa-fighter-jet" /></svg> Bombardeo</button></div>
						<div><button class="btn wide red" se-act="battleGroundsClose"><svg class="icon inline mr"><use xlink:href="#fa-remove" /></svg> Cancelar</button></div>
					</div>
				</div>
			</div>
		</div>
	</dialog>
	
	<!-- USER QR CODES -->
	<div se-elem="userQRDialog"><div se-elem="qrObject"></div></div>

	<!-- SCREENS -->
	<div class="boardScreenContainer">
		<!-- MAIN BOARD -->
		<div id="roskBoard" data-screen-name="riskBoard" class="board mainBoard3" aria-hidden="true" data-fase="" data-step="">

			<div class="main">
				<div se-elem="boardMap">
					{$svgMap}
				</div>
			</div>
			
			<div class="bigBar">
				<div class="boardSectionTitle">ROSK</div>
				
				<div class="liveConsole"></div>

				<div>
					<!-- INI: instructions -->
					<div class="lat_bar_section faseDependent placement">
						<div class="title">Posicionamiento inicial</div>
						<div>
							<p>Elegidos al azar. Se puede dar re roll en la opción inferior.</p>
						</div>
					</div>
					<div class="lat_bar_section faseDependent placementUser">
						<div class="title">Posicionamiento por usuario</div>
						<div>
							<p>Ir poniendo las piezas por jugador o seleccionar llenado random.</p>
						</div>
					</div>
					<div class="lat_bar_section faseDependent firstRoundResupply">
						<div class="title">Primer Round:<br />Suministros</div>
						<div>
							<p>Primeras suministros según posiciones iniciales.</p>
							<p>Todos los jugadores ponen sus piezas antes del combate.</p>
						</div>
					</div>
					<div class="lat_bar_section faseDependent firstRoundCombat">
						<div class="title">Primer Round:<br />Batallas</div>
						<div>
							<p>Primer ronda de batallas.</p>
							<p>No hay colocación de tropas hasta el siguiente round.</p>
						</div>
					</div>
					<div class="lat_bar_section faseDependent mainGame">
						<div class="title">Juego principal</div>
						<div></div>
					</div>
					<!-- END: instructions -->
	
					<!-- INI: timer -->
					<div class="lat_bar_section faseDependent firstRoundCombat mainGame">
						<div class="title">Timer</div>
						<div></div>
					</div>
					<!-- END: timer -->
	
					<!-- INI: contextual clues -->
					<div class="lat_bar_section faseDependent firstRoundCombat">
						<div class="title">Étapas</div>
						<ol class="stages">
							<li data-step="combat">Combate</li>
							<li data-step="movement_end">Movimiento Final</li>
							<li data-step="endTurnEval">Fin de turno</li>
						</ol>
					</div>
					<div class="lat_bar_section faseDependent mainGame">
						<div class="title">Étapas</div>
						<ol class="stages">
							<li data-step="resupply">Colocación</li>
							<li data-step="movement_ini">Movimiento Inicial (Extra)</li>
							<li data-step="combat">Combate</li>
							<li data-step="movement_end">Movimiento Final</li>
							<li data-step="movement_end_extra">Movimiento Final (Extra)</li>
							<li data-step="endTurnEval">Tarjetas</li>
						</ol>
					</div>
					<!-- END: contextual clues -->
					
					<div class="lat_bar_section">
						<div class="title">Jugadores</div>
						<table class="se_table alternate border wide" se-elem="playerList">
							<template>
								<tr data-color="!playerColor;" data-turn="0" data-isalive="!isAlive;">
									<td class="name">!playerName;</td>
									<td se-elem="totalTerritories">!totalTerritories;</td>
									<td se-elem="totalUnits">!totalUnits;</td>
									<td se-elem="totalPlanes">!totalPlanes;</td>
									<td se-elem="totalCapitals">!totalCapitals;</td>
									<td se-elem="totalBases">!totalBases;</td>
									<td><div class="fakeCheck" se-elem="hasCapital" data-value="!hasCapital;"></div></td>
									<td><button se-act="playerQR" data-userid="!userId;" data-userkey="!userKey;"><svg class="icon inline"><use xlink:href="#fa-eye" /></svg></button></td>
								</tr>
							</template>
							<thead>
								<tr class="titles">
									<th>Jugador</th>
									<th><svg class="icon inline"><use xlink:href="#fa-flag" /></svg></th>
									<th><svg class="icon inline"><use xlink:href="#fa-male" /></svg></th>
									<th><svg class="icon inline"><use xlink:href="#fa-fighter-jet" /></svg></th>
									<th><svg class="icon inline"><use xlink:href="#fa-star" /></svg></th>
									<th><svg class="icon inline"><use xlink:href="#fa-fort-awesome" /></svg></th>
									<th>Cap</th>
									<th><svg class="icon inline"><use xlink:href="#fa-mobile-phone" /></svg></th>
								</tr>
							</thead>
							<tbody se-elem="content"></tbody>
						</table>
					</div>
				</div>
			</div>
			
			<div class="smallBar">
				<div>
					<button class="circle_button" se-act="optionsOpen"><svg class="icon"><use xlink:href="#fa-gear" /></svg></button>
				</div>
				
				<div>
					<div class="faseDependent placement">
						<div class="options">
							<button class="circle_button" se-act="fase_placement_reset"><svg class="icon"><use xlink:href="#fa-refresh" /></svg></button>
							<button class="circle_button" se-act="fase_placement_accept"><svg class="icon"><use xlink:href="#fa-check" /></svg></button>
						</div>
					</div>
					<div class="faseDependent placementUser">
						<div class="options">
							<button class="circle_button" se-act="fase_placementUser_random"><svg class="icon"><use xlink:href="#fa-flickr" /></svg></button>
							<button class="circle_button" se-act="fase_placementUser_reset"><svg class="icon"><use xlink:href="#fa-refresh" /></svg></button>
							<button class="circle_button" se-act="fase_placementUser_accept"><svg class="icon"><use xlink:href="#fa-check" /></svg></button>
						</div>
					</div>
					<div class="faseDependent firstRoundResupply">
						<div class="options">
							<button class="circle_button" se-act="nextStep"><svg class="icon"><use xlink:href="#fa-arrow-right" /></svg></button>
						</div>
					</div>
					<div class="faseDependent firstRoundCombat">
						<div class="options">
							<button class="circle_button" se-act="nextStep"><svg class="icon"><use xlink:href="#fa-arrow-right" /></svg></button>
						</div>
					</div>
					<div class="faseDependent mainGame">
						<div class="options">
							<button class="circle_button" se-act="playCards"><svg class="icon"><use xlink:href="#fa-file" /></svg></button>
							<button class="circle_button" se-act="nextStep"><svg class="icon"><use xlink:href="#fa-arrow-right" /></svg></button>
						</div>
					</div>
				</div>
			</div>

		</div>

		<!-- END GAME -->
		<div data-screen-name="engGame" aria-hidden="true">

		</div>
	</div>
</div>
HTML;
