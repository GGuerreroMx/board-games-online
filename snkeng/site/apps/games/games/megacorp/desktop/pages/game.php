<?php
// Files
\se_nav::pageFileGroupAdd(['game_megacorp_desktop.js', 'game_megacorp_desktop.css', 'game_megacorp_general.js', 'game_general_desktop.css', 'game_general_desktop.js', 'game_general.js']);

// Content
$page['body'] = <<<HTML
<div id="boardgame_megacorp" class="general_boardgame" se-plugin="games_megacorp_desktop_game" data-gameid="{$params['vars']['gameId']}" data-gamekey="{$params['vars']['gameKey']}">

	<!-- DIALOG: OPTIONS -->
	<dialog id="boardOptions" class="asWindow">
		<div class="win_main">
			<div class="win_header">
				<div class="title">Nombre</div>
				<button se-act="optionsClose" class="close"><svg class="icon"><use xlink:href="#fa-remove" /></svg></button>
			</div>
			<div class="win_content menuList">
				<button se-act="resetAll"><svg class="icon inline mr"><use xlink:href="#fa-save" /></svg> Reiniciar todo</button>
			</div>
		</div>
	</dialog>
	
	<!-- USER QR CODES -->
	<div se-elem="userQRDialog"><div se-elem="qrObject"></div></div>
	
	<!-- DIALOG: End of game -->
	<dialog id="endOfGame" class="asWindow">
		<div class="win_main">
			<div class="win_header">
				<div class="title">Final del juego</div>
				<button se-act="endOfGameClose" class="close"><svg class="icon"><use xlink:href="#fa-remove" /></svg></button>
			</div>
			<div class="win_content">
				<div class="userVictoryText">Victoria!</div>
				<div class="userInformation" data-side=""></div>
				<div class="userVictoryIcon"><svg class="icon inline"><use xlink:href="#fa-birthday-cake" /></svg></div>
			</div>
		</div>
	</dialog>

	<!-- DIALOG: Land Fall -->
	<dialog id="userLandFall" class="asWindow">
		<div class="win_main">
			<div class="win_header">
				<div class="title">Acción</div>
			</div>
			<div class="win_content">
				<div class="grid">
					<div class="gr_sz07">
						<div class="tile big" data-group="">
							<div class="header"></div>
							<div class="body">
								<div class="name"></div>
								<div class="center">
									<div class="image"><img src="" /></div>
								</div>
								<div></div>
							</div>
						</div>
					</div>
					<div class="gr_sz05">
						<div class="fallOptions">
							<div se-elem="buy">
								<div class="info_section">
									<div class="title">Comprar</div>
									<div class="value"></div>
									
									<form class="se_form" se-elem="buy">
										<input type="hidden" name="location" />
										<input type="hidden" name="player" />
										<input type="hidden" name="value" />
										<button class="btn wide blue" type="submit"><svg class="icon inline mr"><use xlink:href="#fa-shopping-cart" /></svg>Comprar</button>
									</form>
								</div>

								<div class="info_section">
									<div class="title">Subasta</div>
									<form class="se_form" se-elem="bid">
										<input type="hidden" name="location" />
										<label class="separator required">
											<div class="cont"><span class="title">Valor</span><span class="desc"></span></div>
											<input type="number" name="value" />
										</label>
										<label class="separator required">
											<div class="cont"><span class="title">Jugador ganador</span><span class="desc"></span></div>
											<select name="player"></select>
										</label>
										<button class="btn wide blue" type="submit"><svg class="icon inline mr"><use xlink:href="#fa-check" /></svg>Aceptar</button>
									</form>
								</div>
							</div>
							<div se-elem="card">
								<div class="info_section">
									<div class="title">TARJETA</div>
									<div class="subtitle" se-elem="type"></div>
									<div class="content"></div>
									<button se-act="userActionLandingClose" class="btn wide blue"><svg class="icon inline mr"><use xlink:href="#fa-check" /></svg> Aceptar</button>
								</div>
							</div>
							<div se-elem="event">
								<div class="info_section">
									<div class="title">EVENTO</div>
									<div class="subtitle" se-elem="type"></div>
									<div class="value"></div>
									<button se-act="userActionLandingClose" class="btn wide blue"><svg class="icon inline mr"><use xlink:href="#fa-check" /></svg> Aceptar</button>
								</div>
							</div>
							<div se-elem="null">
								<div class="info_section">
									<div class="title"></div>
									<button se-act="userActionLandingClose" class="btn wide blue"><svg class="icon inline mr"><use xlink:href="#fa-check" /></svg> Aceptar</button>
								</div>
							</div>
							<div se-elem="simplePay">
								<div class="info_section">
									<div class="title">Pago</div>
									<div class="value"></div>
									<div class="owner">A:  <span class="userNameDisplay"></span></div>
									<button se-act="userActionLandingClose" class="btn wide blue"><svg class="icon inline mr"><use xlink:href="#fa-check" /></svg> Aceptar</button>
								</div>
							</div>
							<div se-elem="userDead">
								<div class="info_section">
									<div class="title">Jugador eliminado</div>
									<div class="deadIcon"><svg class="icon inline mr"><use xlink:href="#fa-user-times" /></svg> <span class="name"></span></div>
									<div class="value"></div>
									<div class="owner">POR:  <span class="userNameDisplay"></span></div>
									<button se-act="userActionLandingClose" class="btn wide blue"><svg class="icon inline mr"><use xlink:href="#fa-check" /></svg> Aceptar</button>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</dialog>

	<!-- SCREENS -->
	<div class="boardScreenContainer">
		<!-- MAIN BOARD -->
		<div data-screen-name="mainBoard" class="board mainBoard3" aria-hidden="true" data-fase="" data-step="">
		
			<div class="main">
				<div class="boardMap">
					<div class="map" se-elem="map" data-housing="0" >
						<div class="tile simple" se-elem="go">
							<div class="header"></div>
							<div class="body">
								<div class="name">GO</div>
								<div class="center">
									<div class="image">
										<img src="/snkeng/site/apps/games/games/megacorp/shared/img/go.jpg"/>
									</div>
									<div class="userContainer"></div>
								</div>
								<div class="price"></div>
							</div>
						</div>
						<div class="tile" se-elem="land_g01_l01" data-ownercolor="1" data-mortaged="0" data-grouped="0" data-rentindex="1" data-group="1">
							<div class="header">
								<button se-act="user_housing_down">-</button>
								<div class="rating"></div>
								<button se-act="user_housing_up">+</button>
							</div>
							<div class="body">
								<div class="name">de_747</div>
								<div class="center">
									<div class="image">
										<img src="/snkeng/site/apps/games/games/megacorp/shared/img/cs_747.webp" />
									</div>
									<div class="userContainer"></div>
								</div>
								<div class="price">$60</div>
							</div>
						</div>
						<div class="tile" se-elem="card_community_01">
							<div class="header"></div>
							<div class="body">
								<div class="name">COMMUNITY</div>
								<div class="center">
									<div class="image">
										<img src="/snkeng/site/apps/games/games/megacorp/shared/img/counter_terrorist_gign9.webp" />
									</div>
									<div class="userContainer"></div>
								</div>
								<div class="price"></div>
							</div>
						</div>
						<div class="tile" se-elem="land_g01_l02" data-ownercolor="2" data-mortaged="0" data-grouped="0" data-rentindex="2" data-group="1">
							<div class="header">
								<button se-act="user_housing_down">-</button>
								<div class="rating"></div>
								<button se-act="user_housing_up">+</button>
							</div>
							<div class="body">
								<div class="name">de_airstrip</div>
								<div class="center">
									<div class="image">
										<img src="/snkeng/site/apps/games/games/megacorp/shared/img/de_airstrip.webp" />
									</div>
									<div class="userContainer"></div>
								</div>
								<div class="price">$60</div>
							</div>
						</div>
						<div class="tile" se-elem="tax_income">
							<div class="header"></div>
							<div class="body">
								<div class="name">TAX INCOME</div>
								<div class="center">
									<div class="image">
										<img src="/snkeng/site/apps/games/games/megacorp/shared/img/tax_income.webp"/>
									</div>
									<div class="userContainer"></div>
								</div>
								<div class="price">-$200</div>
							</div>
						</div>
						<div class="tile" se-elem="train_01" data-ownercolor="-1">
							<div class="header">
								<button se-act="user_housing_down">-</button>
								<div class="rating"></div>
								<button se-act="user_housing_up">+</button>
							</div>
							<div class="body">
								<div class="name">TRAIN A</div>
								<div class="center">
									<div class="image">
										<img src="/snkeng/site/apps/games/games/megacorp/shared/img/de_train_a.webp" />
									</div>
									<div class="userContainer"></div>
								</div>
								<div class="price">$200</div>
							</div>
						</div>
						<div class="tile" se-elem="land_g02_l01" data-ownercolor="-1" data-mortaged="0" data-grouped="0" data-rentindex="3" data-group="2">
							<div class="header">
								<button se-act="user_housing_down">-</button>
								<div class="rating"></div>
								<button se-act="user_housing_up">+</button>
							</div>
							<div class="body">
								<div class="name">cs_backalley</div>
								<div class="center">
									<div class="image">
										<img src="/snkeng/site/apps/games/games/megacorp/shared/img/cs_backalley.webp" />
									</div>
									<div class="userContainer"></div>
								</div>
								<div class="price">$100</div>
							</div>
						</div>
						<div class="tile" se-elem="card_chance_01">
							<div class="header"></div>
							<div class="body">
								<div class="name">CHANCE</div>
								<div class="center">
									<div class="image">
										<img src="/snkeng/site/apps/games/games/megacorp/shared/img/terrorist_elite_crew.webp" />
									</div>
									<div class="userContainer"></div>
								</div>
								<div class="price"></div>
							</div>
						</div>
						<div class="tile" se-elem="land_g02_l02" data-ownercolor="-1" data-mortaged="0" data-grouped="0" data-rentindex="4" data-group="2">
							<div class="header">
								<button se-act="user_housing_down">-</button>
								<div class="rating"></div>
								<button se-act="user_housing_up">+</button>
							</div>
							<div class="body">
								<div class="name">cs_estate</div>
								<div class="center">
									<div class="image">
										<img src="/snkeng/site/apps/games/games/megacorp/shared/img/cs_estate.webp" />
									</div>
									<div class="userContainer"></div>
								</div>
								<div class="price">$100</div>
							</div>
						</div>
						<div class="tile" se-elem="land_g02_l03" data-ownercolor="-1" data-mortaged="0" data-grouped="0" data-rentindex="5" data-group="2">
							<div class="header">
								<button se-act="user_housing_down">-</button>
								<div class="rating"></div>
								<button se-act="user_housing_up">+</button>
							</div>
							<div class="body">
								<div class="name">de_storm</div>
								<div class="center">
									<div class="image">
										<img src="/snkeng/site/apps/games/games/megacorp/shared/img/de_storm.webp" />
									</div>
									<div class="userContainer"></div>
								</div>
								<div class="price">$120</div>
							</div>
						</div>
						<!-- NEXT ROW -->
						<div class="tile" se-elem="jail">
							<div class="header"></div>
							<div class="body">
								<div class="name">JAIL</div>
								<div class="center">
									<div class="image">
										<img src="/snkeng/site/apps/games/games/megacorp/shared/img/jail.webp"/>
									</div>
									<div class="userContainer"></div>
								</div>
								<div class="price"></div>
							</div>
						</div>
						<div class="tile" se-elem="land_g03_l01" data-ownercolor="-1" data-mortaged="1" data-grouped="0" data-rentindex="0" data-group="3">
							<div class="header">
								<button se-act="user_housing_down">-</button>
								<div class="rating"></div>
								<button se-act="user_housing_up">+</button>
							</div>
							<div class="body">
								<div class="name">de_survivor</div>
								<div class="center">
									<div class="image">
										<img src="/snkeng/site/apps/games/games/megacorp/shared/img/de_survivor.webp" />
									</div>
									<div class="userContainer"></div>
								</div>
								<div class="price">$140</div>
							</div>
						</div>
						<div class="tile" se-elem="utility_01" data-ownercolor="-1">
							<div class="header">
								<button se-act="user_housing_down">-</button>
								<div class="rating"></div>
								<button se-act="user_housing_up">+</button>
							</div>
							<div class="body">
								<div class="name">Water</div>
								<div class="center">
									<div class="image">
										<img src="/snkeng/site/apps/games/games/megacorp/shared/img/utility_02.jpg" />
									</div>
									<div class="userContainer"></div>
								</div>
								<div class="price">$150</div>
							</div>
						</div>
						<div class="tile" se-elem="land_g03_l02" data-ownercolor="-1" data-mortaged="0" data-grouped="0" data-rentindex="0" data-group="3">
							<div class="header">
								<button se-act="user_housing_down">-</button>
								<div class="rating"></div>
								<button se-act="user_housing_up">+</button>
							</div>
							<div class="body">
								<div class="name">cs_havana</div>
								<div class="center">
									<div class="image">
										<img src="/snkeng/site/apps/games/games/megacorp/shared/img/cs_havana.webp" />
									</div>
									<div class="userContainer"></div>
								</div>
								<div class="price">$140</div>
							</div>
						</div>
						<div class="tile" se-elem="land_g03_l03" data-ownercolor="-1" data-mortaged="0" data-grouped="0" data-rentindex="0" data-group="3">
							<div class="header">
								<button se-act="user_housing_down">-</button>
								<div class="rating"></div>
								<button se-act="user_housing_up">+</button>
							</div>
							<div class="body">
								<div class="name">cs_siege</div>
								<div class="center">
									<div class="image">
										<img src="/snkeng/site/apps/games/games/megacorp/shared/img/cs_siege.webp" />
									</div>
									<div class="userContainer"></div>
								</div>
								<div class="price">$160</div>
							</div>
						</div>
						<div class="tile" se-elem="train_02" data-ownercolor="-1">
							<div class="header">
								<button se-act="user_housing_down">-</button>
								<div class="rating"></div>
								<button se-act="user_housing_up">+</button>
							</div>
							<div class="body">
								<div class="name">TRAIN B</div>
								<div class="center">
									<div class="image">
										<img src="/snkeng/site/apps/games/games/megacorp/shared/img/de_train_b.webp" />
									</div>
									<div class="userContainer"></div>
								</div>
								<div class="price">$200</div>
							</div>
						</div>
						<div class="tile" se-elem="land_g04_l01" data-ownercolor="-1" data-mortaged="0" data-grouped="0" data-rentindex="0" data-group="4">
							<div class="header">
								<button se-act="user_housing_down">-</button>
								<div class="rating"></div>
								<button se-act="user_housing_up">+</button>
							</div>
							<div class="body">
								<div class="name">de_vertigo</div>
								<div class="center">
									<div class="image">
										<img src="/snkeng/site/apps/games/games/megacorp/shared/img/de_vertigo.webp" />
									</div>
									<div class="userContainer"></div>
								</div>
								<div class="price">$180</div>
							</div>
						</div>
						<div class="tile" se-elem="card_community_02">
							<div class="header"></div>
							<div class="body">
								<div class="name">COMMUNITY</div>
								<div class="center">
									<div class="image">
										<img src="/snkeng/site/apps/games/games/megacorp/shared/img/counter_terrorist_gign9.webp" />
									</div>
									<div class="userContainer"></div>
								</div>
								<div class="price"></div>
							</div>
						</div>
						<div class="tile" se-elem="land_g04_l02" data-ownercolor="-1" data-mortaged="0" data-grouped="0" data-rentindex="0" data-group="4">
							<div class="header">
								<button se-act="user_housing_down">-</button>
								<div class="rating"></div>
								<button se-act="user_housing_up">+</button>
							</div>
							<div class="body">
								<div class="name">cs_militia</div>
								<div class="center">
									<div class="image">
										<img src="/snkeng/site/apps/games/games/megacorp/shared/img/cs_militia.webp" />
									</div>
									<div class="userContainer"></div>
								</div>
								<div class="price">$180</div>
							</div>
						</div>
						<div class="tile" se-elem="land_g04_l03" data-ownercolor="-1" data-mortaged="0" data-grouped="0" data-rentindex="0" data-group="4">
							<div class="header">
								<button se-act="user_housing_down">-</button>
								<div class="rating"></div>
								<button se-act="user_housing_up">+</button>
							</div>
							<div class="body">
								<div class="name">de_inferno</div>
								<div class="center">
									<div class="image">
										<img src="/snkeng/site/apps/games/games/megacorp/shared/img/de_inferno.webp" />
									</div>
									<div class="userContainer"></div>
								</div>
								<div class="price">$200</div>
							</div>
						</div>
						<!-- ROW END -->
						<div class="tile" se-elem="free_park">
							<div class="header"></div>
							<div class="body">
								<div class="name">FREE PARK</div>
								<div class="center">
									<div class="image">
										<img src="/snkeng/site/apps/games/games/megacorp/shared/img/free_parking.webp" />
									</div>
									<div class="userContainer"></div>
								</div>
								<div class="price"></div>
							</div>
						</div>
						<div class="tile" se-elem="land_g05_l01" data-ownercolor="-1" data-mortaged="0" data-grouped="0" data-rentindex="0" data-group="5">
							<div class="header">
								<button se-act="user_housing_down">-</button>
								<div class="rating"></div>
								<button se-act="user_housing_up">+</button>
							</div>
							<div class="body">
								<div class="name">cs_assault</div>
								<div class="center">
									<div class="image">
										<img src="/snkeng/site/apps/games/games/megacorp/shared/img/cs_assault.webp" />
									</div>
									<div class="userContainer"></div>
								</div>
								<div class="price">$220</div>
							</div>
						</div>
						<div class="tile" se-elem="card_chance_02">
							<div class="header"></div>
							<div class="body">
								<div class="name">CHANCE</div>
								<div class="center">
									<div class="image">
										<img src="/snkeng/site/apps/games/games/megacorp/shared/img/terrorist_elite_crew.webp" />
									</div>
									<div class="userContainer"></div>
								</div>
								<div class="price"></div>
							</div>
						</div>
						<div class="tile" se-elem="land_g05_l02" data-ownercolor="-1" data-mortaged="0" data-grouped="0" data-rentindex="0" data-group="5">
							<div class="header">
								<button se-act="user_housing_down">-</button>
								<div class="rating"></div>
								<button se-act="user_housing_up">+</button>
							</div>
							<div class="body">
								<div class="name">cs_italy</div>
								<div class="center">
									<div class="image">
										<img src="/snkeng/site/apps/games/games/megacorp/shared/img/cs_italy.webp" />
									</div>
									<div class="userContainer"></div>
								</div>
								<div class="price">$220</div>
							</div>
						</div>
						<div class="tile" se-elem="land_g05_l03" data-ownercolor="-1" data-mortaged="0" data-grouped="0" data-rentindex="0" data-group="5">
							<div class="header">
								<button se-act="user_housing_down">-</button>
								<div class="rating"></div>
								<button se-act="user_housing_up">+</button>
							</div>
							<div class="body">
								<div class="name">de_piranesi</div>
								<div class="center">
									<div class="image">
										<img src="/snkeng/site/apps/games/games/megacorp/shared/img/de_piranesi.webp" />
									</div>
									<div class="userContainer"></div>
								</div>
								<div class="price">$240</div>
							</div>
						</div>
						<div class="tile" se-elem="train_03" data-ownercolor="-1">
							<div class="header">
								<button se-act="user_housing_down">-</button>
								<div class="rating"></div>
								<button se-act="user_housing_up">+</button>
							</div>
							<div class="body">
								<div class="name">TRAIN C</div>
								<div class="center">
									<div class="image">
										<img src="/snkeng/site/apps/games/games/megacorp/shared/img/de_train_c.webp" />
									</div>
									<div class="userContainer"></div>
								</div>
								<div class="price">$200</div>
							</div>
						</div>
						<div class="tile" se-elem="land_g06_l01" data-ownercolor="-1" data-mortaged="0" data-grouped="0" data-rentindex="0" data-group="6">
							<div class="header">
								<button se-act="user_housing_down">-</button>
								<div class="rating"></div>
								<button se-act="user_housing_up">+</button>
							</div>
							<div class="body">
								<div class="name">de_prodigy</div>
								<div class="center">
									<div class="image">
										<img src="/snkeng/site/apps/games/games/megacorp/shared/img/de_prodigy.webp" />
									</div>
									<div class="userContainer"></div>
								</div>
								<div class="price">$260</div>
							</div>
						</div>
						<div class="tile" se-elem="land_g06_l02" data-ownercolor="-1" data-mortaged="0" data-grouped="0" data-rentindex="0" data-group="6">
							<div class="header">
								<button se-act="user_housing_down">-</button>
								<div class="rating"></div>
								<button se-act="user_housing_up">+</button>
							</div>
							<div class="body">
								<div class="name">de_chateau</div>
								<div class="center">
									<div class="image">
										<img src="/snkeng/site/apps/games/games/megacorp/shared/img/de_chateau.webp" />
									</div>
									<div class="userContainer"></div>
								</div>
								<div class="price">$260</div>
							</div>
						</div>
						<div class="tile" se-elem="utility_02" data-ownercolor="-1">
							<div class="header">
								<button se-act="user_housing_down">-</button>
								<div class="rating"></div>
								<button se-act="user_housing_up">+</button>
							</div>
							<div class="body">
								<div class="name">Electricity</div>
								<div class="center">
									<div class="image">
										<img src="/snkeng/site/apps/games/games/megacorp/shared/img/utility_01.jpg"/>
									</div>
									<div class="userContainer"></div>
								</div>
								<div class="price">$150</div>
							</div>
						</div>
						<div class="tile" se-elem="land_g06_l03" data-ownercolor="-1" data-mortaged="0" data-grouped="0" data-rentindex="0" data-group="6">
							<div class="header">
								<button se-act="user_housing_down">-</button>
								<div class="rating"></div>
								<button se-act="user_housing_up">+</button>
							</div>
							<div class="body">
								<div class="name">cs_office</div>
								<div class="center">
									<div class="image">
										<img src="/snkeng/site/apps/games/games/megacorp/shared/img/cs_office.webp" />
									</div>
									<div class="userContainer"></div>
								</div>
								<div class="price">$280</div>
							</div>
						</div>
						<!-- END ROW -->
						<div class="tile" se-elem="jail_go">
							<div class="header"></div>
							<div class="body">
								<div class="name">GO JAIL</div>
								<div class="center">
									<div class="image">
										<img src="/snkeng/site/apps/games/games/megacorp/shared/img/go_jail.jpg" />
									</div>
									<div class="userContainer"></div>
								</div>
								<div class="price"></div>
							</div>
						</div>
						<div class="tile" se-elem="land_g07_l01" data-ownercolor="-1" data-mortaged="0" data-grouped="0" data-rentindex="0" data-group="7">
							<div class="header">
								<button se-act="user_housing_down">-</button>
								<div class="rating"></div>
								<button se-act="user_housing_up">+</button>
							</div>
							<div class="body">
								<div class="name">de_cbble</div>
								<div class="center">
									<div class="image">
										<img src="/snkeng/site/apps/games/games/megacorp/shared/img/de_cbble.webp" />
									</div>
									<div class="userContainer"></div>
								</div>
								<div class="price">$300</div>
							</div>
						</div>
						<div class="tile" se-elem="land_g07_l02" data-ownercolor="-1" data-mortaged="0" data-grouped="0" data-rentindex="0" data-group="7">
							<div class="header">
								<button se-act="user_housing_down">-</button>
								<div class="rating"></div>
								<button se-act="user_housing_up">+</button>
							</div>
							<div class="body">
								<div class="name">de_nuke</div>
								<div class="center">
									<div class="image">
										<img src="/snkeng/site/apps/games/games/megacorp/shared/img/de_nuke.webp"/>
									</div>
									<div class="userContainer"></div>
								</div>
								<div class="price">$300</div>
							</div>
						</div>
						<div class="tile" se-elem="card_community_03">
							<div class="header"></div>
							<div class="body">
								<div class="name">COMMUNITY</div>
								<div class="center">
									<div class="image">
										<img src="/snkeng/site/apps/games/games/megacorp/shared/img/counter_terrorist_gign9.webp"/>
									</div>
									<div class="userContainer"></div>
								</div>
								<div class="price"></div>
							</div>
						</div>
						<div class="tile" se-elem="land_g07_l03" data-ownercolor="-1" data-mortaged="0" data-grouped="0" data-rentindex="0" data-group="7">
							<div class="header">
								<button se-act="user_housing_down">-</button>
								<div class="rating"></div>
								<button se-act="user_housing_up">+</button>
							</div>
							<div class="body">
								<div class="name">de_aztec</div>
								<div class="center">
									<div class="image">
										<img src="/snkeng/site/apps/games/games/megacorp/shared/img/de_aztec.webp" />
									</div>
									<div class="userContainer"></div>
								</div>
								<div class="price">$320</div>
							</div>
						</div>
						<div class="tile" se-elem="train_04" data-ownercolor="-1">
							<div class="header">
								<button se-act="user_housing_down">-</button>
								<div class="rating"></div>
								<button se-act="user_housing_up">+</button>
							</div>
							<div class="body">
								<div class="name">TRAIN D</div>
								<div class="center">
									<div class="image">
										<img src="/snkeng/site/apps/games/games/megacorp/shared/img/de_train_d.webp" />
									</div>
									<div class="userContainer"></div>
								</div>
								<div class="price">$200</div>
							</div>
						</div>
						<div class="tile" se-elem="card_chance_03">
							<div class="header"></div>
							<div class="body">
								<div class="name">CHANCE</div>
								<div class="center">
									<div class="image">
										<img src="/snkeng/site/apps/games/games/megacorp/shared/img/terrorist_elite_crew.webp"/>
									</div>
									<div class="userContainer"></div>
								</div>
								<div class="price"></div>
							</div>
						</div>
						<div class="tile" se-elem="land_g08_l01" data-ownercolor="-1" data-mortaged="0" data-grouped="0" data-rentindex="0" data-group="8">
							<div class="header">
								<button se-act="user_housing_down">-</button>
								<div class="rating"></div>
								<button se-act="user_housing_up">+</button>
							</div>
							<div class="body">
								<div class="name">de_dust</div>
								<div class="center">
									<div class="image">
										<img src="/snkeng/site/apps/games/games/megacorp/shared/img/de_dust.webp"/>
									</div>
									<div class="userContainer"></div>
								</div>
								<div class="price">$350</div>
							</div>
						</div>
						<div class="tile" se-elem="tax_luxury">
							<div class="header"></div>
							<div class="body">
								<div class="name">TAX LUXURY</div>
								<div class="center">
									<div class="image">
										<img src="/snkeng/site/apps/games/games/megacorp/shared/img/awp.webp" />
									</div>
									<div class="userContainer"></div>
								</div>
								<div class="price">-$200</div>
							</div>
						</div>
						<div class="tile" se-elem="land_g08_l02" data-ownercolor="-1" data-mortaged="0" data-grouped="0" data-rentindex="0" data-group="8">
							<div class="header">
								<button se-act="user_housing_down">-</button>
								<div class="rating"></div>
								<button se-act="user_housing_up">+</button>
							</div>
							<div class="body">
								<div class="name">de_dust2</div>
								<div class="center">
									<div class="image">
										<img src="/snkeng/site/apps/games/games/megacorp/shared/img/de_dust2.webp"/>
									</div>
									<div class="userContainer"></div>
								</div>
								<div class="price">$400</div>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="bigBar">
				<div>
					<div class="boardSectionTitle">MEGACORP</div>
					<div class="liveConsole"></div>
				</div>
				
				<div class="userActions">

					<div class="userActionOption" se-elem="roll" aria-hidden="true">
						<div class="title">Movimiento</div>
						<div se-elem="prison">
							<div class="notification warning">Jugador en prisión</div>
							<button class="btn wide blue" se-act="userAction_roll_prison_exit_roll"><svg class="icon inline mr"><use xlink:href="#fa-flickr" /></svg>Tiro exitoso (salir)</button>
							<button class="btn wide blue" se-act="userAction_roll_prison_exit_pay"><svg class="icon inline mr"><use xlink:href="#fa-dollar" /></svg>Pagar -$50 (salir)</button>
							<button class="btn wide blue" se-act="userAction_roll_prison_exit_card"><svg class="icon inline mr"><use xlink:href="#fa-file" /></svg>Tarjeta para salir</button>
							<button class="btn wide blue" se-act="userAction_roll_prison_stay"><svg class="icon inline mr"><use xlink:href="#fa-legal" /></svg>Turno en prisión</button>
						</div>
						<div se-elem="normal">
							<form class="se_form" se-elem="roll">
								<input type="hidden" name="location" />
								Valor de los dados:
								<div class="inputSlider">
									<input type="range" name="ammount" min="2" max="12" value="2" />
									<input type="number" min="2" max="12" step="1" value="2" />
								</div>
								<label>
									<input type="checkbox" name="double" />
									<span>Fue doble</span>
								</label>
								<button class="btn wide blue" type="submit"><svg class="icon inline mr"><use xlink:href="#fa-check" /></svg>Tirar</button>
							</form>
						</div>
						<button class="btn wide red" se-act="userAction_roll_cancel"><svg class="icon inline mr"><use xlink:href="#fa-remove" /></svg>Cancelar</button>
					</div>

					<div class="userActionOption" se-elem="housing" aria-hidden="true">
						<div class="title">Housing</div>
						<div class="grid">
							<div class="gr_sz08">
								<table class="se_table wide alternate border" se-elem="housingOps">
									<template>
									<tr><td>!cLocationName;</td><td>!opType;</td><td>!unitPrice;</td><td>!quantity;</td><td class="cost" data-sign="!sign;">!total;</td></tr>
									</template>
									<thead>
									<tr class="titles"><th>Propiedad</th><th>Tipo</th><th>U</th><th>Veces</th><th>Total</th></tr>
									</thead>
									<tbody></tbody>
								</table>
							</div>
							<div class="gr_04">
								<div class="gOMB">Total: $<span se-elem="total">0</span>. Válido: <span class="fakeCheck" se-elem="valid" data-value="1"></span></div>
								<button class="btn wide green" se-act="userAction_housing_apply"><svg class="icon inline mr"><use xlink:href="#fa-check" /></svg>Aplicar</button>
								<button class="btn wide blue" se-act="userAction_housing_reset"><svg class="icon inline mr"><use xlink:href="#fa-refresh" /></svg>Reiniciar</button>
								<button class="btn wide red" se-act="userAction_housing_close"><svg class="icon inline mr"><use xlink:href="#fa-remove" /></svg>Cerrar</button>
							</div>
						</div>
					</div>

					<div class="userActionOption" se-elem="trading" aria-hidden="true">
						<div class="title">Trading</div>
						<div class="">
							<div class="gOMB grid">
								<div class="gr_sz06 tradingObj" se-elem="offer">
									<div class="subTitle">Oferta</div>
									<table class="simpleSpacer"">
										<tr>
											<td><svg class="icon inline"><use xlink:href="#fa-flag" /></svg></td><td se-elem="territories">0</td>
										</tr>
										<tr>
											<td>$</td>
											<td><input class="moneyTransfer" type="number" name="money_offer" min="0" max="3000" /></td>
										</tr>
									</table>
									<div se-elem="lands"></div>
								</div>
								<div class="gr_sz06" se-elem="request">
									<div class="subTitle">Solicitud</div>
									
									<table class="simpleSpacer"">
										<tr>
											<td><svg class="icon inline"><use xlink:href="#fa-flag" /></svg></td><td se-elem="territories">0</td>
										</tr>
										<tr>
											<td>$</td>
											<td><input class="moneyTransfer" type="number" name="money_request" min="0" max="3000" /></td>
										</tr>
									</table>
									<div se-elem="lands"></div>
								</div>
							</div>
							<div class="">
								<button class="btn wide green" se-act="userAction_trading_apply"><svg class="icon inline mr"><use xlink:href="#fa-check" /></svg>Aplicar</button>
								<button class="btn wide blue" se-act="userAction_trading_reset"><svg class="icon inline mr"><use xlink:href="#fa-refresh" /></svg>Reiniciar</button>
								<button class="btn wide red" se-act="userAction_trading_close"><svg class="icon inline mr"><use xlink:href="#fa-remove" /></svg>Cerrar</button>
							</div>
						</div>
					</div>

				</div>

				<div class="lat_bar_section">
					<div class="title">Jugadores</div>
					<table class="se_table alternate border wide" se-elem="playerList">
						<template>
							<tr data-color="!playerColor;" data-turn="0" data-isalive="!isAlive;">
								<td class="name">!playerName;</td>
								<td se-elem="totalTerritories">!totalTerritories;</td>
								<td se-elem="totalGroups">!totalGroups;</td>
								<td se-elem="totalHouses">!totalHouses;</td>
								<td se-elem="totalHotels">!totalHotels;</td>
								<td se-elem="totalTrains">!totalTrains;</td>
								<td se-elem="totalUtilities">!totalUtilities;</td>
								<td><button se-act="playerQR" data-userid="!userId;" data-userkey="!userKey;"><svg class="icon inline"><use xlink:href="#fa-eye" /></svg></button></td>
							</tr>
						</template>
						<thead>
							<tr class="titles">
								<th>Jugador</th>
								<th><svg class="icon inline"><use xlink:href="#fa-flag" /></svg></th>
								<th><svg class="icon inline"><use xlink:href="#fa-tags" /></svg></th>
								<th><svg class="icon inline"><use xlink:href="#fa-home" /></svg></th>
								<th><svg class="icon inline"><use xlink:href="#fa-building" /></svg></th>
								<th><svg class="icon inline"><use xlink:href="#fa-train" /></svg></th>
								<th><svg class="icon inline"><use xlink:href="#fa-shower" /></svg></th>
								<th><svg class="icon inline"><use xlink:href="#fa-mobile-phone" /></svg></th>
							</tr>
						</thead>
						<tbody se-elem="content"></tbody>
					</table>
				</div>
			</div>
			
			<div class="smallBar">
				<div>
					<button class="circle_button" se-act="optionsOpen"><svg class="icon"><use xlink:href="#fa-gear" /></svg></button>
				</div>
				
				<div>
					<div class="faseDependent mainGame">
						<div class="options">
							<button class="circle_button" se-act="userAction_roll_open"><svg class="icon"><use xlink:href="#fa-flickr" /></svg></button>
							<button class="circle_button" se-act="userAction_trade_open"><svg class="icon"><use xlink:href="#fa-file" /></svg></button>
							<button class="circle_button" se-act="userAction_housing_open"><svg class="icon"><use xlink:href="#fa-home" /></svg></button>
							<button class="circle_button" se-act="userAction_nextStep"><svg class="icon"><use xlink:href="#fa-arrow-right" /></svg></button>
						</div>
					</div>
				</div>
			</div>
		</div>

		<!-- END GAME -->
		<div data-screen-name="engGame" aria-hidden="true">

		</div>
	</div>
</div>
HTML;
