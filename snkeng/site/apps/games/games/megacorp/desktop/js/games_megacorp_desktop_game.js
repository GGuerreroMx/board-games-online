"use strict";

//
se.plugin.games_megacorp_desktop_game = function (plugElem) {
	//
	let gameStatus = null,
		boardName = 'risk',
		properties = {
			gameId:intVal(plugElem.se_data('gameid')),
			gameKey:plugElem.se_data('gamekey'),
		},
		qrGenerator,
		//
		userActionActive = null,
		housingOps = {},
		usrTradingOps = {}
		;

	//
	const
		//
		boardOptions = plugElem.querySelector('#boardOptions'),
		//
		endOfGame = plugElem.querySelector('#endOfGame'),
		//
		liveConsole = plugElem.querySelector('div.liveConsole'),
		//
		userPlayerTable = plugElem.querySelector('table[se-elem="playerList"]'),
		userPlayerTableTemplate = userPlayerTable.querySelector('template'),
		userPlayerTableContent = userPlayerTable.querySelector('tbody'),
		//
		userQRDialog = plugElem.querySelector('div[se-elem="userQRDialog"]'),
		qrObject = userQRDialog.querySelector('div[se-elem="qrObject"]'),
		//
		// Specific
		//
		boardScreenContainer = plugElem.querySelector('div.boardScreenContainer'),
		mainBoard = boardScreenContainer.querySelector('div[data-screen-name="mainBoard"]'),
		boardMap = mainBoard.querySelector('div[se-elem="map"]'),

		// User Actions
		userActions = mainBoard.querySelector('div.userActions'),
		// Rolling
		userActionRollObj = userActions.querySelector('div[se-elem="roll"]'),
		userActionRollForm = userActionRollObj.querySelector('form[se-elem="roll"]'),
		userActionRollNumber = userActionRollForm.querySelector('input[type="number"]'),
		userActionRollSlider = userActionRollForm.querySelector('input[type="range"]'),
		// Housing
		userActionHousingObj = userActions.querySelector('div[se-elem="housing"]'),
		userActionHousingTableObj = userActionHousingObj.querySelector('table[se-elem="housingOps"]'),
		userActionHousingTableTemplate = userActionHousingTableObj.querySelector('template'),
		userActionHousingTableContent = userActionHousingTableObj.querySelector('tbody'),
		// Trading
		userActionTradingObj = userActions.querySelector('div[se-elem="trading"]'),

		//
		userLandFallWindow = plugElem.querySelector('#userLandFall'),
		userLandFallOptions = userLandFallWindow.querySelector('div.fallOptions'),
		userLandFallBuyForm = userLandFallWindow.querySelector('form[se-elem="buy"]'),
		userLandFallBidForm = userLandFallWindow.querySelector('form[se-elem="bid"]'),


		//
		//
		unitsMovement = plugElem.querySelector('#unitsMovement')
	;

	//
	function init() {
		console.log("MEGACORP loaded");

		// Bindings

		// General
		plugElem.se_on('click', 'button[se-act]', userAction);
		// User QR
		userQRDialog.se_plugin('pseudoDialog');
		qrGenerator = new QRCode(qrObject, {});

		// Specific

		// Roll
		userActionRollSlider.se_on('input', userActionRollSliderChange);
		userActionRollNumber.se_on('change', userActionRollNumberChange);
		userActionRollForm.se_on('submit', userActionRollOpSubmit);

		// Property buy/bid
		userLandFallBuyForm.se_on('submit', userActionBuySubmit);
		userLandFallBidForm.se_on('submit', userActionBidSubmit);

		// Trade
		boardMap.se_on('click', 'div.tile[data-selectable="1"]', user_trading_select);
		userActionTradingObj.se_on('change', 'input', user_trading_money);


		// Startup


		// Get current board status
		boardGames.func.loadBoard(boardName, properties.gameId, properties.gameKey,
			(msg) => {
				console.log("RECIEVED DATA", msg);
				let firstLoad = false;

				// First time load
				if ( msg.statusGeneral === 1 ) {
					// Need to build game
					first_setup_operation(msg.players);
					firstLoad = true;
				}
				else {
					// Keep game status from server
					gameStatus = msg.gameData;
				}

				//
				console.log("BOARD STATUS", gameStatus);

				// TODO: REMOVE THIS TEST
				for ( let i = 0; i < 4; i++ ) {
					user_recalculate_debtValue(i);
				}

				// Advance to last saved status
				board_status_apply();
				user_list_print();
				mainBoard_update();
				user_turn_apply(firstLoad);

				// MQTT
				mqtt_connector.connect('desktop', properties.gameId, mqttMessageIn, mqttReportUpdate);
			},
			() => {
				alert("el juego no cargó");
			}
		);

	}

	//
	function mqttMessageIn(msgHeader, msgData) {
		console.log("msgin", msgHeader);
		//
		switch ( msgHeader[2] ) {
			//
			case 'userOp':
				switch ( msgHeader[3] ) {
					//
					case 'discard':
						if ( !msgData.card ) {
							console.error("datos no válidos", msgData);
							return;
						}
						//
						user_card_discard(msgData.userId, msgData.card);
						break;
					//
					default:
						console.error("incomming message not valid user op", msgHeader);
						break;
				}
				break;
			//
			default:
				console.error("incomming message not valid", msgHeader);
				break;
		}
	}

	//
	function resetGame() {
		// Get user props
		let cUsers = [];
		for ( let cUser of gameStatus.players ) {
			cUsers.push( se.object.copy(cUser.props) );
		}

		//
		first_setup_operation(cUsers);

		//
		board_status_apply();

		// Send update
		gameStatus_save();
		mqttReportUpdate()
	}

	//
	function board_newFase_set(faseName) {
		// Set parameters
		gameStatus.status.fase = faseName;
		gameStatus.status.faseRounds = 0;
		gameStatus.status.userTurn = 0;

		// Update board
		board_status_apply();

		//
		console.log("GAME - NEW FASE: ", faseName);
	}

	//
	function board_status_apply() {
		//
		switch ( gameStatus.status.fase ) {
			//
			case 'mainGame':
				board_switch_screen('mainBoard');
				break;
			//
			default:
				console.error("learn to code, game status fase not defined...", gameStatus);
				break;
		}
	}

	//
	function board_notification_set(text) {
		liveConsole.se_text(text);
		// Fade out
		setTimeout(() => {
			liveConsole.se_text('');
		}, 4000)
	}

	//
	function board_switch_screen(screenName) {
		let targetScreen = boardScreenContainer.querySelector('div[data-screen-name="' + screenName + '"]');
		//
		if ( !targetScreen ) {
			console.error("wrong board?", screenName);
			return;
		}

		// Set current fase, for menus
		targetScreen.se_data('fase', gameStatus.status.fase);

		// Check if selected and skip if it is
		if ( targetScreen.se_attr('aria-hidden') === 'false' ) {
			return;
		}
		//
		console.log("SWITCHING TO", screenName, targetScreen);
		//
		boardScreenContainer.querySelectorAll('* > div[data-screen-name]').se_attr('aria-hidden', 'true');
		targetScreen.se_attr('aria-hidden', 'false');
	}

	//
	function user_list_print() {
		userPlayerTableContent.se_empty();
		let tableStructure = userPlayerTableTemplate.se_html(),
			locationsArray = Object.keys(boardGames.megacorp.properties.map.locations);

		//
		for ( let userIndex in gameStatus.players ) {
			if ( !gameStatus.players.hasOwnProperty(userIndex) ) { continue; }

			let cUserData = gameStatus.players[userIndex];

			// Print table
			let cRow = userPlayerTableContent.se_append(se.struct.stringPopulate(tableStructure, {
				userId:cUserData.props.id,
				userKey:cUserData.props.iKey,
				playerColor:cUserData.props.color,
				playerName:cUserData.props.name,

				isAlive:( cUserData.data['isAlive'] ) ? 1 : 0,
				totalTerritories:cUserData.data.totalTerritories,
				totalGroups:cUserData.data.totalGroups,
				totalHouses:cUserData.data.totalHouses,
				totalHotels:cUserData.data.totalHotels,
				totalUtilities:cUserData.data.totalUtilities,
				totalTrains:cUserData.data.totalTrains,

			}));

			// Print in board
			if ( cUserData.data.isAlive ) {
				let locationName = locationsArray[cUserData.data.userLocation];
				boardMap.querySelector('div.tile[se-elem="'+locationName+'"] div.userContainer').se_append('<div class="userToken" data-color="' + cUserData.props.color + '"></div>');
			}
		}

		//
	}

	//
	function user_list_update() {
		for ( const [cUserIndex, cUserData] of Object.entries(gameStatus.players) ) {
			let cRow = userPlayerTableContent.children[cUserIndex];

			//
			cRow.se_data('isalive', ( cUserData.data['isAlive'] ) ? 1 : 0);
			console.log("cRow updated", cUserData.data['isAlive'], ( cUserData.data['isAlive'] ) ? 1 : 0);

			//
			se.element.childrenUpdate(cRow,
				{
					'td[se-elem="totalTerritories"]' : [
						['text', cUserData.data['totalTerritories']]
					],
					'td[se-elem="totalGroups"]' : [
						['text', cUserData.data['totalGroups']]
					],
					'td[se-elem="totalHouses"]' : [
						['text', cUserData.data['totalHouses']]
					],
					'td[se-elem="totalHotels"]' : [
						['text', cUserData.data['totalHotels']]
					],
					'td[se-elem="totalUtilities"]' : [
						['text', cUserData.data['totalUtilities']]
					],
					'td[se-elem="totalTrains"]' : [
						['text', cUserData.data['totalTrains']]
					]
				}
			);
		}
	}

	//
	function mainBoard_update() {
		console.log("MAIN BOARD UPDATE");
		// blind update
		for ( const [cLocationName, cLocationData] of Object.entries(gameStatus.board.locations) ) {
			let cLocationTile = boardMap.querySelector('div.tile[se-elem="' + cLocationName + '"]'),
				cLocationProperties = boardGames.megacorp.properties.map.locations[cLocationName],
				priceValue = 0;

			//
			if ( !cLocationTile ) {
				console.error("No se encontró tile en el mapa.", cLocationName, cLocationTile, cLocationData, cLocationProperties);
				return;
			}

			// General

			cLocationTile.se_data('mortaged', ( cLocationData.mortaged ) ? 1 : 0);

			//
			if ( cLocationData.owner === -1 ) {
				priceValue = cLocationProperties.price;

				cLocationTile.se_data('ownercolor', -1);

				//
				switch ( cLocationProperties.type ) {
					case 'land':
						cLocationTile.se_data('grouped', 0);
						cLocationTile.se_data('rentindex', 0);
						break;
					case 'train':
						break;
					case 'utility':
						break;
				}
			}
			else {
				//
				let cUser = gameStatus.players[cLocationData.owner];

				//
				cLocationTile.se_data('ownercolor', cUser.props.color);

				//
				if ( !cLocationData.mortaged ) {
					switch ( cLocationProperties.type ) {
						case 'land':
							cLocationTile.se_data('grouped', (cLocationData.grouped) ? 1 : 0);
							cLocationTile.se_data('rentindex', cLocationData.rent_index);

							// Value
							if ( cLocationData.rent_index === 0 ) {
								priceValue = (cLocationData.grouped) ? cLocationProperties.rent_0 * 2 : cLocationProperties.rent_0;
							} else {
								priceValue = cLocationProperties['rent_' + cLocationData.rent_index];
							}

							break;
						case 'train':
							priceValue = 25 * Math.pow(2, cUser.data.totalTrains - 1);
							break;
						case 'utility':
							priceValue = "Datos * " + (gameStatus.players[cLocationData.owner].totalUtilities === 1) ? 4 : 10;
							break;
					}
				} else {
					priceValue = 0;
				}
			}

			cLocationTile.querySelector('.price').se_text("$" + priceValue);
		}
	}

	//<editor-fold desc="Fase Placement">

	//
	function first_setup_operation(playerList) {
		console.log("\n---\nFIRST TIME SETUP INI");

		// Copy initial conditions
		gameStatus = se.object.copy(boardGames.megacorp.initialStatus);

		// Mix decks
		console.log("CARD SHUFFLING");
		//
		for ( const [cardType, cards] of Object.entries(boardGames.megacorp.properties.cards) ) {
			let totalCards = cards.length,
				cardsArray = [];

			//
			for ( let i = 0; i < totalCards; i++ ) {
				cardsArray.push(i);
			}

			//
			gameStatus.cards[cardType].stack = se.array.shuffle(cardsArray);
			console.log("cards shuffled: ", cardType, gameStatus.cards[cardType].stack);
		}

		//
		console.log("USER SETUP", playerList);

		// Player add
		for ( let cPlayer of playerList ) {
			//
			gameStatus.players.push({
				props:{
					id: cPlayer.id,
					iKey: cPlayer.iKey,
					name: cPlayer.name,
					color: cPlayer.color,
				},
				data: {
					userLocation:0,
					isAlive:true,
					isInPrison:false,
					prisonTurns:0,
					prisonFreeCard:0,
					totalTerritories:0,
					totalGroups:0,
					totalHouses:0,
					totalHotels:0,
					totalUtilities:0,
					totalTrains:0,
					totalMoney:200,
					totalDebtValue:340,
					territoryList:[]
				}
			});
		}

		//
		if ( gameStatus.players.length < 3 ) {
			alert("cantidad de jugadores insuficientes");
		}

		//
		console.log("FIRST TIME SETUP END\n---\n");
	}

	//</editor-fold>

	//<editor-fold desc="Fase Main">

	//
	function user_mainGame_start() {
		gameStatus.status.turnInfo = {
			debtExists:false,
			debtAmmount:0,
			lastRoll:0,
			movementPending:true,
			movementCount:0,
		};
	}

	//
	function user_mainGame_endOfTurn() {
		console.log("user end of turn");
	}

	//</editor-fold>

	//<editor-fold desc="User turn ops">

	//
	function user_turn_reset() {
		gameStatus.status.userTurn = 0;
		user_turn_apply(true);
	}

	//
	function user_turn_apply(firsTime = true) {
		//
		if ( !userPlayerTableContent.children.length ) {
			console.error("no tengo idea porque puse esto XD");
			return;
		}

		console.debug("GAME USER TURN APPLY: ");

		//
		userPlayerTableContent.querySelectorAll('tr').se_data('turn', 0);
		userPlayerTableContent.children[gameStatus.status.userTurn].se_data('turn', 1);

		// Specific operations at start
		if ( firsTime ) {
			switch ( gameStatus.status.fase ) {
				//
				case 'mainGame':
					user_mainGame_start();
					user_set_step('main');
					break;
				//
				default:
					console.error("learn to code, game status fase not defined...", gameStatus);
					break;
			}
			//
			gameStatus_save();
		} else {
			// Set last user step
			user_set_step(gameStatus.status.userStep);
		}
	}

	//
	function user_turn_next() {
		do {
			gameStatus.status.userTurn++;
			if ( gameStatus.status.userTurn >= gameStatus.players.length ) {
				gameStatus.status.userTurn = 0;
				gameStatus.status.faseRounds++;
			}
		} while( !gameStatus.players[gameStatus.status.userTurn].data.isAlive );

		// Reset turn info (avoid collitions)
		gameStatus.status.turnInfo = {};

		// Automatic fase change (on supported games)
		switch ( gameStatus.status.fase ) {
			//
			case 'mainGame':
				// No special operations required
				break;
			//
			default:
				console.error("Current main fase not defined:", gameStatus.status);
				break;
		}

		//
		user_turn_apply(true);
	}

	//
	function user_step_next() {
		// Specific operations per fase
		switch ( gameStatus.status.fase ) {
			//
			case 'mainGame':
				// Check current step operations
				switch ( gameStatus.status.userStep ) {
					case 'main':
						// Ya se movió?
						if ( gameStatus.status.turnInfo.movementPending ) {
							board_notification_set("Usuario no se ha movido.");
							return;
						}
						// Deuda
						if ( gameStatus.status.turnInfo.debtExists ) {
							if ( gameStatus.players[gameStatus.status.userTurn].data.totalMoney >= gameStatus.status.turnInfo.debtAmmount ) {
								gameStatus.players[gameStatus.status.userTurn].data.totalMoney-= gameStatus.status.turnInfo.debtAmmount;
								gameStatus.players[gameStatus.status.userTurn].data.totalDebtValue-= gameStatus.status.turnInfo.debtAmmount;
							} else {
								board_notification_set("Usuario con deuda pendiente: $" + gameStatus.status.turnInfo.debtAmmount);
								return;
							}
						}

						// Advance to final stage
						user_set_step('endTurnEval');
						user_turn_next();
						break;
					default:
						console.error("current game status not defined", gameStatus.status);
						break;
				}
				break;
			//
			default:
				console.error("learn to code, game status fase not defined...", gameStatus);
				break;
		}
	}

	//
	function user_set_step(stepName) {
		boardScreenContainer.querySelector('div[data-screen-name="mainBoard"]').se_data('userStep', stepName);
		gameStatus.status.userStep = stepName;
	}

	//</editor-fold>

	//<editor-fold desc="User actions">

	//
	function userAction(e, cBtn) {
		e.preventDefault();

		//
		switch ( cBtn.se_attr('se-act') ) {
			//
			case 'optionsOpen':
				se.dialog.openModal(boardOptions);
				break;
			//
			case 'optionsClose':
				se.dialog.close(boardOptions);
				break;
			//
			case 'resetAll':
				if ( confirm('¿Borrar todo el juego?') ) {
					resetGame();
					se.dialog.close(boardOptions);
				}
				break;

			//
			case 'playerQR':
				userShowQR(cBtn);
				break;

			//
			case 'userAction_roll_open':
				userAction_roll_open();
				break;
			//
			case 'userAction_roll_prison_exit_roll':
				userAction_roll_prison_exit_roll();
				break;
			//
			case 'userAction_roll_prison_exit_pay':
				userAction_roll_prison_exit_pay();
				break;
			//
			case 'userAction_roll_prison_exit_card':
				userAction_roll_prison_exit_card();
				break;
			//
			case 'userAction_roll_prison_stay':
				userAction_roll_prison_stay();
				break;
			//
			case 'userAction_roll_cancel':
				userAction_roll_cancel();
				break;

			//
			case 'userAction_trade_open':
				user_trading_open();
				break;
			//
			case 'userAction_trading_apply':
				user_trading_apply();
				break;
			//
			case 'userAction_trading_reset':
				user_trading_reset();
				break;
			//
			case 'userAction_trading_close':
				user_trading_close()
				break;

			//
			case 'userAction_housing_open':
				user_housing_open()
				break;
			//
			case 'userAction_housing_apply':
				user_housing_apply();
				break;
			//
			case 'userAction_housing_reset':
				user_housing_reset();
				break;
			//
			case 'user_housing_up':
				user_housing_move(cBtn, 1);
				break;
			//
			case 'user_housing_down':
				user_housing_move(cBtn, -1);
				break;
			//
			case 'userAction_housing_close':
				user_housing_close();
				break;

			//
			case 'userAction_nextStep':
				user_step_next();
				break;

			//
			case 'userActionLandingClose':
				userActionLandingClose();
				break;

			//
			default:
				console.error("EVENTO DE BOTÓN NO DEFINIDO.", cBtn);
				break;
		}
		//
	}

	//</editor-fold>

	//<editor-fold desc="User Actions">

	//
	function userActionDisplay(target) {
		if ( target && userActionActive) {
			console.error("Existe una acción pendiente.");
			return false;
		}

		userActions.querySelectorAll('.userActionOption').se_attr('aria-hidden', 'true');
		if ( target ) {
			userActionActive = target;
			userActions.querySelector('.userActionOption[se-elem="' + target + '"]').se_attr('aria-hidden', 'false');
		} else {
			userActionActive = null;
		}

		return true;
	}


	//<editor-fold desc="User movement">

	//
	function userAction_roll_open() {
		//
		if ( !userActionDisplay('roll') ) {
			return;
		}

		// Is user in prison? (show options)
		if ( gameStatus.players[gameStatus.status.userTurn].data.isInPrison ) {
			userActionRollObj.querySelector('div[se-elem="prison"]').se_attr('aria-hidden', 'false');
			userActionRollObj.querySelector('div[se-elem="normal"]').se_attr('aria-hidden', 'true');
		} else {
			userActionRollObj.querySelector('div[se-elem="prison"]').se_attr('aria-hidden', 'true');
			userActionRollObj.querySelector('div[se-elem="normal"]').se_attr('aria-hidden', 'false');
		}
	}

	//
	function userAction_roll_prison_exit_roll() {
		//
		gameStatus.players[gameStatus.status.userTurn].data.isInPrison = false;
		gameStatus.players[gameStatus.status.userTurn].data.prisonTurns = 0;

		//
		userActionRollObj.querySelector('div[se-elem="prison"]').se_attr('aria-hidden', 'true');
		userActionRollObj.querySelector('div[se-elem="normal"]').se_attr('aria-hidden', 'false');
	}


	//
	function userAction_roll_prison_exit_pay() {
		if ( gameStatus.players[gameStatus.status.userTurn].data.totalMoney < 50 ) {
			board_notification_set("Usuario con fondos insuficientes.");
			return;
		}

		// Simple pay
		gameStatus.players[gameStatus.status.userTurn].data.totalMoney-= 50;
		gameStatus.players[gameStatus.status.userTurn].data.totalDebtValue-= 50;

		//
		userAction_roll_prison_exit_roll();
	}

	//
	function userAction_roll_prison_exit_card() {
		if ( gameStatus.players[gameStatus.status.userTurn].data.prisonFreeCard <= 0 ) {
			board_notification_set("Usuario no tiene la carta para salir de prisión.");
			return;
		}

		// Remove card
		gameStatus.players[gameStatus.status.userTurn].data.prisonFreeCard--;

		//
		userAction_roll_prison_exit_roll();
	}

	//
	function userAction_roll_prison_stay() {
		//
		gameStatus.status.turnInfo.movementPending = false;

		// Hide element
		userActionDisplay();
	}


	//
	function userAction_roll_cancel() {
		// Hide element
		userActionDisplay();
	}


	//
	function userActionRollNumberChange() {
		userActionRollSlider.value = userActionRollNumber.value;
	}

	//
	function userActionRollSliderChange() {
		userActionRollNumber.value = userActionRollSlider.value;
	}

	//
	function userActionRollOpSubmit(e) {
		e.preventDefault()

		//
		let userId = gameStatus.status.userTurn,
			cUserData = gameStatus.players[userId],
			rollValue = intVal(userActionRollNumber.value),
			isDouble = intVal(userActionRollForm.se_formElVal('double')),
			userData = gameStatus.players[userId],
			newLocation = gameStatus.players[userId].data.userLocation + rollValue,
			mapLengh = boardGames.megacorp.properties.map.locations.length,
			passTroughGo = true;

		if ( !gameStatus.status.turnInfo.movementPending ) {
			board_notification_set("Ya no es posible tirar de nuevo.");
			userAction_roll_cancel();
			return;
		}

		// Reset and add to go
		if ( newLocation < mapLengh ) {
			newLocation-= mapLengh;
		}

		// Get location by index
		let locationName = Object.keys(boardGames.megacorp.properties.map.locations)[newLocation];

		// Save movement:
		gameStatus.status.turnInfo.movementCount++;
		gameStatus.status.turnInfo.lastRoll = rollValue;
		gameStatus.status.turnInfo.movementPending = false;
		gameStatus.players[userId].data.userLocation = newLocation;


		// Check for double operation
		if ( isDouble ) {
			// is it jail?
			if ( gameStatus.status.turnInfo.movementCount >= 3 ) {
				board_notification_set("A prisión.");
				locationName = 'jail_go';
				passTroughGo = false;
			} else {
				gameStatus.status.turnInfo.movementPending = true;
			}
		}

		// resetForm
		userActionRollForm.reset();

		// Close
		userAction_roll_cancel();

		// actual movement
		userActionLandingExec(userId, locationName, passTroughGo);
	}

	//
	let userActionLandingNextStep = null;

	//
	function userActionLandingExec(playerIndex, mapTarget, passTroughGo, costModifier = 1) {
		// Set values
		let currentUser = gameStatus.players[playerIndex],
			currentUserLocation = currentUser.data.userLocation,
			locationList = Object.keys(boardGames.megacorp.properties.map.locations),
			locationIndex = locationList.indexOf(mapTarget),
			locationProperties = boardGames.megacorp.properties.map.locations[mapTarget],
			locationStatus = gameStatus.board.locations[mapTarget],
			cPlayerToken = mainBoard.querySelector('div.userToken[data-color="' + currentUser.props.color + '"]'),
			targetTile = mainBoard.querySelector('div.tile[se-elem="' + mapTarget + '"]'),
			cardType, modInfo;

		// Save user location
		currentUser.data.userLocation = locationIndex;

		// Remove card op
		userActionLandingNextStep = null;

		// Passed trough go, add the MONEYSSSSS
		if ( locationIndex < currentUserLocation && passTroughGo ) {
			user_moneyChange(playerIndex, 200);
		}

		// Move
		targetTile.querySelector('div.userContainer').se_append(cPlayerToken);

		// Hide options
		userLandFallOptions.children.se_attr('aria-hidden', 'true');

		//
		switch ( locationProperties.type ) {
			//
			case 'null':
				// Notify
				modInfo = userLandFallOptions.querySelector('div[se-elem="null"]').se_attr('aria-hidden', 'false');
				break;
			//
			case 'card':
				//
				let cDeck = null,
					cDeckProps;
				switch ( locationProperties.action ) {
					case 'chance':
						cardType = 'Chance';
						cDeck = gameStatus.cards.chance;
						cDeckProps = boardGames.megacorp.properties.cards.chance;
						break;
					case 'community':
						cardType = 'Community';
						cDeck = gameStatus.cards.community;
						cDeckProps = boardGames.megacorp.properties.cards.community;
						break;
					default:
						alert("whaaaat");
						return;
						break;
				}

				// Check shuffling
				if ( cDeck.stack.length < 1 ) {
					let tempArray = cDeck.stack.concat(cDeck.discard);
					cDeck.stack = se.array.shuffle(tempArray);
					cDeck.discard = [];
				}

				// Extract card
				let cCardId = cDeck.stack.shift(),
					cCardData = cDeckProps[cCardId];

				// Return card to discard
				cDeck.discard.push(cCardId);

				// What operation
				switch ( cCardData.type ) {
					//
					case 'move_to_land':
						//
						userActionLandingNextStep = {
							type:'move',
							target:cCardData.target
						};
						break;
					//
					case 'move_back_3':

						//
						userActionLandingNextStep = {
							type:'move',
							target:locationList.at(currentUser.data.userLocation - 3)
						};
						break;
					//
					case 'move_next_train':
						//
						userActionLandingNextStep = {
							type:'move',
							target:userActionFindNext('train', mapTarget),
							modifier:2
						};
						break;
					//
					case 'move_next_utility':
						//
						userActionLandingNextStep = {
							type:'move',
							target:userActionFindNext('utility', mapTarget),
							modifier:10
						};
						break;
					//
					case 'bonus_jailcard':
						currentUser.data.prisonFreeCard++;
						break;
					//
					case 'money':
						user_moneyChange(playerIndex, cCardData.value);
						break;
					//
					case 'complex_tax':
						let totalDebt = 0;

						totalDebt+= currentUser.data.totalHouses * cCardData.value_home;
						totalDebt+= currentUser.data.totalHotels * cCardData.value_hotel;

						console.debug("COMPLEX TAX", totalDebt, currentUser.data);

						//
						if ( totalDebt !== 0 ) {
							user_moneyChange(playerIndex, -totalDebt);
						}

						break;
					//
					case 'prison':
						//
						userActionLandingNextStep = {
							type:'prison'
						};
						break;
				}

				//
				modInfo = userLandFallOptions.querySelector('div[se-elem="card"]').se_attr('aria-hidden', 'false');
				modInfo.querySelector('div[se-elem="type"]').se_text(cardType);
				modInfo.querySelector('div.content').se_text(cCardData.text);
				break;

			//
			case 'event':
				// Notify
				modInfo = userLandFallOptions.querySelector('div[se-elem="event"]').se_attr('aria-hidden', 'false');

				//
				switch ( locationProperties.action ) {
					//
					case 'gojail':
						modInfo.querySelector('div[se-elem="type"]').se_text('Ir a la carcel');
						modInfo.querySelector('.value').se_text();

						//
						userActionLandingNextStep = {
							type:'prison'
						};
						break;
					// Tax
					case 'money_del':
						// Handle money loosing
						let consequence = user_money_lost(playerIndex, -1, locationProperties.value);
						switch ( consequence ) {
							case 'none':
							case 'debt':
								modInfo.querySelector('div[se-elem="type"]').se_text('Pago de impuestos');
								modInfo.querySelector('.value').se_text(locationProperties.value);
								break;
							//
							case 'defeat':
								//
								modInfo = userLandFallOptions.querySelector('div[se-elem="userDead"]').se_attr('aria-hidden', 'false');

								// Information
								modInfo.querySelector('.value').se_text(locationProperties.value);
								modInfo.querySelector('.name').se_text(currentUser.props.name);
								modInfo.querySelector('.killerName').se_text("El capitalismo.");
								break;
							//
							default:
								console.error("caso no programado.");
								break;
						}
						break;
					//
					default:
						console.error("Tipo de evento no programado...", locationProperties);
						break;
				}
				break;
			//
			case 'land':
			case 'train':
			case 'utility':
				//
				if ( locationStatus.owner === playerIndex ) {
					// In home, ignore, exit
					return;
				}
				else if ( locationStatus.owner === -1 ) {
					// No owner, sell

					//
					modInfo = userLandFallOptions.querySelector('div[se-elem="buy"]').se_attr('aria-hidden', 'false');

					// Information
					modInfo.querySelector('.value').se_text(locationProperties.price);

					// BuyForm form
					userLandFallBuyForm.se_formElVal('location', mapTarget);
					userLandFallBuyForm.se_formElVal('player', playerIndex);
					userLandFallBuyForm.se_formElVal('value', locationProperties.price);

					// Bid form
					userLandFallBidForm.se_formElVal('location', mapTarget);
					userLandFallBidForm.se_formElVal('value', 10);
					let playerOptions = userLandFallBidForm.se_formEl('player');
					for ( const [pIndex, pData] of Object.entries(gameStatus.players) ) {
						if ( pData.data.isAlive ) {
							let opt = document.createElement('option');
							opt.value = pIndex;
							opt.innerHTML = pData.props.name;
							playerOptions.appendChild(opt);
						}
					}


				}
				else if ( locationStatus.owner !== playerIndex && !locationStatus.mortaged ) {
					// submit payment
					let debtAmmount = 0;
					switch ( locationProperties.type ) {
						//
						case 'land':
							if ( locationStatus.rent_index === 0 ) {
								debtAmmount = ( locationStatus.grouped ) ? locationProperties.rent_0 * 2 : locationProperties.rent_0;
							} else {
								debtAmmount = locationProperties['rent_' + locationStatus.rent_index];
							}
							break;
						//
						case 'train':
							debtAmmount = 25 * Math.pow(2, gameStatus.players[locationStatus.owner].data.totalTrains - 1);
							if ( costModifier !== 1 ) {
								debtAmmount = debtAmmount * 2;
							}
							break;
						//
						case 'utility':
							let cFactor = ( gameStatus.players[locationStatus.owner].totalUtilities === 2 || costModifier !== 1 ) ? 10 : 4;
							debtAmmount = cFactor * gameStatus.status.turnInfo.lastRoll;
							break;
					}

					// Handle money loosing
					let consequence = user_money_lost(playerIndex, locationStatus.owner, debtAmmount);
					switch ( consequence ) {
						case 'none':
						case 'debt':
							//
							modInfo = userLandFallOptions.querySelector('div[se-elem="simplePay"]').se_attr('aria-hidden', 'false');
							break;
						//
						case 'defeat':
							//
							modInfo = userLandFallOptions.querySelector('div[se-elem="userDead"]').se_attr('aria-hidden', 'false');

							// Information
							modInfo.querySelector('.name').se_text(currentUser.props.name);
							break;
						default:
							console.error("caso no programado.");
							break;
					}

					// Información general
					modInfo.querySelector('.value').se_text(debtAmmount);
					modInfo.querySelector('.userNameDisplay').se_text(gameStatus.players[locationStatus.owner].props.name).se_data('color', gameStatus.players[locationStatus.owner].props.color);
				}
				break;
			//
			default:
				console.error("tipo de casilla no definida.", locationProperties.type, locationProperties);
				break;
		}

		// Copiar tarjeta

		let locationTile = userLandFallWindow.querySelector('div.tile'),
			originalTile = boardMap.querySelector('div.tile[se-elem="' + mapTarget + '"]');

		// Update new tile
		locationTile.se_data('group', originalTile.se_data('group'));
		locationTile.querySelector('.name').se_text(originalTile.querySelector('.name').se_text());
		locationTile.querySelector('img').se_attr('src', originalTile.querySelector('img').se_attr('src'));

		// Finally show modal
		se.dialog.openModal(userLandFallWindow);

	}

	//
	function userActionFindNext(targetName, currentLocation) {
		let locArrayName = Object.keys(boardGames.megacorp.properties.map.locations),
			cIndex = locArrayName.indexOf(currentLocation),
			found = false,
			newLocation = null;

		console.warn("BUSCANDO: ", targetName);

		// Search
		do {
			cIndex++;

			// Reset if needed
			if ( cIndex >= locArrayName.length ) {
				cIndex = 0;
			}

			//
			newLocation = locArrayName[cIndex];
			console.log("locación: ", newLocation, newLocation.startsWith(targetName));

			//
			if ( newLocation.startsWith(targetName) ) {
				found = true;
			}
		} while ( !found );


		return newLocation;
	}

	//
	function userActionLandingClose() {
		if ( !userActionLandingNextStep ) {
			se.dialog.close(userLandFallWindow);
			return;
		}

		//
		let currentUser = gameStatus.players[gameStatus.status.userTurn];

		//
		switch ( userActionLandingNextStep.type ) {
			//
			case 'prison':
				//
				gameStatus.status.turnInfo.movementPending = false;
				currentUser.data.isInPrison = true;
				currentUser.data.prisonTurns = 3;

				userActionLandingExec(gameStatus.status.userTurn, 'jail', false);
				break;
			//
			case 'move':
				let cModifier = 1;
				if ( userActionLandingNextStep.hasOwnProperty('modifier') ) {
					cModifier = userActionLandingNextStep.modifier;
				}
				userActionLandingExec(gameStatus.status.userTurn, userActionLandingNextStep.target, true, cModifier);
				break;
		}

	}

	//
	function user_money_lost(playerIndex, playerTargetId, debtAmmount) {
		let payerOrigin = gameStatus.players[playerIndex];

		//
		if ( payerOrigin.data.totalDebtValue < debtAmmount ) {
			// Player will be removed, cleanup

			//
			let payerTarget = ( playerTargetId !== -1 ) ? gameStatus.players[playerTargetId] : null

			// Reset properties and transfer
			for ( const [cLocationName, cLocationProperties] of Object.entries(gameStatus.board.locations) ) {
				//
				if ( cLocationProperties.owner !== playerIndex ) {
					continue;
				}

				let cLocationData = gameStatus.board.locations[cLocationName];

				//
				cLocationData.owner = playerTargetId;

				// Is target bank?
				if ( playerTargetId === -1 ) {
					cLocationData.mortaged = false;
					cLocationData.rent_index = 0;
					cLocationData.grouped = false;
				} else {
					// Target is player, add to current riches
					cLocationData.mortaged = true;
					cLocationData.rent_index = 0;
					cLocationData.grouped = false;

					// Add to winning player
					let cLocationProps = boardGames.megacorp.properties.map.locations[cLocationName];
					switch ( cLocationProps.type ) {
						//
						case 'land':
							payerTarget.data.totalTerritories++;
							break;
						//
						case 'train':
							payerTarget.data.totalHouses++;
							break;
						//
						case 'utility':
							payerTarget.data.totalUtilities++;
							break;
					}
				}
			}

			// Move groups
			for ( const cGroup of Object.entries(gameStatus.group) ) {
				if ( cGroup.owner === playerIndex ) {
					cGroup.owner = playerTargetId;
				}
			}

			// Pass money to target player
			if ( playerTargetId !== -1 ) {
				user_moneyChange(playerTargetId, payerOrigin.data.totalDebtValue);
				user_recalculate_debtValue(playerTargetId);
			}

			// User game over reset values, remove from board.
			gameStatus.players[playerIndex].data = {
				userLocation:-1,
				isAlive:false,
				isInPrison:false,
				prisonTurns:0,
				prisonFreeCard:0,
				totalTerritories:0,
				totalGroups:0,
				totalHouses:0,
				totalHotels:0,
				totalUtilities:0,
				totalTrains:0,
				totalMoney:0,
				totalDebtValue:0,
				territoryList:[]
			};

			// Remove from board
			mainBoard.querySelector('div.userToken[data-color="' + gameStatus.players[playerIndex].props.color + '"]').se_remove();

			// Update list
			user_list_update();
			mainBoard_update();

			//
			return 'defeat';
		}
		else {
			// Simple pay
			user_moneyChange(playerIndex, -debtAmmount);
			// If not player, ignore
			if ( playerTargetId !== -1 ) {
				user_moneyChange(playerTargetId, +debtAmmount);
			}

			//
			return 'none';
		}
	}

	//
	function userActionBuySubmit(e) {
		e.preventDefault();
		console.log("get information, give property");
		//
		let cData = se.form.serializeToObj(userLandFallBuyForm);

		cData.player = intVal(cData.player);
		cData.value = intVal(cData.value);

		//
		if ( gameStatus.players[cData.player].data.totalMoney < cData.value ) {
			alert("fondos insuficientes");
			return;
		}

		//
		user_recieveProperty(cData.location, cData.player, cData.value);
		
		// Cerrar ventana
		se.dialog.close(userLandFallWindow);
	}

	//
	function userActionBidSubmit(e) {
		e.preventDefault();
		//
		let cData = se.form.serializeToObj(userLandFallBidForm);

		cData.player = intVal(cData.player);
		cData.value = intVal(cData.value);

		if ( gameStatus.players[cData.player].data.totalMoney < cData.value ) {
			alert("El usuario no tiene presupuesto.");
			return;
		}

		//
		user_recieveProperty(cData.location, cData.player, cData.value);

		// Cerrar ventana
		se.dialog.close(userLandFallWindow);
	}

	//
	function user_recieveProperty(propertyName, playerIndex, costValue) {
		let locationProperties = boardGames.megacorp.properties.map.locations[propertyName],
			locationStatus = gameStatus.board.locations[propertyName],
			cPlayer = gameStatus.players[playerIndex];

		//
		if ( locationStatus.owner !== -1 ) {
			console.error("SELL PROPERTY FROM BANK ERROR - Property not from bank", locationProperties, locationStatus);
			alert("error al vender esta propiedad");
			return;
		}

		// Process payment
		user_moneyChange(playerIndex, -costValue);

		// Move property
		locationStatus.owner = playerIndex;

		// Add mortage to debtValue
		cPlayer.data.totalDebtValue+= locationProperties.mortage;
		
		// Extra statistical information
		switch ( locationProperties.type ) {
			case 'land':
				cPlayer.data.totalTerritories++;
				let group = true;

				// Check for group unification
				for ( const [cLocationName, cLocationProperties] of Object.entries(boardGames.megacorp.properties.map.locations) ) {
					//
					if ( cLocationProperties.type !== 'land' || cLocationProperties.group !== locationProperties.group ) {
						continue;
					}
					let cLocationStatus = gameStatus.board.locations[cLocationName];
					//
					if ( cLocationStatus.owner !== playerIndex ) {
						group = false;
						break;
					}
				}

				// Mark as group?
				if ( group ) {
					for ( const [cLocationName, cLocationProperties] of Object.entries(boardGames.megacorp.properties.map.locations) ) {
						//
						if ( cLocationProperties.type !== 'land' || cLocationProperties.group !== locationProperties.group ) {
							continue;
						}
						//
						gameStatus.board.locations[cLocationName].grouped = true;
					}

					//
					gameStatus.group[locationProperties.group].owner = playerIndex;

					//
					cPlayer.data.totalGroups++;
				}
				break;
			case 'train':
				cPlayer.data.totalTrains++;
				break;
			case 'utility':
				cPlayer.data.totalUtilities++;
				break;
		}

		//
		mainBoard_update();
		user_list_update();
	}

	//
	function user_moneyChange(playerIndex, moneyDiff) {
		let currentUser = gameStatus.players[playerIndex];

		// Is positive or simply remove
		if ( moneyDiff > 0 || ( moneyDiff < 0 && currentUser.data.totalMoney > Math.abs(moneyDiff) ) ) {
			currentUser.data.totalMoney += moneyDiff;
			currentUser.data.totalDebtValue += moneyDiff;
		} else {
			gameStatus.status.turnInfo.debtExists = true;
			gameStatus.status.turnInfo.debtAmmount = Math.abs(moneyDiff);
		}
	}

	//</editor-fold>

	//<editor-fold desc="Trading">

	//
	function user_trading_open() {
		//
		if ( !userActionDisplay('trading') ) {
			return;
		}

		// Remove all non selectable elements
		boardMap.querySelectorAll('.tile').se_each((cIndex, cEl)=> {
			let cOwnerColor = cEl.se_data('ownercolor');
			if ( !cOwnerColor || intVal(cOwnerColor) === -1 ) {
				cEl.se_data('hidden', 1);
			} else {
				cEl.se_data('selectable', 1);
			}
		});

		// Initial conditions = reset conditions, avoid duplication.
		user_trading_reset();
	}

	//
	function user_trading_select(e, cLocationTile) {
		let cPropOwnerColor = intVal(cLocationTile.se_data('ownercolor')),
			cPlayerId = gameStatus.players.findIndex((cEl) => {
				return cEl.props.color === cPropOwnerColor
			}),
			turnUserId = gameStatus.status.userTurn;

		// Avoid double trading
		if ( !(turnUserId === cPlayerId) && usrTradingOps.userTarget !== -1 && usrTradingOps.userTarget !== cPlayerId ) {
			board_notification_set("Esta propiedad no es del usuario inicial.");
			console.error("this is a bug");
			return;
		}

		let cLocationName = cLocationTile.se_attr('se-elem'),
			cLocationData = gameStatus.board.locations[cLocationName],
			cLocationProps = boardGames.megacorp.properties.map.locations[cLocationName],
			selfUser = ( cPlayerId === turnUserId ),
			landsCheck = ( selfUser ) ? usrTradingOps.offer.lands : usrTradingOps.request.lands;

		console.log("PICK INFORMATION", intVal(cLocationTile.se_data('selected')), cLocationProps.type, cLocationData.grouped);

		// Is select or deselect
		if ( intVal(cLocationTile.se_data('selected')) === 0 ) {
			// Not selected, add
			if ( cLocationProps.type === 'land' && cLocationData.grouped ) {
				//
				for ( const [cLocationName, cLocationProperties] of Object.entries(boardGames.megacorp.properties.map.locations) ) {
					//
					if ( cLocationProperties.type !== 'land' || cLocationProperties.group !== cLocationProps.group ) {
						continue;
					}

					//
					landsCheck.push(cLocationName);

					// Update
					boardMap.querySelector('div.tile[se-elem="' + cLocationName + '"]').se_data('selected', 1);
				}
			} else {
				landsCheck.push(cLocationName);

				// Update
				cLocationTile.se_data('selected', 1);
			}

			// Set fixation
			if ( !selfUser ) {
				usrTradingOps.userTarget = cPlayerId;
			}
		}
		// Unselect
		else {
			//
			if ( cLocationProps.type === 'land' && cLocationData.grouped ) {
				//
				for ( const [cLocationName, cLocationProperties] of Object.entries(boardGames.megacorp.properties.map.locations) ) {
					//
					if ( cLocationProperties.type !== 'land' || cLocationProperties.group !== cLocationProps.group ) {
						continue;
					}

					//
					const index = landsCheck.indexOf(cLocationName);
					if ( index > -1 ) {
						landsCheck.splice(index, 1);
					}

					// Update
					boardMap.querySelector('div.tile[se-elem="' + cLocationName + '"]').se_data('selected', 0);
				}
			} else {
				const index = landsCheck.indexOf(cLocationName);
				if ( index > -1 ) {
					landsCheck.splice(index, 1);
				}

				// Update
				cLocationTile.se_data('selected', 0);
			}

			console.log("unselect current group is", landsCheck);

			// Check if remove fixation
			if ( !selfUser && landsCheck.length === 0 ) {
				usrTradingOps.userTarget = -1;
			}
		}

		// Update available trades
		let cUserColor = gameStatus.players[turnUserId].props.color,
			cTargetColor = ( usrTradingOps.userTarget === -1 ) ? -1 : gameStatus.players[usrTradingOps.userTarget].props.color;
		//
		boardMap.querySelectorAll('.tile[data-ownercolor]').se_each((cIndex, cEl)=> {
			let cOwnerColor = intVal(cEl.se_data('ownercolor'));
			if (
				cOwnerColor === cUserColor ||
				( cTargetColor === -1 && cOwnerColor !== -1 ) ||
				( cTargetColor !== -1 && cOwnerColor === cTargetColor )
			) {
				cEl.se_data('selectable', 1).se_data('hidden', 0);
			} else {
				cEl.se_data('selectable', 0).se_data('hidden', 1);
			}
		});

		// Display trade values
		userActionTradingObj.querySelector('div[se-elem="request"] td[se-elem="territories"]').se_text(usrTradingOps.request.lands.length);
		userActionTradingObj.querySelector('div[se-elem="offer"] td[se-elem="territories"]').se_text(usrTradingOps.offer.lands.length);

		//
		console.log("current offer: ", usrTradingOps);
	}

	//
	function user_trading_money(e, cInput) {
		//
		switch ( cInput.name ) {
			//
			case 'money_offer':
				usrTradingOps.offer.money = intVal(cInput.value);
				usrTradingOps.request.money = 0;
				userActionTradingObj.querySelectorAll('input[name="money_request"]').se_val(0);
				break;
			//
			case 'money_request':
				usrTradingOps.request.money = intVal(cInput.value);
				usrTradingOps.offer.money = 0;
				userActionTradingObj.querySelectorAll('input[name="money_offer"]').se_val(0);
				break;
		}
	}

	//
	function user_trading_apply() {
		console.log("ver como aplicar los gastos actuales", usrTradingOps);

		// Validar dinero
		if ( usrTradingOps.request.money !== 0 && gameStatus.players[usrTradingOps.userTarget].data.totalMoney < usrTradingOps.request.money ) {
			board_notification_set("Usuario destindo con fondos insuficientes.");
			return;
		}

		//
		if ( usrTradingOps.offer.money !== 0 && gameStatus.players[gameStatus.status.userTurn].data.totalMoney < usrTradingOps.offer.money) {
			board_notification_set("Usuario actual con fondos insuficientes.");
			return;
		}

		// Offer
		for ( let cPropertyName of usrTradingOps.offer.lands ) {
			gameStatus.board.locations[cPropertyName].owner = usrTradingOps.userTarget;
		}

		// Money
		if ( usrTradingOps.offer.money !== 0 ) {
			user_moneyChange(usrTradingOps.userTarget, -usrTradingOps.offer.money);
			user_moneyChange(gameStatus.status.userTurn, +usrTradingOps.offer.money);
		}

		// Request
		for ( let cPropertyName of usrTradingOps.request.lands ) {
			gameStatus.board.locations[cPropertyName].owner = gameStatus.status.userTurn;
		}
		if ( usrTradingOps.request.money !== 0 ) {
			user_moneyChange(usrTradingOps.userTarget, +usrTradingOps.request.money);
			user_moneyChange(gameStatus.status.userTurn, -usrTradingOps.request.money);
		}

		//
		user_recalculate_debtValue(gameStatus.status.userTurn);
		user_recalculate_debtValue(usrTradingOps.userTarget);

		// Update maps
		mainBoard_update();
		user_list_update();

		// Finally close
		user_trading_close();
	}

	//
	function user_trading_reset() {
		console.log("reiniciar los elementos");

		// Start conditions
		usrTradingOps = {
			userTarget:-1,
			offer:{
				lands:[],
				money:0
			},
			request:{
				lands:[],
				money:0
			},
		};

		//
		boardMap.querySelectorAll('.tile[data-selected="1"]').se_data('selected', 0);

		// Reset money
		userActionTradingObj.querySelectorAll('input').se_val(0);
		userActionTradingObj.querySelector('div[se-elem="request"] td[se-elem="territories"]').se_text(0);
		userActionTradingObj.querySelector('div[se-elem="offer"] td[se-elem="territories"]').se_text(0);
		userActionHousingObj.querySelector('span[se-elem="total"]').se_text(0);
		userActionHousingObj.querySelector('span[se-elem="valid"]').se_data('value', 1);
	}

	//
	function user_trading_close() {
		// Reset counter and map
		usrTradingOps = {};

		// unfilter all
		boardMap.querySelectorAll('.tile').se_data('hidden', 0).se_data('selectable', 0).se_data('selected', 0);

		// Hide element
		userActionDisplay();
	}


	//</editor-fold>

	//<editor-fold desc="Housing operations">

	//
	function user_housing_open() {
		//
		//
		if ( !userActionDisplay('housing') ) {
			return;
		}

		//
		let userId = gameStatus.status.userTurn,
			userColor = gameStatus.players[userId].props.color;

		// filter all elements
		boardMap.se_data('housing', 1);
		boardMap.querySelectorAll('.tile').se_each((cIndex, cEl)=> {
			if ( intVal(cEl.se_data('ownercolor')) !== userColor ) {
				cEl.se_data('hidden', 1);
			}
		});

		// Reset housing ops
		user_housing_reset();
	}

	//
	function user_housing_apply() {
		console.log("ver como aplicar los gastos actuales");

		//
		if ( !housingOps.results.valid ) {
			board_notification_set("Las condiciones actuales no se pueden aplicar.");
			return;
		}

		let userId = gameStatus.status.userTurn,
			userData = gameStatus.players[userId];

		//
		if ( housingOps.results.total < 0 && userData.data.totalMoney < Math.abs(housingOps.results.total) ) {
			board_notification_set("Usuario sin presupuesto.");
			return;
		}

		// Save operations
		for ( const [cLocationName, cLocationHouseOps] of Object.entries(housingOps.locations) ) {
			let cLocationData = gameStatus.board.locations[cLocationName];
			//
			if ( cLocationHouseOps.mortaged !== 0 ) {
				cLocationData.mortaged = ( cLocationHouseOps.mortaged < 0 );
			}
			//
			if ( cLocationHouseOps.houses !== 0 ) {
				cLocationData.rent_index+= cLocationHouseOps.houses;
			}
		}

		// Save Money
		userData.data.totalMoney+= housingOps.results.total;

		// Recalculate user value
		user_recalculate_debtValue(gameStatus.status.userTurn);

		// Finally close
		user_housing_close();
	}

	//
	function user_housing_reset() {
		console.log("reiniciar los elementos");

		// Reset counter and map
		housingOps = {
			locations:{},
			results:{
				total:0,
				valid:false
			},
		};
		userActionHousingTableContent.se_empty();

		// Reset to initial conditions
		mainBoard_update();
	}

	//
	function user_housing_close() {
		// Reset counter and map
		housingOps = {};
		mainBoard_update();

		// unfilter all
		boardMap.se_data('housing', 0);
		boardMap.querySelectorAll('.tile').se_data('hidden', 0);

		// Hide element
		userActionDisplay();
	}

	//
	function user_housing_move(cBtn, dValue) {
		// Negative dValues should give money

		let userId = gameStatus.status.userTurn,
			userData = gameStatus.players[userId],
			cLocationTile = cBtn.se_closest('.tile'),
			cLocationName = cLocationTile.se_attr('se-elem'),
			cLocationData = gameStatus.board.locations[cLocationName],
			cLocationProps = boardGames.megacorp.properties.map.locations[cLocationName],
			cLocationHouseOp = null;

		// Is it a valid location?
		if ( cLocationData.owner !== userId ) {
			board_notification_set("La propiedad no corresponde al usuario");
			return;
		}

		// Set initial conditions
		if ( !housingOps.locations.hasOwnProperty(cLocationName) ) {
			housingOps.locations[cLocationName] = {
				houses:0,
				mortaged:0,
			};
		}

		// Reset results
		housingOps.results.total = 0;
		housingOps.results.valid = true;

		//
		cLocationHouseOp = housingOps.locations[cLocationName];

		//
		if ( dValue > 0 ) {
			// Add value -> loose money

			// Check if mortaged, unmortage
			if ( cLocationData.mortaged && ( cLocationHouseOp.mortaged >= 0 ) ) {
				cLocationHouseOp.mortaged = 1; // Remove mortage
			}
			if ( !cLocationData.mortaged && ( cLocationHouseOp.mortaged < 0 ) ) {
				cLocationHouseOp.mortaged = 0; // Remove mortage indicator
			}
			// If not upgradable, error
			else if ( cLocationProps.type === 'train' || cLocationProps.type === 'utility' ) {
				board_notification_set("Esta propiedad no puede subir de nivel.");
				return;
			}
			// Check if part of group
			else if ( !cLocationData.grouped ) {
				board_notification_set("No se cuenta con el grupo de propiedades.");
				return;
			}
			// Check if it is not a maximum
			else if ( ( cLocationData.rent_index + cLocationHouseOp.houses ) >= 5  ) {
				board_notification_set("Territorio maximizado.");
				return;
			} else {
				cLocationHouseOp.houses++;
			}
		} else {
			// Remove value -> win money

			// Remove houses
			if ( cLocationProps.type === 'land' && ( cLocationData.rent_index + cLocationHouseOp.houses ) > 0 ) {
				cLocationHouseOp.houses--;
			}
			// Mortage property
			else if ( !cLocationData.mortaged && ( cLocationHouseOp.mortaged === 0 || cLocationHouseOp.mortaged === 1 ) ) {
				cLocationHouseOp.mortaged = -1;
			}
			// Error
			else {
				board_notification_set("Esta propiedad no puede perder más valor.");
				return;
			}
		}

		// Update table, board visibility and processed results

		//
		let tableStructure = userActionHousingTableTemplate.se_html(),
			currentGroups = [];
		userActionHousingTableContent.se_empty();
		for ( const [cLocationName, cLocationHouseOps] of Object.entries(housingOps.locations) ) {
			let cLocationTile =boardMap.querySelector('.tile[se-elem="'+ cLocationName + '"]'),
				cLocationData = gameStatus.board.locations[cLocationName],
				cLocationProps = boardGames.megacorp.properties.map.locations[cLocationName],
				cTotal = 0;

			// Check if property is part of a group, save it
			if ( cLocationProps.type === 'land' && currentGroups.indexOf(cLocationProps.group) === -1 ) {
				currentGroups.push(cLocationProps.group);
			}

			// Mortage
			cLocationTile.se_data('mortaged', ( cLocationHouseOps.mortaged < 0 ) ? 1 : 0 );

			if ( cLocationHouseOps.mortaged !== 0 ) {
				cTotal = ( Math.sign(cLocationHouseOps.mortaged) < 0 ) ? cLocationProps.mortage : -cLocationProps.mortage * 1.1;
				housingOps.results.total+= cTotal;

				//
				userActionHousingTableContent.se_append(se.struct.stringPopulate(tableStructure, {
					cLocationName: cLocationTile.querySelector('.name').se_text(),
					opType: 'Hipoteca',
					unitPrice: cLocationProps.mortage,
					quantity: 1,
					sign: -Math.sign(cLocationHouseOps.mortage),
					total: cTotal,
				}));
			}

			// Houses
			cLocationTile.se_data('rentindex', Math.abs(cLocationHouseOps.houses + cLocationData.rent_index));
			if ( cLocationHouseOps.houses !== 0 ) {
				cTotal = ( Math.sign(cLocationHouseOps.houses) < 0 ) ? cLocationHouseOps.houses * cLocationProps.house * .5 : cLocationHouseOps.houses * cLocationProps.house ;

				housingOps.results.total-= cTotal;

				//
				userActionHousingTableContent.se_append(se.struct.stringPopulate(tableStructure, {
					cLocationName: cLocationTile.querySelector('.name').se_text(),
					opType: 'Casa',
					unitPrice: cLocationProps.house,
					quantity: Math.abs(cLocationHouseOps.houses),
					sign: -Math.sign(cLocationHouseOps.houses),
					total:-cTotal,
				}));
			}
		}

		// Validación
		housingOps.results.valid = true;
		for ( let cGroupId of currentGroups ) {
			let cGroupStatus = [];
			for ( const [cLocationName, cLocationProperties] of Object.entries(boardGames.megacorp.properties.map.locations) ) {
				//
				if ( cLocationProperties.type !== 'land' || cLocationProperties.group !== cGroupId ) {
					continue;
				}

				//
				let cLocationStatus = gameStatus.board.locations[cLocationName],
					cLocationMods = housingOps.locations[cLocationName],
					cHousingValue = ( cLocationStatus.mortaged ) ? 0 : 1 + cLocationStatus.rent_index;

				// Prop status
				if ( cLocationMods ) {
					cHousingValue+= cLocationMods.mortaged + cLocationMods.houses;
				}

				//
				cGroupStatus.push(cHousingValue);
			}

			// Final comparison
			let firstEl = cGroupStatus.shift();
			for ( let cEl of cGroupStatus ) {
				if ( Math.abs( firstEl - cEl) > 1 ) {
					housingOps.results.valid = false;
					break;
				}
			}
		}

		// Set final information
		userActionHousingObj.querySelector('span[se-elem="total"]').se_text(housingOps.results.total);
		userActionHousingObj.querySelector('span[se-elem="valid"]').se_data('value', (housingOps.results.valid) ? 1 : 0);
	}

	//</editor-fold>

	//
	function user_recalculate_debtValue(userId) {
		let userData = gameStatus.players[userId];

		//
		userData.data.totalDebtValue = userData.data.totalMoney;
		userData.data.totalTerritories = 0;
		userData.data.totalGroups = 0;
		userData.data.totalHouses = 0;
		userData.data.totalHotels = 0;
		userData.data.totalUtilities = 0;
		userData.data.totalTrains = 0;

		//
		for ( const [cLocationName, cLocationData] of Object.entries(gameStatus.board.locations) ) {
			let cLocationProperties = boardGames.megacorp.properties.map.locations[cLocationName];

			// Skill all non user properties or mortaged (since value = 0)
			if ( cLocationData.owner !== userId ) {
				continue;
			}

			//
			switch ( cLocationProperties.type ) {
				case 'land':
					userData.data.totalTerritories++;

					// House / Hotels
					if ( cLocationData.rent_index > 0 && cLocationData.rent_index < 5 ) {
						// Houses
						userData.data.totalHouses+= cLocationData.rent_index;
					} else if ( cLocationData.rent_index === 5 ) {
						userData.data.totalHotels++;
					}
					break;
				case 'train':
					userData.data.totalTrains++;
					break;
				case 'utility':
					userData.data.totalUtilities++;
					break;
			}


			// Avoid mortage in debt value
			if ( !cLocationData.mortaged ) {
				// Add default mortage value
				userData.data.totalDebtValue+= cLocationProperties.mortage;

				//
				if ( cLocationProperties.type === 'land' && cLocationData.rent_index > 0 ) {
					userData.data.totalDebtValue+= 0.5 * cLocationProperties.house * cLocationData.rent_index;
				}
			}
		}

		// Count groups
		for ( const cGroup of Object.entries(gameStatus.group) ) {
			if ( cGroup.owner === userId ) {
				userData.data.totalGroups++;
			}
		}

		//
		console.log("USER NEW DEBT VALUE", userId, userData.data.totalMoney, userData.data.totalDebtValue);
	}

	//</editor-fold>


	//<editor-fold desc="System defaults">

	//
	function userShowQR(btn) {
		let usrUrl = window.location.origin + "/games/game/" + properties.gameId + ":" + properties.gameKey + "/mobile/" + btn.se_data('userid') + ":" + btn.se_data('userkey');
		qrGenerator.makeCode(usrUrl);
		userQRDialog.pseudoDialog.show();

		console.log("USER URL IS: ", usrUrl);
	}

	// Save update
	function gameStatus_save() {
		// Set update
		console.log("GAME SAVING...", gameStatus);

		//
		boardGames.func.saveBoard(boardName, properties.gameId, properties.gameKey, gameStatus);

		//
		mqttReportUpdate()
	}

	// Set update in MQTT
	function mqttReportUpdate() {
		mqtt_connector.sendMessage('/board/update', gameStatus, true);
	}

	//</editor-fold>


	//
	init();
	return {};
};
//
