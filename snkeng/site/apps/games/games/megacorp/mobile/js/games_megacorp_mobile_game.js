"use strict";
//
se.plugin.games_megacorp_mobile_game = function(plugElem) {
	let properties = {
			gameId:intVal(plugElem.se_data('gameid')),
			userId:intVal(plugElem.se_data('userid')),
			userIndex:intVal(plugElem.se_data('userindex')),
		},
		boardStatus = null;
	const
		//
		boardScreenContainer = plugElem.querySelector('div.screensContainer'),
		//
		playerSelectListObj = plugElem.querySelector('div[se-elem="playerList"]'),
		playerSelectListTemplate = playerSelectListObj.querySelector('template'),
		playerSelectListContent = playerSelectListObj.querySelector('div[se-elem="content"]'),
		//
		userObjective = plugElem.querySelector('table.userObjective tbody'),
		//
		userPlayerTable = plugElem.querySelector('table[se-elem="playerList"]'),
		userPlayerTableTemplate = userPlayerTable.querySelector('template'),
		userPlayerTableContent = userPlayerTable.querySelector('tbody'),
		//
		objectivesPrimaryObj = plugElem.querySelector('div[se-elem="objectivesPrimary"]'),
		objectivesPrimaryTemplate = objectivesPrimaryObj.querySelector('template'),
		objectivesPrimaryContent = objectivesPrimaryObj.querySelector('div[se-elem="content"]'),
		//
		objectivesSecondaryObj = plugElem.querySelector('div[se-elem="objectivesSecondary"]'),
		objectivesSecondaryTemplate = objectivesSecondaryObj.querySelector('template'),
		objectivesSecondaryContent = objectivesSecondaryObj.querySelector('div[se-elem="content"]'),
		//
		userCardsObj = plugElem.querySelector('div[se-elem="cards"]'),
		userCardsExcessAlert = plugElem.querySelector('div[se-elem="cardExcessAlert"]'),
		userCardsTemplate = userCardsObj.querySelector('template'),
		userCardsContent = userCardsObj.querySelector('div[se-elem="content"]');

	//
	function init() {
		//
		plugElem.se_on('click', 'button[se-act]', btnActions);
		userCardsContent.se_on('click', 'button', cardRemoveRequest);

		//
		boardScreenContainer.se_data('user', ( properties.userIndex !== -1 ) ? 1 : 0);

		//
		mqtt_connector.connect('mobile', properties.gameId, mqttMessageIn);
	}

	//
	function app_screen_select(screenName) {
		let targetScreen = boardScreenContainer.querySelector('div[data-screen-name="' + screenName + '"]');
		//
		if ( !targetScreen ) {
			console.error("wrong board?", screenName);
			return;
		}

		// Check if selected and skip if it is
		if ( targetScreen.se_attr('aria-hidden') === 'false' ) {
			return;
		}
		//
		console.log("SWITCHING TO", screenName, targetScreen);
		//
		boardScreenContainer.querySelectorAll('* > div[data-screen-name]').se_attr('aria-hidden', 'true');
		targetScreen.se_attr('aria-hidden', 'false');
	}

	//
	function mqttMessageIn(msgHeader, msgContent) {
		//
		switch ( msgHeader[2] ) {
			//
			case 'board':
				//
				switch ( msgHeader[3] ) {
					//
					case 'update':
						// Is first time or update (board is a retained post)
						if ( boardStatus ) {
							// Check if user turn
							if ( boardStatus.status.userTurn !== msgContent.status.userTurn && msgContent.status.userTurn === properties.userIndex ) {
								if ( Notification.permission === "granted" ) {
									var notification = new Notification("Tu turno");
								}
							}

							// Update info
							boardStatus = msgContent;
							content_update();
						} else {
							// Copy first status
							boardStatus = msgContent;

							// Debug info
							console.log("initial board data", boardStatus);

							// NO other app screen used now
							app_screen_select('mainGame');
							content_build();
						}
						break;
					//
					default:
						app_screen_select('mainGame');
						content_update();

						console.error("caso no reconocido ()Jin board", msgHeader);
						break;
				}
				break;
			//
			default:
				console.error("caso no reconocido", msgHeader);
				break;
		}
	}

	//
	function cardRemoveRequest(e, cEl) {
		e.preventDefault();
		let cCard = cEl.se_closest('div').se_data('id');

		//
		if ( !confirm('¿Desea descartar esta tarjeta?') ) {
			return;
		}

		//
		mqtt_connector.sendMessage(
			'/userOp/discard',
			{
				userId:properties.userIndex,
				card:cCard
			},
		);
	}

	//
	function content_build() {
		// Initial checkup
		if ( !boardStatus ) {
			console.error("game data not available");
			return;
		}

		//
		scoreBoardCreate();

		// Build cards
		userCardsBuild();

		// Secondary objectives
		secondaryObjectivesBuild();

		// Build objective primary
		primaryObjectivesBuild();

		//
		userPrimaryObjectiveBuild();
	}

	//
	function content_update() {
		// Update users table
		console.warn("all should update");

		//
		scoreBoardUpdate();

		//
		userCardsBuild();

		//
		secondaryObjectivesUpdate();

		//
		userPrimaryObjectiveUpdate();
	}

	//
	function userCardsBuild() {
		// Build objective primary
		let userCardsHTML = userCardsTemplate.se_html();
		userCardsContent.se_empty();
		let cardCount = 0;
		//
		for ( let cardId of boardStatus.players[properties.userIndex].data.cards ) {
			let currentCard = boardGames.risk.properties.cards[cardId],
				cContent = currentCard.value,
				usable = false;

			//
			switch ( currentCard.type ) {
				case 'location':
					cContent = boardGames.risk.properties.map.locations[currentCard.value].name;
					usable = ( boardStatus.locations[currentCard.value].owner === properties.userIndex);
					break;
				case 'sector':
					cContent = boardGames.risk.properties.map.sectors[currentCard.value].name;
					// usable = ( boardStatus.locations[currentCard.value].owner === properties.userIndex);
					break;
			}

			//
			userCardsContent.se_append(se.struct.stringPopulate(userCardsHTML, {
				id:cardId,
				usable:( usable ) ? 1 : 0,
				type:currentCard.type,
				content:cContent
			}));

			cardCount++;
		}

		// Notify alert
		console.log();
		userCardsExcessAlert.se_attr('aria-hidden', (cardCount > 5) ? 'false' : 'true');
	}

	//
	function secondaryObjectivesBuild() {
		// Build objective secondary
		let objectivesSecondaryHTML = objectivesSecondaryTemplate.se_html();
		for ( const [cId, cSideObj] of Object.entries(boardStatus.sideObjectives) ) {
			//
			let sideObjective = boardGames.risk.properties.sideObjectives[cSideObj.sideObjective],
				bonus = boardGames.risk.properties.bonuses[cSideObj.bonus];

			//
			objectivesSecondaryContent.se_append(se.struct.stringPopulate(objectivesSecondaryHTML, {
				difficulty:sideObjective.difficulty,
				owner:cSideObj.owner,
				requirements:sideObjective.text,
				bonus:bonus.text
			}));
		}
	}

	//
	function secondaryObjectivesUpdate() {
		// Build objective secondary
		let objectivesSecondaryHTML = objectivesSecondaryTemplate.se_html();
		objectivesSecondaryContent.se_empty();

		//
		for ( const [cId, cSideObj] of Object.entries(boardStatus.sideObjectives) ) {
			//
			let sideObjective = boardGames.risk.properties.sideObjectives[cSideObj.sideObjective],
				bonus = boardGames.risk.properties.bonuses[cSideObj.bonus];

			//
			objectivesSecondaryContent.se_append(se.struct.stringPopulate(objectivesSecondaryHTML, {
				difficulty:sideObjective.difficulty,
				owner:cSideObj.owner,
				requirements:sideObjective.text,
				bonus:bonus.text
			}));
		}
	}

	//
	function primaryObjectivesBuild() {
		let objectivesPrimaryHTML = objectivesPrimaryTemplate.se_html(),
			userObject = boardStatus.players[properties.userIndex],
			userObjectiveId = userObject.data.objective.toString();

		//
		for ( const [cId, cMainObj] of Object.entries(boardGames.risk.properties.objectives) ) {
			//
			objectivesPrimaryContent.se_append(se.struct.stringPopulate(objectivesPrimaryHTML, {
				own:( cId === userObjectiveId ) ? 1 : 0,
				content:objectiveTranslate(cMainObj)
			}));
		}
	}

	//
	function userPrimaryObjectiveBuild() {
		let userObject = boardStatus.players[properties.userIndex];

		// Build personal objective
		userObjective.se_html(objectiveTranslate(boardGames.risk.properties.objectives[boardStatus.players[properties.userIndex].data.objective], userObject));
	}

	//
	function userPrimaryObjectiveUpdate() {
		let userObject = boardStatus.players[properties.userIndex];

		// Build personal objective
		userObjective.se_html(objectiveTranslate(boardGames.risk.properties.objectives[boardStatus.players[properties.userIndex].data.objective], userObject));
	}

	//
	function objectiveTranslate(cObj, asUser = null) {
		let cStepTxt = '',
			completed = false,
			cObjectives = [];

		//
		if ( cObj.sectors.length ) {
			for ( let cStep of cObj.sectors ) {
				// Text
				if ( cStep.name === 'any' ) {
					cStepTxt = 'Continente "Comodín" ';
				} else {
					cStepTxt = 'Continente "' + boardGames.risk.properties.map.sectors[cStep.name].name + '" ';
				}
				cStepTxt+= ( cStep.type === 2 ) ? 'completo' : 'parcial';

				completed = false;

				// Check current step
				if ( asUser ) {
					let cSector = asUser.data.sectors[cStep.name];
					if ( ( cStep === 1 && cSector.hasMajority ) || ( cStep === 2 && cSector.hasOwnership ) ) {
						completed = true;
					}
				}

				//
				cObjectives.push({ text: cStepTxt, completed:( completed ) ? 1 : 0 });
			}
		}

		//
		if ( cObj.locations.length ) {
			for ( let cStep of cObj.locations ) {

				let cLocation = boardGames.risk.properties.map.locations[cStep.name];

				cStepTxt = 'Territorio "' + cLocation.name;
				cStepTxt+= '" con ' + cStep.units + " unidades.";

				completed = false;

				// Check current step
				if ( asUser ) {
					if ( ( cLocation.owner === asUser.id ) && ( cLocation.units >= cStep.units  ) ) {
						completed = true;
					}
				}

				//
				cObjectives.push({ text: cStepTxt, completed:( completed ) ? 1 : 0 });
			}
		}

		//
		if ( cObj.totals.length ) {
			for ( let cStep of cObj.totals ) {
				cStepTxt = cStep.count + " ";
				//
				switch ( cStep.type ) {
					//
					case 'capitals':
						cStepTxt+= 'capitales ';
						// Check current step
						if ( asUser && asUser.data.totalCapitals >= cStep.count ) {
							// Now check total foreach
							let countValid = 0;

							//
							for ( const [cLocation, cProperties] of Object.entries(boardStatus.locations) ) {
								if ( cProperties.owner === properties.userIndex && cProperties.isCapital && cProperties.units >= cStep.units ) {
									countValid++;
								}
							}

							if ( countValid >= cStep.count ) {
								completed = true;
							}
						}
						break;
					//
					case 'locations':
						cStepTxt+= 'territorios ';
						// Check current step
						if ( asUser && asUser.data.totalTerritories >= cStep.count ) {
							// Now check total foreach
							let countValid = 0;

							//
							for ( const [cLocation, cProperties] of Object.entries(boardStatus.locations) ) {
								if ( cProperties.owner === properties.userIndex && cProperties.units >= cStep.units ) {
									countValid++;
								}
							}

							if ( countValid >= cStep.count ) {
								completed = true;
							}
						}
						break;
					//
					case 'bases':
						cStepTxt+= 'bases ';

						// Check current step
						if ( asUser && asUser.data.totalBases >= cStep.count ) {
							// Now check total foreach
							let countValid = 0;

							//
							for ( const [cLocation, cProperties] of Object.entries(boardStatus.locations) ) {
								if ( cProperties.owner === properties.userIndex && cProperties.isBaseFor !== -1 && cProperties.units >= cStep.units ) {
									countValid++;
								}
							}

							if ( countValid >= cStep.count ) {
								completed = true;
							}
						}
						break;
				}
				cStepTxt+= " con " + cStep.units + " unidades.";

				//
				cObjectives.push({ text: cStepTxt, completed:( completed ) ? 1 : 0 });
			}
		}

		// Tener su capital
		if ( asUser ) {
			cObjectives.push({text: "Tener capital propia", completed: ( asUser.data.hasCapital ) ? 1 : 0});
		}

		// HTML correspondiente
		let cHTML = ( asUser ) ? "<tr><td><div class=\"fakeCheck\" se-elem=\"hasCapital\" data-value=\"!completed;\"></div></td><td>!text;</td></tr>" : "<li>!text;</li>";

		//
		return se.struct.stringPopulateMany(cHTML, cObjectives);
	}

	//
	function scoreBoardCreate() {
		let cTurn = boardStatus.status.userTurn.toString();

		// Build users
		if ( boardStatus.players.length ) {
			userPlayerTableContent.se_empty();
			let tableStructure = userPlayerTableTemplate.se_html();
			for ( let userIndex in boardStatus.players ) {
				if ( !boardStatus.players.hasOwnProperty(userIndex) ) {
					continue;
				}

				//
				userPlayerTableContent.se_append(se.struct.stringPopulate(tableStructure, {
					playerColor: boardStatus.players[userIndex].color,
					playerName: boardStatus.players[userIndex].name,
					totalTerritories: boardStatus.players[userIndex].data.totalTerritories,
					totalUnits: boardStatus.players[userIndex].data.totalUnits,
					totalPlanes: boardStatus.players[userIndex].data.totalPlanes,
					totalCapitals: boardStatus.players[userIndex].data.totalCapitals,
					totalBases: boardStatus.players[userIndex].data.totalBases,
					hasCapital: ( boardStatus.players[userIndex].data.hasCapital ) ? 1 : 0,
					isUser: ( intVal(userIndex) === properties.userIndex ) ? 1 : 0,
					isTurn: ( userIndex === cTurn ) ? 1 : 0,
				}));
			}
		}
	}

	//
	function scoreBoardUpdate() {
		// Update elements
		for ( let userIndex in boardStatus.players ) {
			if ( !boardStatus.players.hasOwnProperty(userIndex) ) {
				continue;
			}

			let userIndexInt = intVal(userIndex);

			//
			se.element.childrenUpdate(
				userPlayerTableContent.children[userIndexInt],
				{
					'td[se-elem="totalTerritories"]': [
						['text', boardStatus.players[userIndexInt].data.totalTerritories]
					],
					'td[se-elem="totalUnits"]': [
						['text', boardStatus.players[userIndexInt].data.totalUnits]
					],
					'td[se-elem="totalPlanes"]': [
						['text', boardStatus.players[userIndexInt].data.totalPlanes]
					],
					'td[se-elem="totalCapitals"]': [
						['text', boardStatus.players[userIndexInt].data.totalCapitals]
					],
					'td[se-elem="totalBases"]': [
						['text', boardStatus.players[userIndexInt].data.totalBases]
					],
					'div[se-elem="hasCapital"]': [
						['data', 'value', (boardStatus.players[userIndexInt].data.hasCapital) ? 1 : 0]
					],
				}
			);

			// User turn?
			userPlayerTableContent.children[userIndex].se_data('turn', ( boardStatus.status.userTurn === intVal(userIndex) ? 1 : 0));
		}
	}

	//
	function btnActions( e, cBtn ) {
		e.preventDefault();
		//
		const cAction = cBtn.se_attr( 'se-act' );
		switch ( cAction ) {
			//
			case 'notificationTurnOn':
				notificationTurnOn();
				break;
			//
			default:
				console.log("Boton no programado: ", cAction, cBtn);
				break;
		}
	}

	//
	function notificationTurnOn() {
		// Let's check if the browser supports notifications
		if ( !("Notification" in window) ) {
			alert("Notificaciones no disponibles en dispositivo actual.");
		}
		// Let's check whether notification permissions have already been granted
		else if ( Notification.permission === "granted" ) {
			// If it's okay let's create a notification
			var notification = new Notification("Notificaciones prendidas");
		}
		// Otherwise, we need to ask the user for permission
		else if ( Notification.permission !== "denied" ) {
			Notification.requestPermission().then(function (permission) {
				// If the user accepts, let's create a notification
				if ( permission === "granted" ) {
					var notification = new Notification("Notificaciones prendidas!");
				}
			});
		}

	}

	//
	init();
	return {
	};
};
//
