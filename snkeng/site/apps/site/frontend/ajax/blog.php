<?php
//
switch ( \se_nav::next() ) {
	//
	case 'postRead':
		$an_qry = (require __DIR__ . '/../nav/blog.php');
		// Preparar estructura
		$asyncReader = new manual_ajaxjson();
		$asyncReader->createJSON($an_qry, 'post');
		break;

	//
	case 'postLike':
		$params['pId'] = intval($_POST['pId']);
		$params['vote'] = strval($_POST['vote']);
		if ( empty($params['pId']) || empty($params['vote']) ) {
			se_killWithError("Incomplete data","pId:{$params['pId']};vote:{$params['vote']};");
		}

		//
		if ( !empty($_SESSION['votes']['p'][$params['pId']]) ) {
			se_killWithError("Voto ya registrado");
		}

		switch ( $params['vote'] ) {
			case 'p':
				$sql_qry = "UPDATE sc_site_articles SET art_likes=art_likes+1 WHERE art_id={$params['pId']} LIMIT 1;";
				break;
			case 'n':
				$sql_qry = "UPDATE sc_site_articles SET art_nolikes=art_nolikes+1 WHERE art_id={$params['pId']} LIMIT 1;";
				break;
			default:
				se_killWithError("Voto no válido.", $params['vote']);
				break;
		}

		$mysql->submitQuery($sql_qry, [
			'errorKey' => 'postUserLike',
			'errorDesc' => 'No se pudo actualizar el contador del post.'
		]);

		// Asignar y evitar impresión
		$_SESSION['votes']['p'][$params['pId']] = 1;
		$sql_qry = "SELECT art_likes, art_nolikes FROM sc_site_articles WHERE art_id={$params['pId']};";
		$data = $mysql->singleRow($sql_qry, [
			'errorKey' => 'postUserLike',
			'errorDesc' => 'No se pudo leer el contador del post.'
		]);

		//
		$response['d'] = "<span class='likeBtn'><svg class='icon inline mr'><use xlink:href='#fa-thumbs-o-up' /></svg>({$data[0]})</span> <span class='likeBtn'><svg class='icon inline mr'><use xlink:href='#fa-thumbs-o-down' /></svg>({$data[1]})</span>";
		break;

	//
	default:
		\se_nav::invalidPage();
		break;
}
//
