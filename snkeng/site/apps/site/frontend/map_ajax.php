<?php
//
switch ( \se_nav::next() )
{
	//
	case 'blog':
		require __DIR__ . '/ajax/blog.php';
		break;

	//
	case 'contact':
		require __DIR__ . '/ajax/contact.php';
		break;

	//
	case 'banner':
		require __DIR__ . '/ajax/banner.php';
		break;

	//
	default:
		\se_nav::invalidPage();
		break;
}
//
