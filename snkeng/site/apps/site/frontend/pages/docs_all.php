<?php
//
$struct = <<<HTML
<a se-nav="se_middle" class="docsSingle" href="/docs/!urlTitle;/">
<div class="imgCont">
<img src="/server/image/site/w_320/!image;" />
</div>
<div class="title">!title;</div>
</a>
HTML;

$userLevel = se_login::getUserId();
$docUserLimit = ( $userLevel > 2 ) ? 3 : $userLevel;

//
$sql_qry = <<<SQL
SELECT
docs.docs_id AS id,
docs_title_normal AS title, docs_title_url AS urlTitle,
docs_img_struct AS image
FROM sc_site_documents AS docs
WHERE docs.docs_published=1 AND docs.docs_access_level>={$docUserLimit}
ORDER BY docs_title_normal ASC
LIMIT 50;
SQL;
//
$content = $mysql->printSimpleQuery($sql_qry, $struct);


// Docs - Navegación
$page['head']['title'] = 'Documentation';
// Página
$page['body'] = <<<HTML
<div class="fullTitle"><div class="wpContent">Documentation</div></div>
<!-- INI:Documentation -->
<div class="wpContent">
<!-- INI:POSTS -->
<div>
Documentación disponible:
<div class="grid_mr3 docsAll">{$content}</div>
</div>
<!-- END:POSTS -->
</div>
<!-- END:BLOG -->\n
HTML;
//
