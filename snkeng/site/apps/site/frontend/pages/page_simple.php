<?php
// Adición de título, desc y tags.
/*
$sql_qry = <<<SQL
SELECT
art_id AS id, art_title AS title, art_urltitle AS urlTitle, art_url AS fullUrl,
art_meta_word AS metaWord, art_meta_desc AS metaDesc, art_content AS content
FROM sc_site_pagmod
WHERE pgm_id={$params['qpId']};
SQL;
$pageData = $mysql->singleRowAssoc($sql_qry);
$mysql->killIfError('No fue posible leer la página', 'full', '');
*/

$page['head']['title'] = $params['page']['title'];
$page['head']['metaWord'] = $params['page']['metaWord'];
$page['head']['metaDesc'].= $params['page']['metaDesc'];
$page['head']['url'] = $params['page']['fullUrl'];


//
if ( se_login::check_loginLevel('admin') )
{
	// Llamado
	$page['mt'].= <<<JS
// Editable
$('#editCont').se_plugin('wysiwyg', { saveUrl:'/ajax/admin/core/site/pages/updLive', saveId:{$params['page']['id']} });\n
JS;
}

//
$page['body'] = <<<HTML
<div class="fullTitle"><div class="wpContent">{$params['page']['title']}</div></div>
<div id="editCont" class="adv_text">{$params['page']['content']}</div>\n
HTML;
//
