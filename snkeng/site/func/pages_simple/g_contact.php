<?php
// SITIO - Contacto
$page['head']['title'] = 'Contacto';
$page['head']['metaWord'] = 'Contacto';
$page['head']['metaDesc'].= 'Información y formulario de contacto.';
//
$page['head']['url'] = '/contacto/';

//
$referer = ( isset($_SERVER['HTTP_REFERER']) ) ? $_SERVER['HTTP_REFERER'] : '';

//
seLoad_appFunc('sitez', 'simpleText');
$textData = site_simpleText(['site_contact'], $page['mt']);
$page['mt'] .= $textData['mt'];

$a = '';
// Contacto
$page['body'] = <<<HTML
<div class="fullTitle"><div class="wpContent"><h1>Contacto</h1></div></div>
<div id="page_contact" class="wpContent grid">
	<div class="gr_sz04 gr_ps02">\n
		<form class="se_form" method="post" action="/api/sitez/user/mail" se-plugin="simpleForm">
		<input type="hidden" name="ref" value="{$referer}" />
		<div class="separator required">
		<label>
		<div class="cont"><span class="title">Nombre</span><span class="desc"></span></div>
		<input type="text" name="uName" required pattern="[A-z\u00C0-\u00FF\ ]{3,50}{$a}" title="Your name">
		</label>
		</div>
		<div class="separator required">
		<label>
		<div class="cont"><span class="title">E-Mail</span><span class="desc"></span></div>
		<input type="email" name="eMail" required title="Requerido para establecer contacto.">
		</label>
		</div>
		<div class="separator required">
		<label>
		<div class="cont"><span class="title">Mensaje</span><span class="desc"></span></div>
		<textarea name="uMsg" rows="7" required se-plugin="autoSize"></textarea>
		</label>
		</div>
		<div class="separator" id="g_captcha"></div>
		<button type="submit"><svg class="icon inline mr"><use xlink:href="#fa-mail-forward" /></svg>Enviar</button>
		<div class="notification" se-elem="response"></div>
		</form>
	</div>
	<div class="gr_sz04">
		<div id="{$textData['elems']['site_contact']['id']}" class="container">{$textData['elems']['site_contact']['content']}</div>
	</div>
</div>
HTML;



// <div id="{$textData['elems']['site_contact']['elem']}" class="container">{$textData['elems']['site_contact']['content']}</div>

$page['files']['js'] = ['https://www.google.com/recaptcha/api.js?render=explicit&hl=es-419&onload=captchaLoad'];
$page['js'] .= <<<JS
function captchaLoad() { grecaptcha.render('g_captcha', {'sitekey':'{$siteVars['extmod']['gCaptcha']['key']}'}); }\n
JS;
$page['mt'].= <<<JS
if(window.grecaptcha) { captchaLoad(); }\n
JS;
